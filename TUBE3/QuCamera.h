#pragma once
#include "QuConstants.h"
#include "QuUtilities.h"
#include "QuMath.h"


inline void applyQuaternionRotation(QuQuaternion r)
{
	float * m3 = r.convertToM3();
	float * m = convertM3ToM4(m3);
	glMultMatrixf(m);
	delete [] m;
	delete [] m3;
}
class QuDynamicRotation
{
	//target rotation as matrix (used for multiplying matrices)
	//current rotation as quaternion
	//still use QuTimer as before
		//while this is techincally not the best way to do it, it wont be a problem since it is unlikely that this class will do any rotations other than 90 degrees so the speed of the rotation seems constant even though it's the time of the rotation that is constant (i.e. can assume rotation angle is constant)

	float * mTarRot;
	QuQuaternion mTarQuat;
	QuQuaternion mPrevQuat;
	QuQuaternion mCurQuat;

	int mMaxTime;
	QuTimer mTimer;
public:
	const QuQuaternion & getCurQuat() const
	{
		return mCurQuat;
	}
	void reset()
	{
		mMaxTime = 20;

		//load the starting transform
		mTarRot = convertMatrixType<int,float,9>(DIRECTION_ENUM_TO_R_MATRIX[0]);
		multiplyTarByMatrix(MATRIX_R_IDENTITY);
		mTimer.expire();
		mCurQuat = QuQuaternion(1,0,0,0);
		mTarQuat = QuQuaternion(1,0,0,0);
		mPrevQuat = QuQuaternion(1,0,0,0);
	}
	QuDynamicRotation():mCurQuat(1,0,0,0),mTarQuat(1,0,0,0),mPrevQuat(1,0,0,0)
	{
		reset();
	}
	~QuDynamicRotation()
	{
		delete [] mTarRot;
	}

	//this should really be a const reference but I'm not going to bother
	QuTimer & getTimer()
	{
		return mTimer;
	}

	void setTargetRotation(float * aTar)
	{
		mTimer = QuTimer(0,mMaxTime);
		memcpy(mTarRot,aTar,sizeof(float)*9);

		float * vals = convertMatrixToQuaternion(mTarRot);
		mTarQuat = QuQuaternion(vals[3],vals[0],vals[1],vals[2]);
		delete [] vals;

		mPrevQuat = mCurQuat;
	}

	void multiplyTarByMatrix(const int * m)
	{
		float * converted = convertMatrixType<int,float,9>(m);
		float * product = multiplyMatrices3<float>(converted,mTarRot);
		setTargetRotation(product);
		delete [] converted;
		delete [] product;
	}
	

	void update()
	{
		mCurQuat = QuQuaternion::slerp(mPrevQuat,mTarQuat,mTimer.getSquareRoot());
		//mCurQuat = QuQuaternion::slerp(mPrevQuat,mTarQuat,mTimer.getLinear());
		mTimer.update();
	}

	void applyTransform()
	{
		applyQuaternionRotation(mCurQuat);
	}
};

class QuCamera
{
	float mBaseScale;
	float mScale;
	float mTargetHeight;
	float mBaseCameraDistance;
	float mCameraDistance; 
	int mVpWidth;
	int mVpHeight;

	QuDynamicRotation mRot;
	QuSho mPhi, mTheta;
public:
	float getHeight(){return mVpHeight;}
	QuCamera(int aVpWidth,int aVpHeight)
	{
		//hack fix you should clean this up a little
		mBaseScale = CUBE_SCALE;
		mVpWidth = 320;
		mVpHeight = mVpWidth * SCREEN_HEIGHT / SCREEN_WIDTH;	//makes sure there is no streching to fit the viewport that's happenning
		//mVpHeight = aVpHeight;
		mTargetHeight = mVpHeight;
		mCameraDistance = mBaseCameraDistance = DEFAULT_CAMERA_DISTANCE;
	}
	~QuCamera(){}
	
	void reset()
	{
		mRot.reset();
	}
	//------------
	//interface
	//------------

	QuQuaternion getInverseRotation()
	{
		return 1 / (
			mRot.getCurQuat() *
			QuQuaternion::fromAngleAxis(-ANGLES_TO_RADIANS*mTheta.getValue(),-1,0,0) *
			QuQuaternion::fromAngleAxis(-ANGLES_TO_RADIANS*mPhi.getValue(),0,1,0) );
			
	}
	bool rotateInDirection(QuTurnDirections aDir, int aNumberTurns = 1)
	{
		if(aDir == IDENTITY_DIRECTION)
			return false;
		mRot.multiplyTarByMatrix(DIRECTION_ENUM_TO_R_MATRIX[(unsigned)aDir]);
		return true;
	}
	void addImpulse(float aX, float aY)
	{
		mTheta.addV(aY/40);
		mPhi.addV(-aX/40);
	}
	void setTiltTarget(QuQuaternion q)
	{
		float h,a,b;
		q.toEulerAngles(h,a,b);
		mPhi.setXNot(h*RADIANS_TO_ANGLES);
		mTheta.setXNot(a*RADIANS_TO_ANGLES);
	}
	void adjustTiltTarget(float aX, float aY)
	{
		mPhi.setXNot( mPhi.getXNot() + aX);
		mTheta.setXNot( mTheta.getXNot() + aY);
	}
	void setTiltTarget(float aX, float aY, float aZ = -1)
	{
		if(aZ > 0)
			return;
		float lambda = pow((quClamp<float>(1-quAbs<float>(aZ),0,1)),3);
		float mag = sqrt(aX*aX + aY*aY);
		mag = lambda*mag + (1-lambda);
		if(mag != 0)
		{
			aX /= mag;
			aY /= mag;
		}
		float s = -140;
        //mPhi.setXNot(aY*aY*aY*s);
        //mTheta.setXNot(aX*aX*aX*s);
		//mPhi.setXNot(quAbs<float>(aY)*aY*s);
		//mTheta.setXNot(quAbs<float>(aX)*aX*s);
		mPhi.setXNot(pow(quAbs<float>(aY),1.5f)*aY*s);
		mTheta.setXNot(pow(quAbs<float>(aX),1.5f)*aX*s);
	}
	void setShake(float aIntensity, int aTime)
	{
		s3eDebugTracePrintf("ERROR, setShake function not implemented in QuCamera.h");
	}
	void setHeightAndDistanceFromRotation()
	{
		float hScale = cos(mRot.getTimer().getParabola(QUARTER_PI,0))*ONE_OVER_COS_QUARTER_PI;
		hScale = (hScale-1)*0.4 +1;

		float maxRot = quMax(quAbs<float>(mPhi.getValue()),quAbs<float>(mTheta.getValue()));
		float cRot = (90-quClamp<float>(maxRot,0,45));
		float hScale2 = sin(cRot*ANGLES_TO_RADIANS)*ONE_OVER_COS_QUARTER_PI;
		hScale2 = (hScale2-1)*0.7 +1;

		mScale = mBaseScale * sqrt(hScale2*hScale2 + hScale*hScale)*ROOT_TWO_OVER_TWO;

		//TODO calculate distance
		//mCameraDistance = mBaseCameraDistance*(mRot.getTimer().getUpsidedownParabola()*100+1);
	}
	void update()
	{
		mPhi.updateWithFakeCriticalDamping();
		mTheta.updateWithFakeCriticalDamping();
		mRot.update();

	}

	//------------
	//triggers
	//------------
	void setCubeModelProjection()
	{
		float h = mTargetHeight;
		float w = mTargetHeight/getVpRatio();
		float eyeX 	= w / 2.0;
		float eyeY 	= h / 2.0;
		float fov = 2*atan(eyeY/mCameraDistance);
		float nearDist 	= mCameraDistance / 100.0;
		float farDist 	= mCameraDistance * 100.0;	
		float aspect 			= (float)w/(float)h;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		QuPerspective(fov*180/PI, getVpRatio(), nearDist, farDist);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		QuLookAt(0, 0, mCameraDistance, 0, 0, 0.0, 0.0, 1.0, 0.0);
		glLightfv(GL_LIGHT1,GL_POSITION,DEFAULT_LIGHT_POSITION);
		glRotatef(mPhi.getValue(),0,1,0);
		glRotatef(mTheta.getValue(),-1,0,0);
		mRot.applyTransform();
		quScale(mScale);
	}

	//------------
	//camera transfrom properties
	//------------
	//NOTE, size of object should be controlled using scale, not viewport height
	void setViewportHeight( float aTargetHeight)
	{
		mTargetHeight = aTargetHeight;
	}
	void setBaseCameraDistance( float aCameraDistance)
	{
		mBaseCameraDistance = aCameraDistance;
		mCameraDistance = mBaseCameraDistance;
	}

private:
	float getVpRatio()
	{ return (float)mVpWidth/(float)mVpHeight; }


	//----------
	//mapping functions
	//----------
};

class QuGuiCamera
{
	float mTargetHeight;
	float mCameraDistance; 
	int mVpWidth;
	int mVpHeight;
public:
	QuGuiCamera(int aVpWidth,int aVpHeight)
	{
		mVpWidth = aVpWidth;
		mVpHeight = aVpHeight;
		mTargetHeight = aVpHeight;
		mCameraDistance = DEFAULT_CAMERA_DISTANCE;
	}
	~QuGuiCamera(){}
	
	void setShake(float aIntensity, int aTime)
	{
		s3eDebugTracePrintf("ERROR, setShake function not implemented in QuCamera.h");
	}

	//------------
	//triggers
	//------------
	void setGuiProjection()
	{		
		float h = mTargetHeight;
		float w = mTargetHeight/getVpRatio();
		float eyeX 	= w / 2.0;
		float eyeY 	= h / 2.0;
		float fov = 2*atan(eyeY/mCameraDistance);
		float nearDist 	= mCameraDistance / 100.0;
		float farDist 	= mCameraDistance * 100.0;	
		float aspect 			= (float)w/(float)h;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		QuPerspective(fov*180/PI, getVpRatio(), nearDist, farDist);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		QuLookAt(0, 0, mCameraDistance, 0, 0, 0.0, 0.0, 1.0, 0.0);
	}
	int getWidth()
	{
		return mVpWidth;
	}
	int getHeight()
	{
		return mVpHeight;
	}
private:
	float getVpRatio()
	{ return (float)mVpWidth/(float)mVpHeight; }
};