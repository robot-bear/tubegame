#pragma once
#include "GLES\gl.h"
#include "QuDatatypes.h"
#include "s3eSurface.h"
#include "s3eAccelerometer.h"
#include "s3eDebug.h"
#include <string>
#include <iostream>

//#define IPAD
//static const int SCREEN_WIDTH = 640;
//static const int SCREEN_HEIGHT = 960;
#define SCREEN_WIDTH int(s3eSurfaceGetInt(S3E_SURFACE_WIDTH))
#define SCREEN_HEIGHT int(s3eSurfaceGetInt(S3E_SURFACE_HEIGHT))

static const float DEFAULT_CAMERA_DISTANCE = 500;

static const int SCREEN_TEX_WIDTH = 1024;
static const int SCREEN_TEX_HEIGHT = 1024;

static const float ANGLES_TO_RADIANS = 0.0174532925;
static const float RADIANS_TO_ANGLES = 57.2957795;

static const float HALF_PI = 1.57079633;
static const float QUARTER_PI = 0.785398163;
static const float ONE_OVER_COS_QUARTER_PI = 1.41421356;
static const float ROOT_TWO_OVER_TWO = 0.707106781;

static const std::string SOUND_BG = "sounds/BG_MUSIC_1.raw";
static const std::string SOUND_NEXT_TURN = "sounds/NEXTTURN.raw";
static const std::string SOUND_WINNER = "sounds/WINNER.raw";
static const std::string SOUND_HIGHLIGHT = "sounds/HIGHLIGHT.raw";
static const std::string SOUND_SELECT = "sounds/SELECT.raw";
static const std::string SOUND_CLICK = "sounds/CLICK.raw";
static const std::string SOUND_ERROR = "sounds/ERROR.raw";
static const std::string SOUND_FIRE = "sounds/FIRE.raw";

static const int SOUND_BG_RATE = 44100;
static const int SOUND_NEXT_TURN_RATE = 44100;
static const int SOUND_WINNER_RATE = 44100;
static const int SOUND_HIGHLIGHT_RATE = 44100;
static const int SOUND_SELECT_RATE = 44100;
static const int SOUND_CLICK_RATE = 44100;
static const int SOUND_ERROR_RATE = 44100;
static const int SOUND_FIRE_RATE = 44100;

static int MAX_AI_DEPTH = 100;
static float CUBE_SCALE = 50;	//TODO compute me as a function of cube width and height

enum QuMoveImplications
{
	OKAY,
	NOT_OKAY,						
	SUBCUBE_FILLED,
	SUBCUBE_UNREACHABLE,
	CURRENT_PLAYER_WINS,
	CURRENT_PLAYER_WILL_LOSE,
	FACE_UNREACHABLE,
	FACE_FULL,
	//FACE_FULL_AND_UNREACHABLE,
};

enum QuSubCubeStates
{
	NEUTRAL = 0,
	P1_HIGHLIGHT = 1,
	P2_HIGHLIGHT = 2,
	P1_OWN = 3,
	P2_OWN = 4,
	P1_FLASH = 5,
	P2_FLASH = 6
};

enum QuGameState
{
	P1_MOVE,
	P1_TURN,
	P2_MOVE,
	P2_TURN,
	P1_WINNER,
	P2_WINNER
};

//these are array indices used on mOrient
enum QuTurnDirections
{
	IDENTITY_DIRECTION = 0,
	UP = 4,
	DOWN = 5,
	LEFT = 2,
	RIGHT = 3,
	INVALID_DIRECTION = 6
};
static const int DIFFICULTY_TO_DEPTH[3] = {	1,3,10 };
static const int SPEC_TYPE_TO_DIFFICULTY[9][2] =
{	
	0,0,	0,1,	0,2,
	1,0,	1,1,	1,2,
	2,0,	2,1,	2,2
};

//drawing constants
//for our coordinate system
static GLfloat COORDINATE_SYSTEM_COORDS[6][3] = 
{
	0,0,0, 0,0,1,
	0,0,0, 0,1,0,
	0,0,0, 1,0,0
};
static GLfloat COORDINATE_SYSTEM_COLORS[6][4] =
{
	1,0,0,1, 1,0,0,1,
	0,1,0,1, 0,1,0,1,
	0,0,1,1, 0,0,1,1
};


//for game sub cubes
static GLfloat STANDARD_FACE_COORDS[4][3] = {0,-0.5,-0.5,  0,0.5,-0.5, 0,-0.5,0.5, 0,0.5,0.5};
static GLfloat STANDARD_FACE_NORMALS[4][3] = {-1,0,0,	-1,0,0,	-1,0,0,	-1,0,0};
static GLfloat STANDARD_FACE_UV[4][2] = {0,0, 0,1, 1,1, 1,0};
//static GLubyte DEFAULT_FACE_COLOR[4][4] = {255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255};
//for game spheres
static GLfloat OCTAHEDRON_TRIANGLE_COORDS[8][3][3] =
{
	0,0,1, -1,0,0, 0,1,0,
	0,0,1, 0,1,0, 1,0,0,
	0,0,1, 1,0,0, 0,-1,0,
	0,0,1, 0,-1,0, -1,0,0,

	0,0,-1, 0,1,0, -1,0,0,
	0,0,-1, 1,0,0, 0,1,0,
	0,0,-1, 0,-1,0, 1,0,0,
	0,0,-1, -1,0,0, 0,-1,0
};
static GLfloat STANDARD_TRIANGLE_UV[3][2] = {0.1f,0.1f, 0.4f,0.6f, 0.7f,0.1f};		//currently unused as texture mapping on spheres is too slow
//for gui
static GLfloat GUI_RECT_COORDS[4][3] = {-0.5,-0.5,0,  0.5,-0.5,0, -0.5,0.5,0, 0.5,0.5,0};
static GLfloat GUI_RECT_UV[4][2] = {0,0,0,1,1,0,1,1};

//for background image
static unsigned STEPS_TILL_FULL_DARK = 27;
static GLfloat STARTING_RED_COLOR[] = { 1,0.8,0.8,1 };
static GLfloat ENDING_RED_COLOR[] = { 0.3,0.2,0.2,1 };
static GLfloat STARTING_BLUE_COLOR[] = { 0.8,0.8,1,1 };
static GLfloat ENDING_BLUE_COLOR[] = { 0.2,0.2,0.3,1 };

//for transparency fading
static GLfloat FULL_OPAQUE[] = {1,1,1,1};
static GLfloat FULL_TRANSPARENT[] = {1,1,1,0};
static GLfloat QUARTER_OPAQUE[] = {1,1,1,0.25};
static GLfloat THREE_QUARTER_OPAQUE[] = {1,1,1,0.75};
static GLfloat ONE_EIGTH_OPAQUE[] = {1,1,1,0.125};
static GLfloat FULL_RED_OPAQUE[] = {1,0,0,1};
static GLfloat FULL_BLUE_OPAQUE[] = {0,0,1,1};

//lighting constants
static GLfloat DEFAULT_LIGHT_AMBIENT[] = { 0.5f, 0.5f, 0.5f, 1 };
static GLfloat DEFAULT_LIGHT_DIFFUSE[] = { 0.4f, 0.4f, 0.4f, 1 };
static GLfloat DEFAULT_LIGHT_SPECULAR[] = { 0.1f, 0.1f, 0.1f, 1 };
static GLfloat DEFAULT_LIGHT_POSITION[] = { -0.20f, 0.75f, 3.0f, 0.0f };

//used for selection
static const GLfloat SELECTION_INDEX_TO_COLOR[9][4] =
{
	0.1,0,0,1,
	0.2,0,0,1,
	0.3,0,0,1,
	0.4,0,0,1,
	0.5,0,0,1,
	0.6,0,0,1,
	0.7,0,0,1,
	0.8,0,0,1,
	0.9,0,0,1,
};

static const GLubyte SELECTION_INDEX_TO_COLOR_FOR_COMPARISON[9][4] =
{
	25,0,0,255,
	51,0,0,255,
	76,0,0,255,
	102,0,0,255,
	127,0,0,255,
	153,0,0,255,
	179,0,0,255,
	204,0,0,255,
	230,0,0,255,
};

static const GLfloat SELECTION_TOLERANCE = 7;

//material constants
static const GLfloat DEFAULT_MAT_AMBIENT[] = { 0.2f,0.2f,0.2f,1.0f };
static const GLfloat DEFAULT_MAT_DIFFUSE[] = { 0.8f,0.8f,0.8f,1.0f };
static const GLfloat DEFAULT_MAT_SPECULAR[] = { 0.0f,0.0f,0.0f,1.0f };
static const GLfloat DEFAULT_MAT_EMISSION[] = { 0.0f,0.0f,0.0f,1.0f };
static const GLfloat DEFAULT_MAT_SHININESS = 0.0f;

//mapping constants
static const unsigned FACE_TO_SUBCUBES[6][9] =
{
	18, 9, 0, 21, 12, 3, 24, 15, 6,
	2, 11, 20, 5, 14, 23, 8, 17, 26,
	18, 19, 20, 9, 10, 11, 0, 1, 2,
	6, 7, 8, 15, 16, 17, 24, 25, 26,
	0, 1, 2, 3, 4, 5, 6, 7, 8,
	24, 25, 26, 21, 22, 23, 18, 19, 20
};

static const unsigned TRIPLE_INDICES[8][3] =
{
	0,1,2,
	3,4,5,
	6,7,8,

	0,3,6,
	1,4,7,
	2,5,8,

	0,4,8,
	2,4,6
};

static const int FACE_TO_POSSIBLE_TURNS[6][4] = 
{
	2, 3, 4, 5, 2, 3, 4, 5,
	0, 1, 4, 5, 0, 1, 4, 5,
	0, 1, 2, 3, 0, 1, 2, 3
};

//this is used by QuDrawing.h to rotate face 0 to another face
static const int FACE_TO_AXIS_ANGLE[6][2] =
{
	2,0,
	2,180,
	1,-90,
	1,90,
	2,-90,
	2,90
};

static int VECTOR_INDEX_TO_AXIS[3][3] =
{
	1,0,0,
	0,1,0,
	0,0,1
};

static GLint SUBCUBE_TO_SPATIAL_POSITION[27][3] =
{
	-1,1,-1, 0,1,-1, 1,1,-1,
	-1,1,0, 0,1,0, 1,1,0, 
	-1,1,1, 0,1,1, 1,1,1, 

	-1,0,-1, 0,0,-1, 1,0,-1,
	-1,0,0, 0,0,0, 1,0,0, 
	-1,0,1, 0,0,1, 1,0,1, 

	-1,-1,-1, 0,-1,-1, 1,-1,-1,
	-1,-1,0, 0,-1,0, 1,-1,0, 
	-1,-1,1, 0,-1,1, 1,-1,1, 
};

static const unsigned char SUBCUBE_TO_EXPOSED_FACETS[27][6] = 
{
	1,0,1,0,1,0,	0,0,1,0,1,0,	0,1,1,0,1,0,
	1,0,0,0,1,0,	0,0,0,0,1,0,	0,1,0,0,1,0,
	1,0,0,1,1,0,	0,0,0,1,1,0,	0,1,0,1,1,0,

	1,0,1,0,0,0,	0,0,1,0,0,0,	0,1,1,0,0,0,
	1,0,0,0,0,0,	0,0,0,0,0,0,	0,1,0,0,0,0,
	1,0,0,1,0,0,	0,0,0,1,0,0,	0,1,0,1,0,0,

	1,0,1,0,0,1,	0,0,1,0,0,1,	0,1,1,0,0,1,
	1,0,0,0,0,1,	0,0,0,0,0,1,	0,1,0,0,0,1,
	1,0,0,1,0,1,	0,0,0,1,0,1,	0,1,0,1,0,1,
};

static const float FRONT_FACING_VECTOR[3] = {0,0,1};
//TODO this is probaly not correct anymore, need to swap 2 and 3, 4 and 5


static const int FACE_TO_NORMAL[6][3] =
{
	-1,0,0,
	1,0,0,
	0,0,-1,
	0,0,1,
	0,1,0,
	0,-1,0
};

/*
static const int FACE_TO_NORMAL[6][3] =
{
	
	1,0,0,
	-1,0,0,

	0,0,-1,
	0,0,1,
	
	0,-1,0,
	0,1,0
};*/

static const unsigned ROTATION_TO_ORIENTATION[6][6] = 
{
	0,1,2,3,4,5,
	1,0,2,3,4,5,
	
	2,3,1,0,4,5,
	3,2,0,1,4,5,

	4,5,2,3,1,0,
	5,4,2,3,0,1
};

//---------------
//rotation matrices following the right hand rule
//these rotations are all by positive 90 degrees
//---------------
static const int MATRIX_R_IDENTITY[9] =
{
	1,0,0,
	0,1,0,
	0,0,1
};

static const int MATRIX_R_90_X_TO_Y[9] =
{
	0,-1,0,
	1,0,0,
	0,0,1
};
static const int MATRIX_R_90_Y_TO_X[9] =
{
	0,1,0,
	-1,0,0,
	0,0,1
};

static const int MATRIX_R_90_Y_TO_Z[9] =
{
	1,0,0,
	0,0,-1,
	0,1,0
};

static const int MATRIX_R_90_Z_TO_Y[9] =
{
	1,0,0,
	0,0,1,
	0,-1,0
};

static const int MATRIX_R_90_X_TO_Z[9] =
{
	0,0,1,
	0,1,0,
	-1,0,0
};
static const int MATRIX_R_90_Z_TO_X[9] =
{
	0,0,-1,
	0,1,0,
	1,0,0
};

static const int * DIRECTION_ENUM_TO_R_MATRIX[7]=
{
	MATRIX_R_IDENTITY,
	NULL,

	MATRIX_R_90_Z_TO_X,
	MATRIX_R_90_X_TO_Z,
	
	
	MATRIX_R_90_Z_TO_Y,
	MATRIX_R_90_Y_TO_Z,

	NULL
};


//----------------
//conversion functions
//----------------
//0 - 1
//1 - 2
inline char stateToPlayer(QuGameState aState)
{
	if(aState == P1_MOVE || aState == P1_TURN || aState == P1_WINNER)
		return 0;
	return 1;
}

//0 - move
//1 - turn
//2 - win
inline char stateToMoveType(QuGameState aState)
{
	if(aState == P1_WINNER || aState == P2_WINNER)
		return 2;
	if(aState == P1_MOVE || aState == P2_MOVE)
		return 0;
	return 1;
}

inline QuSubCubeStates playerToOwnSubCubeState(unsigned aPlayer)
{
	if(aPlayer == 0)
		return P1_OWN;
	else
		return P2_OWN;
}
inline QuSubCubeStates playerToWinSubCubeState(unsigned aPlayer)
{
	if(aPlayer == 0)
		return P1_FLASH;
	else
		return P2_FLASH;
}
inline unsigned posToCube(unsigned aFace, unsigned aPos)
{
	return FACE_TO_SUBCUBES[aFace * 9][aPos];
}
inline QuVector3 faceToNormal(unsigned aFace)
{
	return QuVector3(FACE_TO_NORMAL[aFace][0],FACE_TO_NORMAL[aFace][1],FACE_TO_NORMAL[aFace][2]);
}
inline unsigned normalToFace(QuVector3 aNorm)
{
	//six case checks
	//note this may still return if normal is invalid
	if(aNorm.x == 1)
		return 1;
	if(aNorm.x == -1)
		return 0;
	if(aNorm.y == 1)
		return 4;
	if(aNorm.y == -1)
		return 5;
	if(aNorm.z == 1)
		return 3;
	if(aNorm.z == -1)
		return 2;
	s3eDebugTracePrintf("ERRRRORRRRR NORMAL TO FACE");
	return 0;
}