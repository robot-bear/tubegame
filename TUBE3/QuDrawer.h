#pragma once
#include "QuConstants.h"
#include "QuDatatypes.h"
#include "QuModels.h"
#include "QuDrawer.h"
#include "QuMaterials.h"
#include "../QuImage.h"
#include <vector>
#include <assert.h>

//TODO fix the templating issues...
template<class T>
struct QuArrayPointer
{
	bool mInit;
	size_t mSize;

	T * mData;
	int mStride;
	int mStart;
	GLenum mType;
	int mDim;

public:
	QuArrayPointer()
	{
		mInit = false;
		mSize = sizeof(T);
	}
	QuArrayPointer(T * aData, int aStride = 0, int aStart = 0)
	{
		initialize(aData,aStride,aStart);
	}
	QuArrayPointer(const QuArrayPointer & o)
	{
		mInit = false;
		mSize = sizeof(T);
		if(o.mInit)
			assert(false);
	}
	~QuArrayPointer()
	{
		deInit();
	}
	QuArrayPointer & operator=(QuArrayPointer & o)
	{
		deInit();
		mInit = o.mInit; mSize = o.mSize; mData = o.mData; mStride = o.mStride; mStart = o.mStart; mType = o.mType; mDim = o.mDim;
		return *this;
	}
	QuArrayPointer & operator=(QuArrayPointer o)
	{
		deInit();
		mInit = o.mInit; mSize = o.mSize; mData = o.mData; mStride = o.mStride; mStart = o.mStart; mType = o.mType; mDim = o.mDim;
		return *this;
	}
	bool isInit()
	{
		return mInit;
	}
	bool initialize(T * aData, GLenum aType, int aDim, int aStride = 0, int aStart = 0)
	{
		if(mInit)
			deInit();
		if(!checkType(aType))
			return false;
		mData = aData;
		mStride = aStride;
		mStart = aStart;
		mType = aType;
		mDim = aDim;
		mInit = true;
		return true;
	}
	void deInit()
	{
		if(!mInit)
			return;
		delete [] mData;
	}
	bool checkType(GLenum aType)
	{
		switch(aType)
		{
		case GL_TEXTURE_COORD_ARRAY:
			break;
		case GL_VERTEX_ARRAY:
			break;
		case GL_NORMAL_ARRAY:
			break;
		case GL_COLOR_ARRAY:
			break;
		default:
			return false;
		}
		return true;
	}

	void enable()
	{
		
		if(!mInit)
			return;
		glEnableClientState(mType);
		switch(mType)
		{
		case GL_TEXTURE_COORD_ARRAY:
			glTexCoordPointer(mDim,GL_FLOAT,mSize*mStride,mData+mStart);
			break;
		case GL_VERTEX_ARRAY:
			glVertexPointer(mDim,GL_FLOAT,mSize*mStride,mData+mStart);
			break;
		case GL_NORMAL_ARRAY:
			glNormalPointer(GL_FLOAT,mSize*mStride,mData+mStart);
			break;
		case GL_COLOR_ARRAY:
			glColorPointer(mDim,GL_FLOAT,mSize*mStride,mData+mStart);
			break;
		default:
			break;
		}

	}
	void disable()
	{
		if(!mInit)
			return;
		glDisableClientState(mType);
	}
};

class QuDrawObject
{
	int mCount;
	QuArrayPointer<float> mV;	//vertex
	QuArrayPointer<float> mT;	//texture
	QuArrayPointer<float> mN;	//normal

	bool mUseImage;
	GLuint mTexId;
	QuImage mImage;
	GLenum mDrawType;
	
public:
	QuDrawObject()
	{
		mCount = 0;
		mUseImage = false;
		mDrawType = GL_TRIANGLE_STRIP;
	}

	~QuDrawObject()
	{
	}

	void setCount(int aCount)
	{
		mCount = aCount;
	}
	void setDrawType(GLenum aType)
	{
		//TODO check if draw type is valid
		mDrawType = aType;
	}
	void loadVertices(float * aData)
	{
		mV.initialize(aData,GL_VERTEX_ARRAY,3);
	}
	void loadVerticesAndGenerateNormals(float * aData)
	{
		mV.initialize(aData,GL_VERTEX_ARRAY,3);

		//generate vertices
		if(mCount > 0)
				mN.initialize(makeNormalArrayFromTriangleStrip(aData,mCount),GL_NORMAL_ARRAY,3);
		
	}
	void loadNormals(float * aData)
	{
		mN.initialize(aData,GL_NORMAL_ARRAY,3);
	}

	void loadTexture(GLuint aTexId, float * aTexData, int aTexStride = 0, int aTexStart = 0)
	{
		mTexId = aTexId;
		mT.initialize(aTexData,GL_TEXTURE_COORD_ARRAY,2,aTexStride,aTexStart);
	}
	bool loadImage(std::string aFilename, float * aTexData, int aTexStride = 0, int aTexStart = 0)
	{
		if(!mImage.loadImage(aFilename))
		{
			//since draw object is responsible for deleting memory, we better take ctare of it here
			delete [] aTexData;
			return false;
		}
		mUseImage = true;
		mT.initialize(aTexData,GL_TEXTURE_COORD_ARRAY,2,aTexStride,aTexStart);
		return true;
	}

	virtual void enable()
	{
		if(mUseImage)
			mImage.bind();
		else
			quActivateTexture(mTexId);

		mV.enable();
		mT.enable();
		mN.enable();
	}

	virtual void disable()
	{
		if(mUseImage)
			mImage.unbind();
		else
			quDeactivateTexture(mTexId);

		mV.disable();
		mT.disable();
		mN.disable();
	}
	void autoDraw()
	{
		enable();
		draw();
		disable();
	}
	void draw()
	{
		if(mCount > 0 && mV.isInit())
			glDrawArrays(mDrawType,0,mCount);
	}

private:
};

template<typename T>
class QuColorableDrawObject : public QuDrawObject
{
	std::map<T,QuArrayPointer<float> > mColorMap;
	T mKey;
public:
	QuColorableDrawObject<T> & operator=(QuColorableDrawObject<T> o)
	{
		mColorMap = o.mColorMap;
		mKey = o.mKey;
		return *this;
	}
	void addColor(T aKey, float * aColor, int size = 3)
	{
		if(mColorMap.size() == 0)
			mKey = aKey;
		mColorMap.erase(aKey);
		mColorMap[aKey] = QuArrayPointer<float>();
		mColorMap[aKey].initialize(aColor,GL_COLOR_ARRAY,size);
	}

	void clearColors()
	{
		mColorMap.clear();
	}

	void setKey(T aKey)
	{
		//if(mColorMap.find(aKey) == mColorMap.end())
		//	cout << "ERROR, SETTING INVALID KEY TO COLOR MAP " << endl; return;
		mKey = aKey;
	}

	bool isColorsExist(){ return mColorMap.size() > 0; }

	virtual void enable()
	{
		QuDrawObject::enable();
		if(isColorsExist())
			mColorMap[mKey].enable();
	}
	virtual void disable()
	{
		QuDrawObject::disable();
		if(isColorsExist())
			mColorMap[mKey].disable();
	}
};

class QuCoordinateSystemDrawer
{
	QuColorableDrawObject<char> mCoords;
public:
	QuCoordinateSystemDrawer()
	{
		mCoords.setCount(6);
		float * verts = new float[6*3];
		float * colors = new float[6*4];
		memcpy(verts,COORDINATE_SYSTEM_COORDS,sizeof(float)*6*3);
		memcpy(colors,COORDINATE_SYSTEM_COLORS,sizeof(float)*6*4);
		mCoords.loadVertices(verts);
		mCoords.addColor(0,colors,4);
		mCoords.setKey(0);
		mCoords.setDrawType(GL_LINES);
	}
	void draw()
	{
		glPushMatrix();
		glScalef(200,200,200);
		glDisable(GL_LIGHTING);
		mCoords.autoDraw();
		glEnable(GL_LIGHTING);
		glPopMatrix();
	}
};

class QuDrawer
{
	int steps;
	QuDrawObject mFaceDrawObject;
	QuDrawObject mDivetDrawObject;
	QuDrawObject mSphereDrawObject;

	QuColorableDrawObject<unsigned> mSelectionDrawObject;
	QuMaterial mRedMat, mBlueMat, mNeutralMat, mRedHighMat, mBlueHighMat;
	QuMaterial mBFN, mBFR, mBFB, mBDN, mBDR, mBDB, mFFN, mFFR, mFFB, mFDN, mFDR, mFDB;

	bool mFrontFacing [6];

	QuTimer mGameTimer;

public:
	QuDrawer()
	{
		initialize();
	}

	~QuDrawer() 
	{
	}

private:
	void initModelsAndLights()
	{
		//MODELS
		steps = 28;
		float * texData;
		float trans[3] = {0,0.5,0.5};

		std::string image = "images/paper_texture.png";

		mFaceDrawObject.setCount(steps*2);
		mFaceDrawObject.loadVerticesAndGenerateNormals(QuFunctionTesselator::basicTesselator(QuFunctionTesselator::square<1,1>,QuFunctionTesselator::circle<1,3,0,1>,steps,QuFunctionTesselator::squarePoi()));
		texData = QuFunctionTesselator::basicTesselator(QuFunctionTesselator::square<1,1>,QuFunctionTesselator::circle<1,3,0,1>,steps,QuFunctionTesselator::squarePoi());
		operateAndReplaceVector<3>(texData,steps*2,1,trans);
		mFaceDrawObject.loadImage(image,texData,3,1);;
		//printMatrix<float>(mFaceDrawObject.mN.mData,steps*2,3);
		
		mDivetDrawObject.setCount(steps*2);
		mDivetDrawObject.loadVerticesAndGenerateNormals(QuFunctionTesselator::basicTesselator(QuFunctionTesselator::circle<1,3,0,1>,QuFunctionTesselator::circle<1,4,1,6>,steps,QuFunctionTesselator::squarePoi()));
		texData = QuFunctionTesselator::basicTesselator(QuFunctionTesselator::circle<1,3,0,1>,QuFunctionTesselator::circle<1,4,1,6>,steps,QuFunctionTesselator::squarePoi());
		operateAndReplaceVector<3>(texData,steps*2,1,trans);
		mDivetDrawObject.loadImage(image,texData,3,1);

		int sub = 2;
		int iter = 1;
		for(int i = 0; i < sub; i++)
			iter*=4;
		//mSphereDrawObject.setCount(8*3*iter);
		mSphereDrawObject.setCount(4*3*iter);
		float * v = NULL;
		float * n = NULL;
		float * t = NULL;
		//generateIcosphere(v,n,t,sub);
		generatePartialIcosphere(v,n,t,sub);
		mSphereDrawObject.loadVertices(v);
		mSphereDrawObject.loadNormals(n);
		//mSphereDrawObject.loadImage("images/noise.jpg",t);
		mSphereDrawObject.setDrawType(GL_TRIANGLES);

		float * faceData = new float[4*3];
		memcpy(faceData,STANDARD_FACE_COORDS,sizeof(float)*4*3);
		mSelectionDrawObject.setCount(4);
		mSelectionDrawObject.loadVertices(faceData);
		for(unsigned i = 0; i < 9; i++)
			mSelectionDrawObject.addColor(i,cpcpcpVector<float>(SELECTION_INDEX_TO_COLOR[i],4,4),4);

		//LIGHTS
		mRedMat = QuMaterialPrefabs::getMaterial("RED");
		mBlueMat = QuMaterialPrefabs::getMaterial("BLUE");
		mRedHighMat = QuMaterialPrefabs::getMaterial("REDHIGH");
		mBlueHighMat = QuMaterialPrefabs::getMaterial("BLUEHIGH");
		mNeutralMat = QuMaterialPrefabs::getMaterial("NEUTRAL");
		
		//notation <F/B><D/F><R/N/B> front/back, divet/face, red/blue/neutral
		mBFN = QuMaterialPrefabs::getMaterial("BFN");
		mBFR = QuMaterialPrefabs::getMaterial("BFR");
		mBFB = QuMaterialPrefabs::getMaterial("BFB");
		mBDN = QuMaterialPrefabs::getMaterial("BDN");
		mBDR = QuMaterialPrefabs::getMaterial("BDR");
		mBDB = QuMaterialPrefabs::getMaterial("BDB");
		mFFN = QuMaterialPrefabs::getMaterial("FFN");
		mFFR = QuMaterialPrefabs::getMaterial("FFR");
		mFFB = QuMaterialPrefabs::getMaterial("FFB");
		mFDN = QuMaterialPrefabs::getMaterial("FDN");
		mFDR = QuMaterialPrefabs::getMaterial("FDR");
		mFDB = QuMaterialPrefabs::getMaterial("FDB");
	}
	void initialize()
	{
		initModelsAndLights();
	}

	void drawSubCubeFace(unsigned aSubCube, unsigned aFace, unsigned aState,  QuMaterial & facemat, QuMaterial & divetmat, float aScale = 1)
	{
		//QuVector3 norm = faceToNormal(aFace);
		glPushMatrix();
		//translate to correct place
		GLint * pos = SUBCUBE_TO_SPATIAL_POSITION[aSubCube];
		glTranslatef(pos[0],pos[1],pos[2]);
		//calculate rotations and scaling
		int * axis = VECTOR_INDEX_TO_AXIS[FACE_TO_AXIS_ANGLE[aFace][0]];
		int angle = FACE_TO_AXIS_ANGLE[aFace][1];
		glScalef(aScale,aScale,aScale);
		glRotatef(angle,axis[0],axis[1],axis[2]);
		//compensate for scale effect on x axis
		//glTranslatef(-1/2.0f/aScale,0,0);
		glTranslatef(-1/2.0f,0,0);
		
		facemat.enable();
		mFaceDrawObject.enable();
		mFaceDrawObject.draw();
		mFaceDrawObject.disable();
		
		divetmat.enable();
		mDivetDrawObject.enable();
		mDivetDrawObject.draw();
		mDivetDrawObject.disable();

		glPopMatrix();
	}

	void drawPartialSphere(unsigned aSubCube, unsigned aFace, float aScale)
	{
		glPushMatrix();
		aScale*=0.5;
		GLint * pos = SUBCUBE_TO_SPATIAL_POSITION[aSubCube];
		glTranslatef(pos[0],pos[1],pos[2]);
		int * axis = VECTOR_INDEX_TO_AXIS[FACE_TO_AXIS_ANGLE[aFace][0]];
		int angle = FACE_TO_AXIS_ANGLE[aFace][1];
		glScalef(aScale,aScale,aScale);
		glRotatef(angle,axis[0],axis[1],axis[2]);
		mSphereDrawObject.enable();
		mSphereDrawObject.draw();
		mSphereDrawObject.disable();
		glPopMatrix();
	}
	/*use only for full sphere model
	void drawSphere(unsigned aSubCube, float aScale)
	{
		aScale*=0.5;
		glPushMatrix();
		//translate to correct place
		GLint * pos = SUBCUBE_TO_SPATIAL_POSITION[aSubCube];
		glTranslatef(pos[0],pos[1],pos[2]);
		glScalef(aScale,aScale,aScale);
		//glutSolidSphere(0.45f,20,20);
		mSphereDrawObject.enable();
		mSphereDrawObject.draw();
		mSphereDrawObject.disable();
		glPopMatrix();
	}*/
public:
	
	unsigned drawSelectionFaces(unsigned aFace, int x, int y, QuCamera & aCam)
	{
		
		//glPushAttrib(GL_LIGHTING_BIT | GL_CURRENT_BIT | GL_DEPTH_BUFFER_BIT);
		glDepthMask(GL_FALSE);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		//set buffer NAH, we draw on the main buffer and shit gets cleared for us. it's cool
		const unsigned * subCubes = FACE_TO_SUBCUBES[aFace]; 
		for(unsigned i = 0; i < 9; i++)
		{
			glPushMatrix();
			GLint * pos = SUBCUBE_TO_SPATIAL_POSITION[subCubes[i]];
			glTranslatef(pos[0],pos[1],pos[2]);
			int * axis = VECTOR_INDEX_TO_AXIS[FACE_TO_AXIS_ANGLE[aFace][0]];
			int angle = FACE_TO_AXIS_ANGLE[aFace][1];
			glRotatef(angle,axis[0],axis[1],axis[2]);
			glTranslatef(-1/2.0f,0,0);
			mSelectionDrawObject.setKey(i);
			mSelectionDrawObject.autoDraw();
			glPopMatrix();
		}
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);
		glEnable(GL_LIGHTING);
		//glPopAttrib();

		
		GLubyte r[4];
		//cout << "click at " << x << " " << y << endl;
        glReadPixels(x,SCREEN_HEIGHT-y,1,1,GL_RGBA,GL_UNSIGNED_BYTE,r);
		//cout << "color selected " << (int)r[0] << " " << (int)r[1] << " " << (int)r[2] << " " << (int)r[3] << endl;
        for(int i = 0; i < 9; i++)
        {
			if(r[1] > 0 || r[2] > 0)
				continue;
			//cout << "touching difference " << r[0]-SELECTION_INDEX_TO_COLOR_FOR_COMPARISON[i][0] << endl;
            if(quAbs<float>(r[0]-SELECTION_INDEX_TO_COLOR_FOR_COMPARISON[i][0]) < SELECTION_TOLERANCE)
                return subCubes[i];
        }
		return 13;	//return center if no cube selected
	}
	void computeFrontFacingFaces(QuCamera & cam)
	{
		//float * vm3 = cam.getInverseRotation().convertToM3();

		float vm[ 16 ]; 
		glGetFloatv( GL_MODELVIEW_MATRIX, vm );
		float * vm3 = convertM4toM3(vm);
		float * l = transformVectorWithMatrix<float,3>(vm3,FRONT_FACING_VECTOR);

		for(int i = 0; i < 6; i++)
		{
			float n[3];
			for(int j = 0; j < 3; j++)
				n[j] = FACE_TO_NORMAL[i][j]; 
			if(dot<float,3>(n,l) > 0)
				mFrontFacing[i] = true;
			else
				mFrontFacing[i] = false;			
		}
		delete [] l;
		delete [] vm3;
	}
};