#pragma once

#include "../pngloader.h"
#include "GLES\gl.h"
#include <string>
#include <set>


class QuImage
{
	static std::set<QuImage*> globalImageList;
	GLuint mId;
	PngLoader * loader;
	bool isLoaded; 
	std::string mFilename;
public:
	static void reloadAllImages()
	{
		//cout << "Reloading all images" << endl;
		for(std::set<QuImage*>::iterator it = globalImageList.begin(); it != globalImageList.end(); it++)
		{
			(*it)->reloadImage();
		}
	}
	QuImage():loader(NULL),mId(-1),isLoaded(false)
	{
		globalImageList.insert(this);
	}
	~QuImage()
	{
		destroy();
		globalImageList.erase(this);
	}
	void destroy()
	{
		if(loader!=NULL) delete loader;
		if(isLoaded)
			glDeleteTextures(1,&mId);
		isLoaded = false;
		mId = -1;
	}
	void bind()
	{
		if(isLoaded)
		{
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D,mId); 
		}
	}
	void unbind()
	{ 
		//glBindTexture(GL_TEXTURE_2D,NULL); 
		//glDisable(GL_TEXTURE2D);
	}
	bool reloadImage()
	{
		if(mFilename == "")
			return false;
		destroy();
		if(loadImage(mFilename))
		{
			finishLoadingImage();
			return true;
		}
		return false;
	}
	//returns true if image is done loading
	bool pumpImage(int rows)
	{
		if(loader->update(rows))
			return setTexture();
		return false;
	}
	bool finishLoadingImage()
	{
		loader->update(loader->getHeight());
		return setTexture();
	}
	bool loadImage(std::string filename)
	{
		mFilename = filename;
		loader = new PngLoader();
		loader->setFile(filename);
		//load the entire image TEMP
		return finishLoadingImage();
		//return loader->isPngReadyToLoad();
	}
private:
	bool setTexture()
	{
		if(loader->isPngDoneLoading())
		{
			glPixelStorei(GL_UNPACK_ALIGNMENT,1);
			glGenTextures(1,&mId);
			glBindTexture(GL_TEXTURE_2D,mId);
			glTexParameterx(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameterx(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
			//glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
			glTexImage2D(GL_TEXTURE_2D, 0, loader->getFormat(), loader->getWidth(), loader->getHeight(), 0, loader->getFormat(), GL_UNSIGNED_BYTE, loader->getImageData());
			delete loader;
			loader = NULL;
			isLoaded = true;
			return true;
		}
		return false;
	}
};