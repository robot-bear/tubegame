#pragma once
#include "QuConstants.h"
#include "QuDrawer.h"
#include "QuUtilities.h"
#include "QuCamera.h"
#include "../QuSound_STROKE.h"
#include <string>

//generic rendering context for full screen images with blending
class QuGuiRenderingContext
{
	QuGuiCamera mCamera;
public:
	QuGuiRenderingContext():mCamera(SCREEN_WIDTH,SCREEN_HEIGHT){}
	void enable(bool scale = true)
	{
		mCamera.setGuiProjection();
		if(scale)
			glScalef(mCamera.getWidth(),mCamera.getHeight(),1);
		if(s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION) != S3E_SURFACE_BLIT_DIR_ROT180)
			glRotatef(180,0,0,1);
		glEnable(GL_BLEND);
		//glBlendFunc(GL_DST_ALPHA, GL_SRC_ALPHA);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(false);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
	}
	void disable()
	{
		glEnable(GL_LIGHTING);
		glEnable(GL_DEPTH_TEST);
		glDepthMask(true);
		glDisable(GL_BLEND);
	}
};

//-------------
//blurring
//code taken from http://nehe.gamedev.net/data/lessons/lesson.asp?lesson=36
//-------------
#define WIDTH SCREEN_TEX_WIDTH
#define HEIGHT SCREEN_TEX_HEIGHT
class QuBlurrer
{
	QuGuiCamera mCamera;
	QuColorableDrawObject<int> mDraw;
	GLuint mTex;
	float mInc;
	float mRep;
private:
	void createDrawObject()
	{
		mDraw.setCount(4);
		float * aTexData = new float[4*2];
		memcpy(aTexData,GUI_RECT_UV,sizeof(float)*4*2);
		float * rectCoords = new float[4*3];
		memcpy(rectCoords,GUI_RECT_COORDS,sizeof(float) * 4 * 3);
		mDraw.loadVertices(rectCoords);
		mDraw.loadTexture(mTex,aTexData);
	}
	void captureImage()
	{
		glBindTexture(GL_TEXTURE_2D,mTex);
		glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, 0, 0, WIDTH, HEIGHT, 0);
	}
	void initialize()
	{			
		unsigned int* data;

		// Create Storage Space For Texture Data (128x128x4)
		data = (unsigned int*)new GLuint[((WIDTH * HEIGHT)* 4 * sizeof(unsigned int))];
		//ZeroMemory(data,((WIDTH * HEIGHT)* 4 * sizeof(unsigned int)));	// Clear Storage Memory

		glGenTextures(1, &mTex);								// Create 1 Texture
		glBindTexture(GL_TEXTURE_2D, mTex);					// Bind The Texture
		glTexImage2D(GL_TEXTURE_2D, 0, 4, WIDTH, HEIGHT, 0,
			GL_RGBA, GL_UNSIGNED_BYTE, data);						// Build Texture Using Information In data
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

		delete [] data;												// Release data
	}
public:
	QuBlurrer():mCamera(SCREEN_WIDTH,SCREEN_HEIGHT)
	{
		initialize();
	}
	~QuBlurrer()
	{
	}
	void setProperties(int reps,float inc)
	{
		mRep = reps;
		mInc = inc;
		//TODO generate colors
	}
	void capture()
	{
		captureImage();
		createDrawObject();
	}
	void draw()							// Draw The Blurred Image
	{
		glBlendFunc(GL_SRC_ALPHA,GL_ONE);							// Set Blending Mode
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glDisable(GL_DEPTH_TEST);									// Disable Depth Testing
		glEnable(GL_BLEND);											// Enable Blending

		glPushMatrix();
		mCamera.setGuiProjection();
		glScalef(WIDTH,HEIGHT,0);
		//glTranslatef(-(mCamera.getWidth() - WIDTH)/2.,-(mCamera.getHeight() - HEIGHT)/2.,0);
		//glScalef(50,50,50);
		glRotatef(90,0,0,1);
		mDraw.enable();
		for(int i = 0; i < mRep; i++)
		{
			glScalef(mInc,mInc,mInc);
			mDraw.draw();
		}
		mDraw.disable();
		glPopMatrix();

		glEnable(GL_DEPTH_TEST);									// Enable Depth Testing
		glDisable(GL_BLEND);										// Disable Blending
	}

};
#undef WIDTH
#undef HEIGHT




class QuTintableGuiImage
{
	QuGuiRenderingContext mContext;
	QuColorableDrawObject<int> mDrawObject;
	QuTimer mTimer;
	QuArrayInterpolator<float,4> mColor;
	bool mIsUseColor;
public:
	QuTintableGuiImage():mTimer(0,30),mIsUseColor(true)
	{
		float c[4] = {0.6,0.6,0.8,1};
		mColor.setBaseValue(c);
		float d[4] = {0.8,0.6,0.6,1};
		mColor.setTargetValue(d);
	}
	~QuTintableGuiImage()
	{
	}
	void useColor(bool aUseColor)
	{
		mIsUseColor = aUseColor;
		if(!mIsUseColor)
			mDrawObject.clearColors();
	}
	void update()
	{
		if(mIsUseColor)
		{
			mTimer.update();
			float * fourc = cpcpcpVector<float>(mColor.interpolate(mTimer.getLinear()).d,4,4);
			mDrawObject.addColor(1,fourc,4);
			mDrawObject.setKey(1);
		}
	}
	
	void unloadImage()
	{
		mDrawObject = QuColorableDrawObject<int>();
	}
		 
	bool loadImage(std::string aFilename, float * aTexData = NULL)
	{
		mDrawObject.setCount(4);
		if(aTexData == NULL)
		{
			aTexData = new float[4*2];
			memcpy(aTexData,GUI_RECT_UV,sizeof(float)*4*2);
		}
		if(!mDrawObject.loadImage(aFilename, aTexData))
			return false;
		//otherwise, if image loaded succesfully, we generate the rest of the info
		float * rectCoords = new float[4*3];
		memcpy(rectCoords,GUI_RECT_COORDS,sizeof(float) * 4 * 3);
		mDrawObject.loadVertices(rectCoords);
		return true;
	}
	void draw()
	{
		glPushMatrix();
		mContext.enable();
		mDrawObject.autoDraw();
		mContext.disable();
		glPopMatrix();
	}
	void draw(float x, float y, float rot, float s = 1)
	{
		glPushMatrix();
		mContext.enable(false);
		glScalef(1, 1, 1);
  		glTranslatef(-SCREEN_WIDTH/2, SCREEN_HEIGHT/2, 0);
		glTranslatef(x,-y,0);
		glScalef(s,s,s);
		glRotatef(rot,0,0,1);
		mDrawObject.autoDraw();
		mContext.disable();
		glPopMatrix();
	}
	void setColor(float aColor[4])
	{
		mColor.setBaseValue(aColor);
		mColor.setTargetValue(aColor);
		update();
	}
	void setBaseColor(float aColor[4])
	{
		mColor.setBaseValue(aColor);
	}
	void setTargetColor(float aColor[4])
	{
		setBaseColor(mColor.interpolate(mTimer.getLinear()).d);
		mColor.setTargetValue(aColor);
		mTimer.reset();
	}
private:
};

struct QuConstantForceParticle
{
	float x,y,rot,s;
	float vx,vy,vr,vs;
	float ax,ay;
	QuTimer mTimer;
	QuArrayInterpolator<float,4> mColor;
	bool isUseColor;
	QuConstantForceParticle(unsigned profile = 0)
	{
		switch(profile)
		{
		case 1:
			isUseColor = true;
			x = y = ax = ay = vx = vy = rot = vr =0;
			s = 20;
			vs = 60;
			mTimer.setTargetAndReset(4);
			mColor.setBaseValue(FULL_OPAQUE);
			mColor.setTargetValue(FULL_TRANSPARENT);
			break;
		case 2:	//player 1 wins
			isUseColor = true;
			mTimer.setTargetAndReset(35);
			mColor.setBaseValue(FULL_RED_OPAQUE);
			mColor.setTargetValue(FULL_TRANSPARENT);
			x = y = 0;
			vx = quRandfSym()*30;
			vy = quRandfSym()*30;
			ax = 0;
			ay = 0;
			vr = quRandfSym()*3;
			s = 30;
			vs = -2;	//set to 20, pretty cool
			break;
		case 3: //player 2 wins;
		default:
			isUseColor = true;
			mTimer.setTargetAndReset(35);
			mColor.setBaseValue(FULL_BLUE_OPAQUE);
			mColor.setTargetValue(FULL_TRANSPARENT);
			x = y = 0;
			vx = quRandfSym()*15;
			vy = quRandfSym()*15;
			ax = 0;
			ay = 0;
			vr = quRandfSym()*3;
			s = 30;
			vs = -2;	//set to 20, pretty cool
			break;
		}
	}
	void setPosition(int argx, int argy)
	{
		x = argx; y = argy;
	}
	struct ARRAY4
	{
		ARRAY4(float _d[4])
		{
			for(int i = 0; i<4; i++)
				d[i] = _d[i];
		}
		float d[4];
	};
	ARRAY4 getColor()
	{
		if(!isUseColor)
			return ARRAY4(FULL_OPAQUE);
		return ARRAY4(mColor.interpolate(mTimer.getLinear()).d);
	}
	void update()
	{
		mTimer.update();
		x += vx;
		y += vy;
		rot += vr;
		s += vs;

		vx += ax;
		vy += ay;
	}
	bool isInBox(QuRectangle<int> rect)
	{
		return rect.doesContainPoint(x,y);
	}
};

class QuFireworks
{
	QuTintableGuiImage mImg;
	std::list<QuConstantForceParticle> mParts;
	QuTimer mFireworksTimer;
	unsigned mPlayer;
public:
	QuFireworks()
	{
		mFireworksTimer.setTarget(150);
		mFireworksTimer.expire();
		mImg.loadImage("images/fireworks.png");
	}
	~QuFireworks()
	{
	}
	void fireworks(int x, int y)
	{
		//TODO play a sound
		for(int i = 0; i < 15; i++)
		{
			mParts.push_back(QuConstantForceParticle(2+mPlayer));
			mParts.back().setPosition(x,y);
			float r = HALF_PI;
			#ifdef TARGET_OF_IPHONE
			r = ofxAccelerometer.getForce().y;
			#endif
			mParts.back().ax = 5*cos(r);
			mParts.back().ay = 5*sin(r);
			
		}
	}
	void makeFireworks(unsigned aPlayer)
	{
		mPlayer = aPlayer;
		mFireworksTimer.reset();
	}
	void draw()
	{
		for(std::list<QuConstantForceParticle>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			if(i->isUseColor)
				mImg.setColor(i->getColor().d);
			mImg.draw(i->x,i->y,i->rot,i->s);
		}
	}
	void update()
	{

		//fireworks stuff
		switch(mFireworksTimer.getTimeSinceStart())
		{
		case 0:
			QuSoundManager::getRef().playSound(SOUND_FIRE);
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			break;
		case 10:
			QuSoundManager::getRef().playSound(SOUND_FIRE);
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			break;
		case 40:
			QuSoundManager::getRef().playSound(SOUND_FIRE);
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			break;
		case 65:
			QuSoundManager::getRef().playSound(SOUND_FIRE);
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			break;
		case 89:
			QuSoundManager::getRef().playSound(SOUND_FIRE);
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			break;
		case 120:
			QuSoundManager::getRef().playSound(SOUND_FIRE);
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			break;
		default:
			//if(mFireworksTimer.getTimeSinceStart() < 149 && quRandRange(0,4) == 0)
			//	fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			break;
		}
		mFireworksTimer++;


		std::list<std::list<QuConstantForceParticle>::iterator> removal;
		for(std::list<QuConstantForceParticle>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			i->update();
			//TODO handle particle destruction
			int exp = 20;
			if(i->mTimer.isExpired())
				removal.push_back(i);
			else if(!i->isInBox(QuRectangle<int>(0-exp,0-exp,SCREEN_WIDTH + 2*exp,SCREEN_HEIGHT + 2*exp)))
				removal.push_back(i);
			
		}
		for(std::list<std::list<QuConstantForceParticle>::iterator>::iterator i = removal.begin(); i != removal.end(); i++)
			mParts.erase(*i);
	}
private:
};
class QuTouchEffect
{
	QuTintableGuiImage mImg;
	std::list<QuConstantForceParticle> mParts;
public:
	QuTouchEffect()
	{
		mImg.loadImage("images/image.png");
	}
	~QuTouchEffect()
	{
	}
	void touch(int x, int y)
	{
		for(int i = 0; i < 1; i++)
		{
			mParts.push_back(QuConstantForceParticle(1));
			mParts.back().setPosition(x,y);
		}
	}
	void draw()
	{
		for(std::list<QuConstantForceParticle>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			if(i->isUseColor)
				mImg.setColor(i->getColor().d);
			mImg.draw(i->x,i->y,i->rot,i->s);
		}
	}
	void update()
	{
		std::list<std::list<QuConstantForceParticle>::iterator> removal;
		for(std::list<QuConstantForceParticle>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			i->update();
			//TODO handle particle destruction
			int exp = 20;
			if(i->mTimer.isExpired())
				removal.push_back(i);
			else if(!i->isInBox(QuRectangle<int>(0-exp,0-exp,SCREEN_WIDTH + 2*exp,SCREEN_HEIGHT + 2*exp)))
				removal.push_back(i);
			
		}
		for(std::list<std::list<QuConstantForceParticle>::iterator>::iterator i = removal.begin(); i != removal.end(); i++)
			mParts.erase(*i);
	}
private:
};

class QuEffects
{
	QuTouchEffect mTouch;
public:
	QuEffects()
	{
	}
	~QuEffects()
	{
	}
	void draw()
	{

	}
	void update()
	{

	}
private:
};


inline void drawRectangle(QuRectangle<int> r, float color [4])
{
	QuGuiRenderingContext().enable(false);
	//invert the y
	glScalef(1,-1,1);
	glTranslatef(-SCREEN_WIDTH/2,-SCREEN_HEIGHT/2,0);
	//make the points
	float verts [4*3] = 
	{
		r.x, r.y, 0,
		r.x + r.w, r.y, 0,
		r.x, r.y + r.h, 0,
		r.x + r.w, r.y + r.h, 0
	};
	float colors [4 * 4] =
	{
		color[0],color[1],color[2],color[3],
		color[0],color[1],color[2],color[3],
		color[0],color[1],color[2],color[3],
		color[0],color[1],color[2],color[3]
	};

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	glVertexPointer(3,GL_FLOAT,0,verts);
	glColorPointer(4,GL_FLOAT,0,colors);
	//glDrawArrays(GL_LINE_STRIP,0,4);
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
}

class QuButtonGroup
{
	std::vector<QuRectangle<int> > mRects;
public:
	void addButton(QuRectangle<int> aR)
	{
		mRects.push_back(aR);

	}
	//returns -1 if nothing was pressed, otherwise the index of the vector
	int isButtonPressed(int x, int y)
	{
		for(int i = 0; i < mRects.size(); i++)
		{
			if(mRects[i].doesContainPoint(x,y))
				return i;
		}
		return -1;
	}
	void drawSelection()
	{
		QuGuiRenderingContext().enable(false);
		//invert the y
		glScalef(1,-1,1);
		glTranslatef(-SCREEN_WIDTH/2,-SCREEN_HEIGHT/2,0);
		for(int i = 0; i < mRects.size(); i++)
		{
			QuRectangle<int> r =  mRects[i];
			//make the points
			float verts [4*3] = 
			{
				r.x, r.y, 0,
				r.x + r.w, r.y, 0,
				r.x, r.y + r.h, 0,
				r.x + r.w, r.y + r.h, 0
			};
			/*
			float colors [4 * 4] =
			{
				1,0,0,1,1,0,0,1,1,0,0,1,1,0,0,1
			};*/

			float colors [4 * 4] =
			{
				0,0,0,1, 0,0,0,1, 0,0,0,1, 0,0,0,1,
			};

			glEnableClientState(GL_VERTEX_ARRAY);
			glEnableClientState(GL_COLOR_ARRAY);
			glVertexPointer(3,GL_FLOAT,0,verts);
			glColorPointer(4,GL_FLOAT,0,colors);
			glDrawArrays(GL_LINE_STRIP,0,4);
			//glDrawArrays(GL_TRIANGLE_STRIP,0,4);
			glDisableClientState(GL_VERTEX_ARRAY);
			glDisableClientState(GL_COLOR_ARRAY);
		}
	}
};
class QuMenuItem : public QuButtonGroup
{
	std::string mFn;
	bool isSet;
	bool lazyLoading;
	QuTintableGuiImage mImg;

public:
	QuMenuItem(bool ll = false)
	{
		lazyLoading = ll;
		isSet = false;
		mImg.useColor(false);
	}
	void loadImage()
	{
		if(isSet && lazyLoading)
			mImg.loadImage(mFn);
	}
	void setImage(std::string filename)
	{
		mFn = filename;
		isSet = true;
		if(!lazyLoading)
			mImg.loadImage(mFn);
	}
	void unloadImage()
	{
		mImg.unloadImage();
	}
	void draw()
	{
		mImg.draw();
	}
	void update()
	{
		//no need to update the image since it does nto change color
	}
};
class QuMenu
{
	std::list<QuMenuItem> mItems;
	unsigned mCur;
	
//READ ONLY
public:
	bool mIsUseAi;	//do we use ai?
	int mGameDiff;	//ai difficulty
	int mSpecType;	//-1 if no spectate mode
	bool mIsPlayerFirst;	//player or ai first
	bool lazyLoading;

public:
	QuMenu()
	{
		mSpecType = -1;
		lazyLoading = true;
		mCur = 0;

#ifdef IPAD
		//GOOD
		mItems.push_back(QuMenuItem(lazyLoading)); //0 start
		mItems.back().setImage("images/title1.png");
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH/3,1.5*SCREEN_HEIGHT/4,SCREEN_WIDTH/2.5,SCREEN_HEIGHT/4));
		if(lazyLoading)
			mItems.back().loadImage();


		mItems.push_back(QuMenuItem(lazyLoading)); //1 main menu
		mItems.back().setImage("images/title2.png");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH/4,SCREEN_HEIGHT));
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH/4,0,SCREEN_WIDTH/4,SCREEN_HEIGHT));
		mItems.back().addButton(QuRectangle<int>(2*SCREEN_WIDTH/4,0,SCREEN_WIDTH/4,SCREEN_HEIGHT));
		
		mItems.push_back(QuMenuItem(lazyLoading)); //2 one or 2 players or spectate
		mItems.back().setImage("images/title3.png");
		mItems.back().addButton(QuRectangle<int>(0,3*SCREEN_HEIGHT/4,SCREEN_WIDTH/4,SCREEN_HEIGHT/4));	//1p
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH/4,3*SCREEN_HEIGHT/4,SCREEN_WIDTH/4,SCREEN_HEIGHT/4));	//2p
		mItems.back().addButton(QuRectangle<int>(2*SCREEN_WIDTH/4,SCREEN_HEIGHT/4,SCREEN_WIDTH/4,3*SCREEN_HEIGHT/4)); //spectate
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH-SCREEN_WIDTH/6,0,SCREEN_WIDTH/6,SCREEN_WIDTH/6)); //back button

		mItems.push_back(QuMenuItem(lazyLoading)); //3 diff select
		mItems.back().setImage("images/title4.png");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH/4,SCREEN_HEIGHT*4/5));
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH/4,0,SCREEN_WIDTH/4,SCREEN_HEIGHT*4/5));
		mItems.back().addButton(QuRectangle<int>(2*SCREEN_WIDTH/4,0,SCREEN_WIDTH/4,SCREEN_HEIGHT*4/5));
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH-SCREEN_WIDTH/6,0,SCREEN_WIDTH/6,SCREEN_WIDTH/6)); //back button

		mItems.push_back(QuMenuItem(lazyLoading));	//4 first or seecond
		mItems.back().setImage("images/moves.png");
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH/4,0,SCREEN_WIDTH/4,SCREEN_HEIGHT*3/4));
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH/4,SCREEN_HEIGHT*3/4));
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH-SCREEN_WIDTH/6,0,SCREEN_WIDTH/6,SCREEN_WIDTH/6)); //back button

		mItems.push_back(QuMenuItem(lazyLoading));	//5 spec
		mItems.back().setImage("images/spec.png");
		mItems.back().addButton(QuRectangle<int>(200, 1024-80-331,80,80));
		mItems.back().addButton(QuRectangle<int>(200, 1024-80-472,80,80));
		mItems.back().addButton(QuRectangle<int>(200, 1024-80-613,80,80));

		mItems.back().addButton(QuRectangle<int>(348, 1024-80-331,80,80));
		mItems.back().addButton(QuRectangle<int>(348, 1024-80-472,80,80));
		mItems.back().addButton(QuRectangle<int>(348, 1024-80-613,80,80));
		
		mItems.back().addButton(QuRectangle<int>(491, 1024-80-331,80,80));
		mItems.back().addButton(QuRectangle<int>(491, 1024-80-474,80,80));
		mItems.back().addButton(QuRectangle<int>(491, 1024-80-613,80,80));

		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH-SCREEN_WIDTH/6,0,SCREEN_WIDTH/6,SCREEN_WIDTH/6)); //back button
#else
		mItems.push_back(QuMenuItem(lazyLoading)); //0 start
		mItems.back().setImage("images/title1.png");
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH/3,1.5*SCREEN_HEIGHT/4,SCREEN_WIDTH/2.5,SCREEN_HEIGHT/4));
		if(lazyLoading)
			mItems.back().loadImage();

		mItems.push_back(QuMenuItem(lazyLoading)); //1 main menu
		mItems.back().setImage("images/title2.png");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH/3.5,SCREEN_HEIGHT));
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH/3.5,0,SCREEN_WIDTH/3.5,SCREEN_HEIGHT));
		mItems.back().addButton(QuRectangle<int>(2*SCREEN_WIDTH/3.5,0,SCREEN_WIDTH/3.5,SCREEN_HEIGHT));
		
		mItems.push_back(QuMenuItem(lazyLoading)); //2 one or 2 players or spectate
		mItems.back().setImage("images/title3.png");
		mItems.back().addButton(QuRectangle<int>(0,3*SCREEN_HEIGHT/4,SCREEN_WIDTH/3.5,SCREEN_HEIGHT/4));	//1p
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH/3.5,3*SCREEN_HEIGHT/4,SCREEN_WIDTH/3.5,SCREEN_HEIGHT/4));	//2p
		mItems.back().addButton(QuRectangle<int>(2*SCREEN_WIDTH/3.5,SCREEN_HEIGHT/4,SCREEN_WIDTH/3.5,3*SCREEN_HEIGHT/4)); //spectate
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH-SCREEN_WIDTH/6,0,SCREEN_WIDTH/6,SCREEN_WIDTH/6)); //back button

		mItems.push_back(QuMenuItem(lazyLoading)); //3 diff select
		mItems.back().setImage("images/title4.png");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH/3.5,SCREEN_HEIGHT*4/5));
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH/3.5,0,SCREEN_WIDTH/3.5,SCREEN_HEIGHT*4/5));
		mItems.back().addButton(QuRectangle<int>(2*SCREEN_WIDTH/3.5,0,SCREEN_WIDTH/3.5,SCREEN_HEIGHT*4/5));
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH-SCREEN_WIDTH/6,0,SCREEN_WIDTH/6,SCREEN_WIDTH/6)); //back button

		mItems.push_back(QuMenuItem(lazyLoading));	//4 first or seecond
		mItems.back().setImage("images/moves.png");
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH/3.5,0,SCREEN_WIDTH/3.5,SCREEN_HEIGHT*3/4));
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH/3.5,SCREEN_HEIGHT*3/4));
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH-SCREEN_WIDTH/6,0,SCREEN_WIDTH/6,SCREEN_WIDTH/6)); //back button

		mItems.push_back(QuMenuItem(lazyLoading));	//5 spec
		mItems.back().setImage("images/spec.png");
		//test
		//mItems.back().addButton(QuRectangle<int>(1/float(2)*SCREEN_WIDTH, 1/float(2)*SCREEN_HEIGHT,SCREEN_WIDTH/2,SCREEN_HEIGHT/2));
		//test
		mItems.back().addButton(QuRectangle<int>(70/float(320)*SCREEN_WIDTH, 290/float(480)*SCREEN_HEIGHT,SCREEN_HEIGHT/12,SCREEN_HEIGHT/12));
		mItems.back().addButton(QuRectangle<int>(70/float(320)*SCREEN_WIDTH, 221/float(480)*SCREEN_HEIGHT,SCREEN_HEIGHT/12,SCREEN_HEIGHT/12));
		mItems.back().addButton(QuRectangle<int>(70/float(320)*SCREEN_WIDTH, 150/float(480)*SCREEN_HEIGHT,SCREEN_HEIGHT/12,SCREEN_HEIGHT/12));

		mItems.back().addButton(QuRectangle<int>(142/float(320)*SCREEN_WIDTH, 290/float(480)*SCREEN_HEIGHT,SCREEN_HEIGHT/12,SCREEN_HEIGHT/12));
		mItems.back().addButton(QuRectangle<int>(142/float(320)*SCREEN_WIDTH, 221/float(480)*SCREEN_HEIGHT,SCREEN_HEIGHT/12,SCREEN_HEIGHT/12));
		mItems.back().addButton(QuRectangle<int>(142/float(320)*SCREEN_WIDTH, 150/float(480)*SCREEN_HEIGHT,SCREEN_HEIGHT/12,SCREEN_HEIGHT/12));

		mItems.back().addButton(QuRectangle<int>(214/float(320)*SCREEN_WIDTH, 290/float(480)*SCREEN_HEIGHT,SCREEN_HEIGHT/12,SCREEN_HEIGHT/12));
		mItems.back().addButton(QuRectangle<int>(214/float(320)*SCREEN_WIDTH, 221/float(480)*SCREEN_HEIGHT,SCREEN_HEIGHT/12,SCREEN_HEIGHT/12));
		mItems.back().addButton(QuRectangle<int>(214/float(320)*SCREEN_WIDTH, 150/float(480)*SCREEN_HEIGHT,SCREEN_HEIGHT/12,SCREEN_HEIGHT/12));

		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH-SCREEN_WIDTH/6,0,SCREEN_WIDTH/6,SCREEN_WIDTH/6)); //back button
#endif

		mItems.push_back(QuMenuItem(lazyLoading));	//6 credits
		mItems.back().setImage("images/credits.png");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH,SCREEN_HEIGHT));

		mItems.push_back(QuMenuItem(lazyLoading));	//7 tutorial
		mItems.back().setImage("images/tutorials-01.png");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH,SCREEN_HEIGHT));

		mItems.push_back(QuMenuItem(lazyLoading));	//8 tutorial
		mItems.back().setImage("images/tutorials-02.png");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH,SCREEN_HEIGHT));

		mItems.push_back(QuMenuItem(lazyLoading));	//9 tutorial
		mItems.back().setImage("images/tutorials-03.png");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH,SCREEN_HEIGHT));

		mItems.push_back(QuMenuItem(lazyLoading));	//10 tutorial
		mItems.back().setImage("images/tutorials-04.png");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH,SCREEN_HEIGHT));

		mItems.push_back(QuMenuItem(lazyLoading));	//11 tutorial
		mItems.back().setImage("images/tutorials-05.png");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH,SCREEN_HEIGHT));

	}
	~QuMenu()
	{
	}
	bool isMenuOver()
	{
		return mCur >= mItems.size();
	}
	std::list<QuMenuItem>::iterator getCurrent()
	{
		std::list<QuMenuItem>::iterator it = mItems.begin();
		if(isMenuOver())
			return mItems.end();
		for(int i = 0; i < mCur; i++)
			it++;
		return it;
	}
	void resetMenu()
	{
		mCur = 0;
		mSpecType = -1;
		if(lazyLoading)
			getCurrent()->loadImage();
	}
	void drawSelection()
	{
		if(!isMenuOver())
			getCurrent()->drawSelection();
	}
	void draw()
	{
		if(!isMenuOver())
			getCurrent()->draw();
	}
	void update()
	{
		//no need to update the buttons
	}
	bool back()
	{
		if(isMenuOver())
			return false;		
		if(lazyLoading)
			getCurrent()->unloadImage();
		switch(mCur)
		{
		case 0:
			return true;
			break;
		case 1:	
			mCur = 0;
			break;
		case 2:
			mCur = 1;
			break;
		case 3:
			mCur = 2;
			break;
		case 4:	
			mCur = 3;
			break;
		case 5:
			mCur = 2;
			break;
		case 6: 
			mCur = 1;
		case 7: 
			mCur = 1;
			break;
		case 8: 
			mCur = 7;
			break;
		case 9: 
			mCur = 8;
			break;
		case 10: 
			mCur = 9;
			break;
		case 11: 
			mCur = 10;
			break;
		default: 
			break;
		}
		if(isMenuOver())
			return false;
		getCurrent()->loadImage();
		return false;
	}

	void mouseReleased(int x, int y)
	{
		if(isMenuOver())
			return;		
		unsigned v = getCurrent()->isButtonPressed(x,y);
		//cout << "clicked id " << v << " current menu " << mCur << endl;
		if(v == -1)
			return;
		if(lazyLoading)
			getCurrent()->unloadImage();

		switch(mCur)
		{
		case 0:	//press to start
			mCur = 1;
			break;
		case 1:	//choose
			if( v == 0) //play
				mCur = 2;
			else if (v == 1) //tutorial
				mCur = 7;
			else if (v == 2) //credits
				mCur = 6;
			break;
		case 2:	//one or two players
			if( v == 3)//back
				mCur = 1;
			if( v == 1)	//two
			{
				mIsUseAi = false;
				mCur = mItems.size(); 
				break;
			}
			else if (v == 0)
			{
				mIsUseAi = true;
				mCur = 3;
			}
			else if (v == 2)
				mCur = 5;
			break;

		case 3:	//difficulty select
			if( v == 3)//back
				mCur = 2;
			else if ( v < 3)
			{
				switch (v){
				case 0: mGameDiff = 0; break;
				case 1: mGameDiff = 1; break;
				case 2: mGameDiff = 2; break;
				default: break;}
				mCur = 4;
			}
			break;
		case 4:	//first or second
			if( v == 2) //back
			{
				mCur = 3;
				break;
			}
			mIsPlayerFirst = v; //0 cpu first, 1 player first
			mCur = mItems.size();
			break;
		case 5: //spectate
			if( v == 9){
				mCur = 2;
			}
			else {
				mSpecType = v;
				mCur = mItems.size();
			}
			break;
		case 6: //credits
			mCur = 1; //back
			break;
		case 7: //tutorial 1
			mCur = 8;
			break;
		case 8: //tutorial 2
			mCur = 9;
			break;
		case 9: //tutorial 3
			mCur = 10;
			break;
		case 10: //tutorial 4
			mCur = 11;
			break;
		case 11: //tutorial 5
			mCur = 1;
			break;
		default: break;
		}
		if(isMenuOver())
			return;
		getCurrent()->loadImage();
	}
private:
};

//TODO has undo buttons and music change/mute options if you ever get to that.
//NOTE drawing of the menu items is actually handled by QuOverlayer for convenience (since I wrote that code already to take care of fading and stuff)
class QuGameMenu
{
	QuMenuItem undo, music, mute;
public:
	QuGameMenu()
	{
		undo.addButton(QuRectangle<int>(0,0,SCREEN_WIDTH/4,SCREEN_HEIGHT/4));
		undo.addButton(QuRectangle<int>(0-SCREEN_WIDTH/4,0-SCREEN_HEIGHT/4,SCREEN_WIDTH/4,SCREEN_HEIGHT/4));
	}
	~QuGameMenu()
	{
	}
	void update()
	{
		//no need to update the buttons
	}
	bool pmusic(int x, int y)
	{
		return false;
	}
	bool pmute(int x, int y)
	{
		return false;
	}
	bool pundo(int x, int y)
	{
		return undo.isButtonPressed(x,y);
	}
};

class QuTransition
{
public:
	virtual void update(){}
	virtual void draw(){}
	virtual bool isFinished() = 0;
};

class QuSlidingTransition : QuTransition
{
	QuTintableGuiImage mImg;
	QuTimer mSlideTimer;
	QuTimer mPauseTimer;
	QuArrayInterpolator<float,4> mColor;
public:
	QuSlidingTransition():mSlideTimer(0,10),mPauseTimer(0,8)
	{
		mColor.setBaseValue(THREE_QUARTER_OPAQUE);
		mColor.setTargetValue(FULL_TRANSPARENT);
		mPauseTimer.expire();
	}
	virtual void update()
	{
		if(mPauseTimer.isExpired())
		{
			mSlideTimer--;
		}
		else if(mSlideTimer.isExpired())
		{
			mPauseTimer++;
		}
		else 
		{
			mSlideTimer++;
		}
	}
	void loadImage(std::string filename)
	{
		mImg.loadImage(filename);
	}
	void reset()
	{
		mPauseTimer.reset();
		mSlideTimer.reset();
	}
	virtual void draw()
	{
		//TODO calculate position and stuff
		float y;
		if(!mPauseTimer.isExpired() && !mSlideTimer.isExpired())
			y = (mSlideTimer.getSquareRoot()-1);
		else if(!mPauseTimer.isExpired() && mSlideTimer.isExpired())
			y = 0;
		else
			y = (1-mSlideTimer.getSquare());
		//TODO FIX LEAK
		mImg.setColor(mColor.interpolate(quAbs<float>(y)).d);
		//mImg.draw((y)*SCREEN_WIDTH/2+SCREEN_WIDTH/2,SCREEN_HEIGHT/2,0,250);
		mImg.draw(SCREEN_WIDTH/2,y*SCREEN_HEIGHT/2+ SCREEN_HEIGHT/2,0,250);
	}
	virtual bool isFinished()
	{
		return mPauseTimer.isExpired() && mSlideTimer.getLinear() == 0;
	}
};

class QuOverlayer
{
	std::map<int,QuSlidingTransition> mParts;
	int mPrevPlayer;
public:
	QuOverlayer()
	{
		mPrevPlayer = 0;
		mParts[0] = QuSlidingTransition();
		mParts[0].loadImage("images/red_turn.png");
		mParts[1] = QuSlidingTransition();
		mParts[1].loadImage("images/blue_turn.png");
		
	}
	~QuOverlayer()
	{
	}
	void setOverlay(int player)
	{
		if( player != mPrevPlayer)
			mParts[player].reset();
		mPrevPlayer = player;
	}
	void draw()
	{
		for(std::map<int,QuSlidingTransition>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			if(!i->second.isFinished())
			{
				i->second.draw();
			}
		}
	}
	void update()
	{
		for(std::map<int,QuSlidingTransition>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			i->second.update();
		}
	}
private:
};


class QuBackground
{
	QuArrayInterpolator<float,4> mRed;
	QuArrayInterpolator<float,4> mBlue;
	QuTimer mColorTimer;
	QuTimer mSlideTimer;
	QuTintableGuiImage mImg;
	//TODO make this happen with tinting alpha
	QuTintableGuiImage mArrows;
	char mPreviousPlayer;
	char mPreviousMoveType;
private:
	void setPlayer(char aPlayer)
	{
		if(mPreviousPlayer != aPlayer)
		{
			//not set target color automatically sets the base color
			//if player is red (P1)
			if(aPlayer == 0)
				mImg.setTargetColor(mRed.interpolate(mColorTimer.getLinear()).d);
			else
				mImg.setTargetColor(mBlue.interpolate(mColorTimer.getLinear()).d);
			mColorTimer++;
		}
		mPreviousPlayer = aPlayer;
	}
	void setMoveType(char aMove)
	{
		if(mPreviousMoveType != aMove)
		{
			//turn on arrows
			if(aMove == 1)
				mArrows.setTargetColor(ONE_EIGTH_OPAQUE);
			//turn off arrows
			else
				mArrows.setTargetColor(FULL_TRANSPARENT);
		}
		mPreviousMoveType = aMove;
	}
public:
	QuBackground():mColorTimer(0,STEPS_TILL_FULL_DARK),mSlideTimer(0,8)
	{
		mPreviousMoveType = 0;
		mPreviousPlayer = 0;

		mRed.setBaseValue(STARTING_RED_COLOR);
		mRed.setTargetValue(ENDING_RED_COLOR);
		mBlue.setBaseValue(STARTING_BLUE_COLOR);
		mBlue.setTargetValue(ENDING_BLUE_COLOR);
		mImg.loadImage("images/background_02.png");
		//mArrows.loadImage("images/TURNICON_IPHONE4.png");

		mArrows.setTargetColor(FULL_TRANSPARENT);
		mArrows.setBaseColor(FULL_TRANSPARENT);

		mImg.update();
		mArrows.update();
	}
	~QuBackground()
	{
	}
	void reset()
	{
		mSlideTimer.reset();
		mColorTimer.reset();
		mImg.setTargetColor(mRed.interpolate(mColorTimer.getLinear()).d);
	}
	void setState(QuGameState aState)
	{
		setPlayer(stateToPlayer(aState));
		setMoveType(stateToMoveType(aState));
	}
	void drawBars()
	{
		float SLIDE_RATIO = 0.22;
		int slideAmount = SLIDE_RATIO * SCREEN_WIDTH * mSlideTimer.getSquareRoot();
		QuRectangle<int> r1(0,0,slideAmount,SCREEN_HEIGHT);
		QuRectangle<int> r2(SCREEN_WIDTH-slideAmount,0,slideAmount,SCREEN_HEIGHT);

		float black [4] = {0,0,0,1};
		drawRectangle(r1,black);
		drawRectangle(r2,black);
	}
	void draw()
	{
		mImg.draw();
		//mArrows.draw();

		//draw awesome black bars
		if(mSlideTimer.getTimeSinceStart() > 0)
			drawBars();
	}
	void update()
	{
		if(mPreviousMoveType == 1)
		{
			if(!mSlideTimer.isExpired())
				mSlideTimer++;
		}
		else
			mSlideTimer--;
		mImg.update();
		//mArrows.update();
	}
private:
};