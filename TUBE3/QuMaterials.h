#pragma once
#include  "QuConstants.h"
#include <list>


class QuLights
{
	//lets hope this initializes to zero
	static std::list<int> globalAvail;
	static int getFirstGlobalAvail()
	{
		globalAvail.sort();
		int r = 0;
		for(std::list<int>::iterator i = globalAvail.begin(); i != globalAvail.end(); i++)
			if(r == *i)
				r++;
		return r;
	}
	int mIndex;
private:
	GLenum getLight()
	{
		return GL_LIGHT0+mIndex;
	}
public:
	QuLights()
	{
		mIndex = getFirstGlobalAvail();
		//cout << "setting up light with index " << mIndex << endl;
		globalAvail.push_back(mIndex);
		
		glLightfv(getLight(),GL_DIFFUSE,DEFAULT_LIGHT_DIFFUSE);
		glLightfv(getLight(),GL_SPECULAR,DEFAULT_LIGHT_SPECULAR);
		glLightfv(getLight(),GL_AMBIENT,DEFAULT_LIGHT_AMBIENT);
	}
	~QuLights()
	{
		//just in case
		glDisable(getLight());
		globalAvail.remove(mIndex);
	}

	void enable()
	{
		glEnable(GL_LIGHTING);
		glEnable(getLight());
	}
	void disable()
	{
		glDisable(getLight());
		glDisable(GL_LIGHTING);
	}
private:
};

class QuMaterial
{
	float amb[4];
	float diff[4];
	float spec[4];
	float emis[4];
	float shin;

	GLenum mFace;
public:
	QuMaterial()
	{
		//set all material to their default value
		memcpy(amb,DEFAULT_MAT_AMBIENT,sizeof(GLfloat)*4);
		memcpy(diff,DEFAULT_MAT_DIFFUSE,sizeof(GLfloat)*4);
		memcpy(spec,DEFAULT_MAT_SPECULAR,sizeof(GLfloat)*4);
		memcpy(emis,DEFAULT_MAT_EMISSION,sizeof(GLfloat)*4);
		shin = DEFAULT_MAT_SHININESS;

		mFace = GL_FRONT_AND_BACK;
	}

	void setFace(GLenum aFace)
	{
		//NOTE GL_FRONT and GL_BACK no longer supported in opengles
		if(aFace == GL_FRONT || aFace == GL_BACK || aFace == GL_FRONT_AND_BACK)
			mFace = aFace;
		else
			std::cout << "INVALID FACE ENUM SPECIFIED IN setFace(GLenum aFace) IN QuMaterial CLASS" << std::endl;
	}

	void setParameters(float * aAmb = NULL, float * aDiff = NULL, float * aSpec = NULL, float * aEmis = NULL, float aShin = -1)
	{
		if(aAmb != NULL)
			memcpy(amb,aAmb,sizeof(GLfloat)*4);
		if(aDiff != NULL)
			memcpy(diff,aDiff,sizeof(GLfloat)*4);
		if(aSpec != NULL)
			memcpy(spec,aSpec,sizeof(GLfloat)*4);
		if(aEmis != NULL)
			memcpy(emis,aEmis,sizeof(GLfloat)*4);
		if(aShin != -1)
			shin = aShin;
	}

	//TODO consider making this static? need to change mFace parameter to GL_FRONT_AND_BACK
	//NOTE disable functions here and in general need not be used, use push/pop attribute instead
	void disable()
	{
		glMaterialfv(mFace,GL_AMBIENT,DEFAULT_MAT_AMBIENT);
		glMaterialfv(mFace,GL_DIFFUSE,DEFAULT_MAT_DIFFUSE);
		glMaterialfv(mFace,GL_SPECULAR,DEFAULT_MAT_SPECULAR);
		glMaterialfv(mFace,GL_EMISSION,DEFAULT_MAT_EMISSION);
		glMaterialf(mFace,GL_SHININESS,DEFAULT_MAT_SHININESS);
	}

	void enable()
	{
		glMaterialfv(mFace,GL_AMBIENT,amb);
		glMaterialfv(mFace,GL_DIFFUSE,diff);
		glMaterialfv(mFace,GL_SPECULAR,spec);
		glMaterialfv(mFace,GL_EMISSION,emis);
		glMaterialf(mFace,GL_SHININESS,shin);
	}
private:
};

class QuMaterialPrefabs
{
public:
	static QuMaterial getMaterial(std::string type)
	{
		if(type == "RED")
		{
			QuMaterial r;
			float amb[] = {0.1f,0.1f,0.1f,1};
			float diff[] = {0.7,0.2,0.2,1};
			float spec[] = {1,1,1,1};
			r.setParameters(NULL,diff,spec);
			return r;
		}
		else if(type == "BLUE")
		{
			QuMaterial r;
			//float amb[] = {0.1f,0.1f,0.1f,1};
			float diff[] = {0.2,0.2,0.7,1};
			float spec[] = {1,1,1,1};
			r.setParameters(NULL,diff,spec);
			return r;
		}
		else if(type == "REDHIGH")
		{
			QuMaterial r;
			float amb[] = {0.5f,0.5f,0.5f,1};
			float diff[] = {0.9,0.6,0.6,1};
			r.setParameters(amb,diff);
			return r;
		}
		else if(type == "BLUEHIGH")
		{
			QuMaterial r;
			float amb[] = {0.5f,0.5f,0.5f,1};
			float diff[] = {0.6,0.6,0.9,1};
			r.setParameters(amb,diff);
			return r;
		}
		else if(type == "NEUTRAL")	//TODO make me darker maybe?
		{
			QuMaterial r;
			float amb [] = {0.5,0.5,0.5,1};
			float diff[] = {0.7f,0.7f,0.7f,1};
			r.setParameters(amb,diff);
			return r;
		}
		//cube face materials
		//notation <F/B><D/F><R/N/B> front/back, divet/face, red/blue/neutral
		else if(type == "BFN" || type == "BFR" || type == "BFB" || type == "BDN")
		{
			QuMaterial r;
			float diff [] = {0.8,0.8,0.8,1};
			float amb [] = {0.8,0.8,0.8,1};
			r.setParameters(amb,diff);
			return r;
		}
		else if(type == "FFN" || type == "FFR" || type == "FFB" || type == "FDN")
		{
			QuMaterial r;
			float diff [] = {1.0,1.0,1.0,1};
			float amb [] = {1.0,1.0,1.0,1};
			r.setParameters(amb,diff);
			return r;
		}
		else if(type == "BDR")
		{
			QuMaterial r;
			float diff [] = {0.8,0.8,0.8,1};
			float amb [] = {0.8,0.8,0.8,1};
			float spec [] = {0.5,0,0,1};
			r.setParameters(amb,diff,spec);
			return r;
		}
		else if(type == "BDB")
		{
			QuMaterial r;
			float diff [] = {0.8,0.8,0.8,1};
			float amb [] = {0.8,0.8,0.8,1};
			float spec [] = {0,0,0.5,1};
			r.setParameters(amb,diff,spec);
			return r;
		}
		else if(type == "FDR")
		{
			QuMaterial r;
			float diff [] = {1,1,1,1};
			float amb [] = {1,1,1,1};
			float spec [] = {1,0.5,0.5,1};
			r.setParameters(amb,diff,spec);
			return r;
		}
		else if(type == "FDB")
		{
			QuMaterial r;
			float diff [] = {1,1,1,1};
			float amb [] = {1,1,1,1};
			float spec [] = {0.5,0.5,1,1};
			r.setParameters(amb,diff,spec);
			return r;
		}
		return QuMaterial();
	}
};
