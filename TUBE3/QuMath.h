#pragma once
#include "QuConstants.h"
#include <assert.h>
#include <math.h>

//thanks http://jeffreystedfast.blogspot.com/2008/06/calculating-nearest-power-of-2.html
static uint32_t QuCielPowerOfTwo(uint32_t num)
{
    uint32_t n = num > 0 ? num - 1 : 0;
    n |= n >> 1;
    n |= n >> 2;
    n |= n >> 4;
    n |= n >> 8;
    n |= n >> 16;
    n++;
	if(n > num)
		n *= 2;
    return n;
}

inline double unifRand()
{
    return rand() / double(RAND_MAX);
}
inline float gaussianRand()
{
	float x1, x2, w, y1, y2;
 
	do {
		x1 = 2.0 * unifRand() - 1.0;
		x2 = 2.0 * unifRand() - 1.0;
		w = x1 * x1 + x2 * x2;
	} while ( w >= 1.0 );

	w = sqrt( (-2.0 * log( w ) ) / w );
	y1 = x1 * w;
	y2 = x2 * w;
	return y1;
}
struct QuQuaternion
{
	float x,y,z,w;
	
	//identity quaternion
	QuQuaternion() 
	{
		x = y = z = 0;
		w = 1;
	}

	QuQuaternion(float _w, float _x, float _y, float _z):x(_x),y(_y),z(_z),w(_w)
	{
	}

	//NOTE, this assumes x y z are normalized
	static QuQuaternion fromAngleAxis(float a, float x, float y, float z)
	{
		float sha = sin(a/2);
		float cha = cos(a/2);
		return QuQuaternion(cha,x*sha,y*sha,z*sha);
	}

	const QuQuaternion operator *(const QuQuaternion &q) const	
	{	
		return QuQuaternion(w*q.w - x*q.x - y*q.y - z*q.z,
						  y*q.z - z*q.y + w*q.x + x*q.w,
						  z*q.x - x*q.z + w*q.y + y*q.w,
						  x*q.y - y*q.x + w*q.z + z*q.w);
	}

	const QuQuaternion operator -() const						
	{	return QuQuaternion(-w, -x, -y, -z); }

	const QuQuaternion operator *(float scale) const			
	{	return QuQuaternion(w*scale,x*scale,y*scale,z*scale);		}

	const QuQuaternion operator /(float scale) const
	{	return QuQuaternion(w/scale,x/scale,y/scale,z/scale);	}

	const QuQuaternion operator +(const QuQuaternion &q) const	
	{	return QuQuaternion(w+q.w,x+q.x,y+q.y,z+q.z);	}

	float * convertToM3() const
	{
		//TODO check length is equal to 1 or close to one
		assert(length() > 0.9999 && length() < 1.0001);
		//this is stupid but it saves me time implementing
		float v[9] = {1-2*(y*y+z*z), 2*(x*y-w*z),   2*(x*z+w*y),   
		2*(x*y+w*z),   1-2*(x*x+z*z), 2*(y*z-w*x),   
		2*(x*z-w*y),   2*(y*z+w*x),   1-2*(x*x+y*y)};
		float * r = new float[9];
		memcpy(r,v,sizeof(float)*9);

		return r;
	}

	static inline float dot(const QuQuaternion &q1, const QuQuaternion &q2) 
	{   return q1.x*q2.x + q1.z*q2.z + q1.y*q2.y + q1.w*q2.w;  }

	//! gets the length of this quaternion
	float length() const
	{	return (float)sqrt(dot(*this,*this));   }

	QuQuaternion normalized() const
	{   return  *this/length();  }

	static QuQuaternion lerp(const QuQuaternion &q1, const QuQuaternion &q2, float t) 
	{	return (q1*(1-t) + q2*t).normalized();	}

	static QuQuaternion slerp(const QuQuaternion &q1, const QuQuaternion &q2, float t) 
	{
		QuQuaternion q3;
		float dot = QuQuaternion::dot(q1, q2);

		/*	dot = cos(theta)
			if (dot < 0), q1 and q2 are more than 90 degrees apart,
			so we can invert one to reduce spinning	*/
		if (dot < 0)
		{
			dot = -dot;
			q3 = -q2;
		} else q3 = q2;
		
		if (dot < 0.95f)
		{
			float angle = acosf(dot);
			return (q1*sinf(angle*(1-t)) + q3*sinf(angle*t))/sinf(angle);
		} else // if the angle is small, use linear interpolation								
			return lerp(q1,q3,t);		
	}

	void toEulerAngles(float & heading, float & attitude, float & bank) 
	{
		double test = x*y + z*w;
		if (test > 0.499) { // singularity at north pole
			heading = 2 * atan2(x,w);
			attitude = HALF_PI;
			bank = 0;
			return;
		}
		if (test < -0.499) { // singularity at south pole
			heading = -2 * atan2(x,w);
			attitude = - HALF_PI;
			bank = 0;
			return;
		}
		double sqx = x*x;
		double sqy = y*y;
		double sqz = z*z;
		heading = atan2((float)(2*y*w-2*x*z), (float)(1 - 2*sqy - 2*sqz));
		attitude = asin(2*test);
		bank = atan2(float(2*x*w-2*y*z), float(1 - 2*sqx - 2*sqz));
	}
};

inline QuQuaternion operator /(float f,QuQuaternion v)
{
	return QuQuaternion(v.w,-v.x,-v.y,-v.z)/v.length();
}


//taken from http://www.gamedev.net/community/forums/topic.asp?topic_id=502905
template <typename T>
inline float * convertMatrixToQuaternion(const T * m)
{
	float * q = new float[4];

	const float trace = 1.0f + m[0] + m[4] + m[8];

	if (trace > 0.00001f)
	{
		const float s = sqrt(trace) * 2;
		q[0] = (m[7] - m[5]) / s;
		q[1] = (m[2] - m[6]) / s;
		q[2] = 	(m[3] - m[1]) / s;
		q[3] = 	s / 4;
	}
	else if (m[0] > m[4] && m[0] > m[8])
	{
		const float s = sqrt(1.0f + m[0] - m[4] - m[8]) * 2;
		q[0] = 	s / 4;
		q[1] = 	(m[3] + m[1]) / s;
		q[2] = 	(m[2] + m[6]) / s;
		q[3] = 	(m[7] - m[5]) / s;
	}
	else if (m[4] > m[8])
	{
		const float s = sqrt(1.0f + m[4] - m[0] - m[8]) * 2;
		q[0] = 	(m[3] + m[1]) / s;
		q[1] = 	s / 4;
		q[2] = 	(m[7] + m[5]) / s;
		q[3] = 	(m[2] - m[6]) / s;
	}
	else
	{
		const float s = sqrt(1.0f + m[8] - m[0] - m[4]) * 2;
		q[0] = 	(m[2] + m[6]) / s;
		q[1] = 	(m[7] + m[5]) / s;
		q[2] = 	s / 4;
		q[3] = 	(m[3] - m[1]) / s;
	}
	return q;
}

inline QuQuaternion computeDeviceRotation(float x, float y, float z)
{
	static int zm = 1;
	//pseudo normalize the z axis
	float mag = sqrt(x*x + y*y);
	//cout << "mag: " << mag << " pow: " << pow((1-mag),3) << endl;
	float lambda = pow((quClamp<float>(1-quAbs<float>(mag),0,1)),3);
	z = (z/quAbs<float>(z)) * lambda + z*(1-lambda);
	z = quClamp<float>(z,-1,1);
	//cout << "lambda: " << lambda <<  " adjusted z: " << z << endl;

	if(mag != 0)
	{
		//normalize x and y against themselves
		x = x/mag;
		y = y/mag;
	}

	float sinphi = sin(acos(z));
	//cout << "sinphi: " << sinphi << endl;

	float start [3] = {0,0,1};
	float finish [3] = {sinphi*x,sinphi*y,z};
	printMatrix<float>(finish,1,3);
	//now we want to rotate from (0,0,1) to (sinphi*x, sinphi*y, z)
	float p [3];
	crossProduct(start,finish,p);
	printMatrix<float>(p,1,3);

	float mag2 = magnitude<3>(p);
	//cout << "mag2: " << mag2 << endl;
	if(z < 0) {mag2 = HALF_PI - mag2;}

	//if our rotation is stupid
	if(mag2 < 0.001)
		return QuQuaternion();
	else if(mag2 > HALF_PI-0.001)
		return QuQuaternion(ROOT_TWO_OVER_TWO,ROOT_TWO_OVER_TWO,0,0);
		//return QuQuaternion::fromAngleAxis(mag2,1,0,0);


	normalize(p);
	return QuQuaternion::fromAngleAxis(mag2,p[0],p[1],p[2]);
}