#pragma once
#include "QuDatatypes.h"
#include "QuConstants.h"

//-----------------
//drawing functions
//-----------------
inline void generatePartialIcosphere(float * & v, float * & n, float * & t, int sub)
{
	int count = 4;
	std::vector<float> coords(count*9);
	float partialAngle = QUARTER_PI+0.2;
	float cosphi = cos(partialAngle);
	float sinphi = sin(partialAngle);
	float nexus[3] = {-1,0,0};
	float edges[4][3] = 
	{
		-cosphi,0,sinphi,
		-cosphi,sinphi,0,
		-cosphi,0,-sinphi,
		-cosphi,-sinphi,0
	};
	for(int i = 0; i < 4; i++)
	{
		memcpy(&coords[i*9 + 0*3],edges[i%4],sizeof(float)*3);		
		memcpy(&coords[i*9 + 1*3],nexus,sizeof(float)*3);		
		memcpy(&coords[i*9 + 2*3],edges[(i+1)%4],sizeof(float)*3);		
	}


	for(int i = 0; i < sub; i++)
	{
		std::vector<float> newCoords(count*4*9);
		//for each triangle generate 4 triangels
		for(int j = 0; j < count; j++)
		{
			//first convert coords to triples of vector 3
			std::vector<QuVector3> calc(3);
			calc[0] = QuVector3(coords[j*9],coords[j*9+1],coords[j*9+2]);
			calc[1] = QuVector3(coords[j*9+3],coords[j*9+4],coords[j*9+5]);
			calc[2] = QuVector3(coords[j*9+6],coords[j*9+7],coords[j*9+8]);
			std::vector<QuVector3> mids(3);
			mids[0] = (calc[0]+calc[1])/2;
			mids[1] = (calc[1]+calc[2])/2;
			mids[2] = (calc[2]+calc[0])/2;
			for(int k = 0; k < 3; k++)
				mids[k].normalize();
			std::vector<QuVector3> out(12);

			out[0] = mids[0];
			out[1] = calc[1];
			out[2] = mids[1];

			out[3] = mids[2];
			out[4] = mids[1];
			out[5] = calc[2];

			out[6] = calc[0];
			out[7] = mids[0];
			out[8] = mids[2];

			out[9] = mids[0];
			out[10] = mids[1];
			out[11] = mids[2];

			for(int k = 0; k < 12; k++)
			{
				newCoords[j*4*9+k*3] = out[k].x;
				newCoords[j*4*9+k*3 + 1] = out[k].y;
				newCoords[j*4*9+k*3 + 2] = out[k].z;
			}
		}
		coords = newCoords;
		count = count*4;
	}
	v = new float[coords.size()];
	memcpy(v,&coords[0],sizeof(float)*coords.size());
	/*n = new float[coords.size()];
	for(int i = 0; i < count*3; i++)
	{
		QuVector3 m = QuVector3(v[i*3],v[i*3+1],v[i*3+2]);
		m.normalize();
		n[i*3] = m.x;
		n[i*3+1] = m.y;
		n[i*3+2] = m.z;

	}
	t = (float*)cpcpcpVector<float>((float*)STANDARD_TRIANGLE_UV,3,coords.size()/3);
	*/
	n = makeNormalArrayFromTriangles(v,(int)(coords.size()/3));
}

//puts vertices as GL_TRIANGLES in v, normals in n and texture coordinates in t, makes sub subdivisions
//v n and t should all be NULL pointers
inline void generateIcosphere(float * & v, float * & n, float * & t, int sub)
{
	int count = 8;
	std::vector<float> coords(count*9);
	memcpy(&coords[0],OCTAHEDRON_TRIANGLE_COORDS,count*9*sizeof(float));
	for(int i = 0; i < sub; i++)
	{
		std::vector<float> newCoords(count*4*9);
		//for each triangle generate 4 triangels
		for(int j = 0; j < count; j++)
		{
			//first convert coords to triples of vector 3
			std::vector<QuVector3> calc(3);
			calc[0] = QuVector3(coords[j*9],coords[j*9+1],coords[j*9+2]);
			calc[1] = QuVector3(coords[j*9+3],coords[j*9+4],coords[j*9+5]);
			calc[2] = QuVector3(coords[j*9+6],coords[j*9+7],coords[j*9+8]);
			std::vector<QuVector3> mids(3);
			mids[0] = (calc[0]+calc[1])/2;
			mids[1] = (calc[1]+calc[2])/2;
			mids[2] = (calc[2]+calc[0])/2;
			for(int k = 0; k < 3; k++)
				mids[k].normalize();
			std::vector<QuVector3> out(12);

			out[0] = mids[0];
			out[1] = calc[1];
			out[2] = mids[1];

			out[3] = mids[2];
			out[4] = mids[1];
			out[5] = calc[2];

			out[6] = calc[0];
			out[7] = mids[0];
			out[8] = mids[2];

			out[9] = mids[0];
			out[10] = mids[1];
			out[11] = mids[2];

			for(int k = 0; k < 12; k++)
			{
				newCoords[j*4*9+k*3] = out[k].x;
				newCoords[j*4*9+k*3 + 1] = out[k].y;
				newCoords[j*4*9+k*3 + 2] = out[k].z;
			}
		}
		coords = newCoords;
		count = count*4;
	}
	v = new float[coords.size()];
	memcpy(v,&coords[0],sizeof(float)*coords.size());
	/*n = new float[coords.size()];
	for(int i = 0; i < count*3; i++)
	{
		QuVector3 m = QuVector3(v[i*3],v[i*3+1],v[i*3+2]);
		m.normalize();
		n[i*3] = m.x;
		n[i*3+1] = m.y;
		n[i*3+2] = m.z;

	}*/
	t = (float*)cpcpcpVector<float>((float*)STANDARD_TRIANGLE_UV,3,coords.size()/3);
	n = makeNormalArrayFromTriangles(v,(int)(coords.size()/3));
}

inline void generateSierpinskyIcosphere(float * & v, float * & n, float * & t, int sub)
{
	int count = 8;
	std::vector<float> coords(count*9);
	memcpy(&coords[0],OCTAHEDRON_TRIANGLE_COORDS,count*9*sizeof(float));
	for(int i = 0; i < sub; i++)
	{
		std::vector<float> newCoords(count*4*9);
		//for each triangle generate 4 triangels
		for(int j = 0; j < count; j++)
		{
			//first convert coords to triples of vector 3
			std::vector<QuVector3> calc(3);
			calc[0] = QuVector3(coords[j*9],coords[j*9+1],coords[j*9+2]);
			calc[1] = QuVector3(coords[j*9+3],coords[j*9+4],coords[j*9+5]);
			calc[2] = QuVector3(coords[j*9+6],coords[j*9+7],coords[j*9+8]);
			std::vector<QuVector3> mids(3);
			mids[0] = (calc[0]+calc[1])/2;
			mids[1] = (calc[1]+calc[2])/2;
			mids[2] = (calc[2]+calc[0])/2;
			for(int k = 0; k < 3; k++)
				mids[k].normalize();
			std::vector<QuVector3> out(12);

			out[0] = mids[0];
			out[1] = calc[1];
			out[2] = mids[1];

			out[3] = mids[2];
			out[4] = mids[1];
			out[5] = calc[2];

			out[6] = calc[0];
			out[7] = mids[0];
			out[8] = mids[2];

			out[9] = mids[0];
			out[10] = mids[1];	
			out[11] = mids[1];	//boboboo is here

			for(int k = 0; k < 12; k++)
			{
				newCoords[j*4*9+k*3] = out[k].x;
				newCoords[j*4*9+k*3 + 1] = out[k].y;
				newCoords[j*4*9+k*3 + 2] = out[k].z;
			}
		}
		coords = newCoords;
		count = count*4;
	}
	v = new float[coords.size()];
	memcpy(v,&coords[0],sizeof(float)*coords.size());
}

class QuFunctionTesselator
{
public:
	//TODO intertwine the normal array
	static float * basicTesselator(float *(*f)(float),float *(*g)(float), int steps, std::vector<float> poi)
	{
		//create a list of time steps
		std::vector<float> tv(steps+1);
		for(int i = 0; i < steps; i++)
			tv[i] = i/(float)(steps-1);
		tv[steps] = 2;	//dummy point so we do not need to make special cases in loops to follow

		//note this algorithm replaces points with poi rather than inserts them hence it is possible for one poi to replace another if steps is not large enough
		for(int i = 0; i < poi.size(); i++)
		{
			for(int j = 0; j < steps; j++)
			{
				if(poi[i] >= tv[j] && poi[i] < tv[j+1])
				{
					//see which one is closer 
					if(poi[i] - tv[j] <= tv[j+1] - poi[i])
						tv[j] = poi[i];
					else
						tv[j+1] = poi[i];
				}
			}
		}

		//new we begin constructing our triangle strip point list
		float * r = new float[steps*2*3];
		for(int i = 0; i < steps; i++)
		{
			float * fVal = f(tv[i]);
			float * gVal = g(tv[i]);
			memcpy(r+2*3*i,fVal,sizeof(float)*3);
			memcpy(r+2*3*i+3,gVal,sizeof(float)*3);
			delete [] fVal;
			delete [] gVal;
		}
		return r;
	}

	//draws a square centered at 0,0,0 with width 1
	//note this function will leak if return result is not cleaned up.
	template<int num, int denom>
	static float * square(float t)
	{
		float halfWidth = num/(float)denom/(float)2;
		float * r = new float[3];
		r[0] = 0;
		float * lerpC;
		int sign;
		float offset = int(t*4)*0.25f;
		if(t < 0.25)
		{
			r[1] = -halfWidth;
			lerpC = r+2;
			sign = 1;
		}
		else if(t < 0.5)
		{
			r[2] = halfWidth;
			lerpC = r+1;
			sign = 1;
		}
		else if(t < 0.75)
		{
			r[1] = halfWidth;
			lerpC = r+2;
			sign = -1;
		}
		else
		{
			r[2] = -halfWidth;
			lerpC = r+1;
			sign = -1;
		}

		//special case
		if(offset == 1)
		{
			r[1] = -halfWidth;
			return r;
		}

		*lerpC = sign*(t-offset)*4 - sign*halfWidth;
		return r;
	}
	
	static std::vector<float> squarePoi()
	{
		std::vector<float> r(4);
		r[0] = 0;
		r[1] = 0.25f;
		r[2] = 0.5f;
		r[3] = 0.75f;
		return r;
	}
	
	template<int rNum, int rDenom, int zNum, int zDenom>
	static float* circle(float t)
	{
		float zOffset = zNum/(float)zDenom;
		float radius = rNum/(float)rDenom;
		float * r = new float[3];
		r[0] = zOffset;
		r[2] = radius*cos(t*2*PI-PI*3/4);
		r[1] = radius*sin(t*2*PI-PI*3/4);
		return r;
	}

	static std::vector<float> combinePoi(std::vector<float> v, std::vector<float> u)
	{
		std::vector<float> r;
		for(int i = 0; i < v.size(); i++)
		{
			r[i] = v[i];
		}
		for(int i = 0; i < u.size(); i++)
		{
			r[i+v.size()] = u[i];
		}
		return r;
	}

};