#pragma once
#include <string>
#include <map>
#include "QuUtilities.h"
#include "s3eSound.h"
#include "s3eFile.h"
#include "s3eMemory.h"

class QuSoundStruct
{
	QuTimer mVolumeTimer;
	float mMaxVolume;
	float mMinVolume;
	bool mLoop;
	bool mMultiPlay;
	int mChannel;
	void * mSoundData;
	int32 mFileSize;
	int mHz;
	void loadSound(std::string filename)
	{
		//load the sound
		s3eFile *fileHandle = s3eFileOpen(filename.c_str(), "rb");
			//TODO error in loading??
		mFileSize = s3eFileGetSize(fileHandle);
		mSoundData = s3eMallocBase(mFileSize);
		//memset(mSoundData, 0, mFileSize);
		s3eFileRead(mSoundData, mFileSize, 1, fileHandle);
			//TODO error in reading??
		s3eFileClose(fileHandle);
	}
public:
	static int32 OnEndOfSample(void * _systemData, void* userData)
	{
		return 0;
	}
	QuSoundStruct(std::string filename, int aHz, bool stream, bool aLoop, bool multiplay)
	{
		loadSound(filename);

		mHz = aHz;

		//currently only multiplay supported
		mMultiPlay = true;
		mLoop = aLoop;
		mMaxVolume = 1;
		mMinVolume = 0;
		mChannel = -1;
	}
	~QuSoundStruct()
	{
		s3eFreeBase(mSoundData);
	}
	void play()
	{
		//try and get a free mChannel
		//TODO should use s3eSoundChannelRegister with S3E_CHANNEL_END_SAMPLE so we know when the sound has finished playing
		mChannel =  s3eSoundGetFreeChannel();
		if(mChannel == -1)
			return;
		int repeat = 1;
		if(mLoop)
			repeat = 0;
		s3eSoundChannelSetInt(mChannel,S3E_CHANNEL_RATE,mHz);
		s3eSoundChannelPlay(mChannel,(int16 *)mSoundData,mFileSize/2,repeat,0);
	}
	void stop()
	{
		if(mChannel != -1)
			s3eSoundChannelStop(mChannel);
	}
	float getVolume()
	{
		float t = mVolumeTimer.getLinear();
		return (mMinVolume*(1-t) + mMaxVolume*t);
	}
	void update()
	{
		float t = mVolumeTimer.getLinear();
		if(mChannel != -1)
			s3eSoundChannelSetInt(mChannel,S3E_CHANNEL_VOLUME,getVolume()*S3E_SOUND_MAX_VOLUME);
		mVolumeTimer++;
	}
	void setFade(int time, float max)
	{
		if(mVolumeTimer.isSet())
			mMinVolume = getVolume();
		else mMinVolume = 0;
		mMaxVolume = max;
		mVolumeTimer.setTargetAndReset(time);
	}
	bool isFadingOut()
	{
		return mMaxVolume <= mMinVolume;
	}
	void setLoop(bool looping)
	{
		mLoop = looping;
	}
	void setMultiplay(bool multiplay)
	{
		//TODO
	}
};

class QuSoundManager
{
	static QuSoundManager * mSelf;
	std::map<std::string,QuSoundStruct *> mSounds;
public:
	static QuSoundManager & getRef()
	{
		//NOTE not thread safe...
		if(mSelf == NULL)
			mSelf = new QuSoundManager();
		return *mSelf;
	}
	~QuSoundManager()
	{
		for(std::map<std::string,QuSoundStruct *>::iterator i = mSounds.begin(); i != mSounds.end(); i++)
			delete i->second;
	}
	void loadSound(std::string filename, int aHz, bool stream = false, bool loop = false, bool multiplay = false)
	{
		if (mSounds.find(filename) != mSounds.end())
			return;
		mSounds[filename] = new QuSoundStruct(filename, aHz, stream,loop,multiplay);
	}
	void playSound(std::string filename,bool looping = false)
	{
		if (mSounds.find(filename) != mSounds.end())
		{
			mSounds[filename]->play();
			mSounds[filename]->setLoop(looping);
		}
	}
	void stopSound(std::string filename)
	{
		if (mSounds.find(filename) != mSounds.end())
			mSounds[filename]->stop();
	}

	void update()
	{
		for(std::map<std::string,QuSoundStruct *>::iterator i = mSounds.begin(); i != mSounds.end(); i++)
			i->second->update();
	}
	void setFade(std::string filename, bool isFadeIn)
	{
		if (mSounds.find(filename) != mSounds.end())
		{
			if(isFadeIn)
				mSounds[filename]->setFade(20,1);
			else
				mSounds[filename]->setFade(20,0);
		}
	}
	bool isFadingOut(std::string filename)
	{
		if (mSounds.find(filename) != mSounds.end())
			return mSounds[filename]->isFadingOut();
	}
private:
	QuSoundManager()
	{
	}
};