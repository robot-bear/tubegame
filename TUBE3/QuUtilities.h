#pragma once
#include <map>
#include <vector>
#include <math.h>
#include "s3eDebug.h"
#include "IwGeom.h"
#include "GLES\gl.h"
#include "ug.h"
#include "QuConstants.h"

template <typename T>
struct QuPoint
{
	T x,y;
	QuPoint(T ax, T ay)
	{
		x = ax;
		y = ay;
	}
};

template <typename T>
struct QuRectangle
{
	T x,y,w,h;
	QuRectangle(T ax, T ay, T aw, T ah)
	{
		x = ax;
		y = ay;
		w = aw;
		h = ah;
	}
	bool doesContainPoint(QuPoint<T> pt)
	{
		if(pt.x > x+w || pt.x < x)
			return false;
		if(pt.y > y+h || pt.y < y)
			return false;
		return true;
	}
	bool doesContainPoint(int ax, int ay)
	{
		return doesContainPoint(QuPoint<int>(ax,ay));
	}
};

inline void QuWriteGlError()
{
	s3eDebugTracePrintf(s3eDebugGetErrorString());
}

template <typename T>
inline bool quDoesArrayContain(const T* aArray, const unsigned & aLength, const T & aElement)
{
	for(int i = 0; i < aLength; i++)
		if(aArray[i] == aElement)
			return true;
	return false;
}

template <typename T>
inline T quAbs(const T & aVal)
{
	if( aVal < 0)
		return -aVal;
	return aVal;
}

template <typename T>
inline T quMax(const T & a, const T & b)
{
	return a > b ? a : b;
}

template <typename T>
inline T quMin(const T & a, const T & b)
{
	return a < b ? a : b;
}

template <typename T>
inline T quClamp(const T aVal, T min, T max)
{
	if( aVal < min)
		return min;
	else if (aVal > max)
		return max;
	else
		return aVal;
}

inline float quRandf()
{
	return ((float)rand())/RAND_MAX;
}
inline float quRandfSym()
{
	return -1+2*((float)rand())/RAND_MAX;
}
//returns random int from min inclusive to max exclusive
inline int quRandRange(int min, int max)
{
	return min + rand()%(max-min);
}



//-----------------
//matrix multiplication functions
//note (c.f. pg 104, the red book) m[i][j] in c erfers to ith collumn jth row, whereas it refers to ith row and jth in ogl conventions
//-----------------
template <typename T>
inline T * multiplyMatrices3(const T * m1, const T * m2)
{
	T * r = new T[9];
	for(unsigned i = 0; i < 3; i++)
	{
		for(unsigned j = 0; j < 3; j++)
		{
			//ith row, jth collumn
			r[j*3 + i] = 
				m1[0*3+i]*m2[j*3+0] +
				m1[1*3+i]*m2[j*3+1] +
				m1[2*3+i]*m2[j*3+2];
		}
	}
	return r;
}

template <typename T, int N>
inline T* transformVectorWithMatrix(const T * m, const T * v)
{
	T * r = new T[N];
	for(int i = 0; i < N; i++)
	{
		r[i] = (T)0;
		for(int j = 0; j < N; j++)
			r[i] += v[j] * m[i*N + j];	//ith row jth collumn
	}
	return r;
}

template <typename T, int N>
inline T * applyMatrix(const T * m, T * & v)
{
	T * o = v;
	delete [] v;
	v = transformVectorWithmatrix(m,o);
	delete [] o;
	return v;
}

template <typename T>
inline float * convertM3ToM4(const T * m)
{
	T * r = new T[16];
	for(unsigned i = 0; i < 3; i++)
		for(unsigned j = 0; j < 3; j++)
			r[i*4 + j] = m[i*3 + j];		//do I need to reverse this?

	//make sure everything else is zero
	r[3] = r[7] = r[11] = r[12] = r[13] = r[14] = 0;
	r[15] = 1;
	return r;
}
template<typename T,int W>
inline T * getSubMatrix(const T * m, int x, int y, int w,  int h)
{
	T * r = new T[w*h];
	for(int i = 0; i < h; i++)
		for(int j = 0; j < w; j++)
			r[i*w + j] = m[(i+x)*W +(j+y)];
	return r;
}

inline float * convertM4toM3(const float * m)
{
	return getSubMatrix<float,4>(m,0,0,3,3);
}

template<typename T, int R, int C>
inline T * transposeMatrix(const T * m)
{
	T * r = new T[R*C];
	for(int i = 0; i < R; i++)
		for(int j = 0; j < C; j++)
			r[i*C + j] = m[j*R + i];
	return r;
}

template<typename T, typename R, int N>
inline R * convertMatrixType(const T * m)
{
	R * r = new R[N];
	for(int i = 0; i < N; i++)
		r[i] = (R)m[i];
	return r;
}

template<typename T, int N>
inline float * lerpMatrices(const T * m1, const T * m2, float aRatio)
{
	float * r = new float[N];
	for(int i = 0; i < N; i++)
		r[i] = (1-aRatio)*m1[i] + aRatio*m2[i];
	return r;
}

template<typename T>
inline void printMatrix(const T * m, unsigned r, unsigned c)
{
	//std::cout << std::setw(5);
	//std::cout << std::setprecision(3);
	for(int i = 0; i < r; i++)
	{
		for(int j = 0; j < c; j++)
			std::cout << m[i*c + j] << "         ";
		std::cout << '\n';
	}
	std::cout << std::endl;
}

//-----------------
//vector functions
//-----------------
//computes u x v
inline float * crossProduct(const float * u, const float * v)
{
	float * r = new float [3];
	r[0] = u[1]*v[2] - u[2]*v[1];
	r[1] = -u[0]*v[2] + u[2]*v[0];
	r[2] = u[0]*v[1] - u[1]*v[0];
	return r;
}
inline float * crossProduct(const float * u, const float * v, float * r)
{
	r[0] = u[1]*v[2] - u[2]*v[1];
	r[1] = -u[0]*v[2] + u[2]*v[0];
	r[2] = u[0]*v[1] - u[1]*v[0];
	return r;
}

template<int N>
inline float magnitude(float * v)
{
	float r = 0;
	for(int i = 0; i < N; i++)
		r += v[i] * v[i];
	return sqrt(r);
}
//sets v = v/|v|
inline float * normalize(float * v)
{
	float m = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
	for(int i = 0; i < 3; i++)
		v[i] /= m;
	return v;
}

inline float * multiply(const float c, float * r)
{
	for(int i = 0; i < 3; i++)
		r[i] *= c;
	return r;
}

//returns u - v
inline float * substract (const float * u, const float * v)
{
	float * r = new float[3];
	for(int i = 0; i < 3; i++)
		r[i] = u[i] - v[i];
	return r;
}

inline float * substract (const float * u, const float * v, float * r)
{
	for(int i = 0; i < 3; i++)
		r[i] = u[i] - v[i];
	return r;
}

template<typename T, int N>
inline T dot(const T * v1, const T * v2)
{
	T r = (T)0;
	for(int i = 0; i < N; i++)
		r += v1[i]*v2[i];
	return r;
}

template<typename T>
inline T * cpcpcpVector(const float * data, const int count, const int copyCount)
{
	T * r = new T[copyCount * count];
	for(int i = 0; i < copyCount; i++)
		memcpy(r+count*i,data,count*sizeof(T));
	return r;
}

//-----------------
//triangle strip functions
//-----------------
template <typename T>
inline float * makeNormalArrayFromTriangleStrip(T * data, int count, int size = 3)
{
	//first and last triangle have their normals represented twice
	//NOTE this version swaps the first and last normals which seems to fix a funky graphical glitch. This makes no sense, revert to commented version for something that does make sense
	//equivalently, size is stride in gl terms

	float * u = new float [3];
	float * v = new float [3];
	float * r = new float[count*size];

	multiply(-1,normalize(crossProduct( substract(data+0,data+size,u),substract(data+size,data+2*size,v),r+size*(count-1)/*r+0*/)));
	for(int i = 1; i < count-1; i++)
	{
		float * z = normalize(crossProduct( substract(data+size*(i-1),data+size*i,u),substract(data+size*i,data+size*(i+1),v),r+i*size));
		if(i % 2)
			multiply(-1,z);

	}
	float * z = normalize(crossProduct(substract(data+size*(count-3),data+size*(count-2),u),substract(data+size*(count-2),data+size*(count-1),v),r/*r+size*(count-1)*/));
	if(count % 2)
		multiply(-1,z);

	delete [] u;
	delete [] v;
	return r;
}

template<typename T>
inline float * makeNormalArrayFromTriangles(T * data, int count, int size = 3)
{
	float * u = new float [3];
	float * v = new float [3];
	float * r = new float[count*size];

	for(int i = 0; i < count; i+=3)
	{
		normalize(crossProduct( substract(data+size*i,data+size*(i+1),v), substract(data+size*(i+2),data+size*(i+1),u),r+i*size));
		memcpy(r+i*size+3,r+i*size,sizeof(float)*size);
		memcpy(r+i*size+6,r+i*size,sizeof(float)*size);
	}

	delete [] u;
	delete [] v;
	return r;
}




//-----------------
//array functions
//-----------------

//translates THEN scales
template<int N>
inline void operateAndReplaceVector(float * data, int count, float aScale, float aTrans[N], int start = 0, int stride = 0)
{
	for(int i = 0; i < count; i++)
	{
		int ind = start + N * i+ i * stride;
		for(int j = 0; j < N; j++)
			data[ind+j] = data[ind+j]*aScale + aTrans[j];
	}
}

template<typename T>
inline T * joinArrays(const T ** data, const int * dataCount, const int count)
{
	throw QuNotImplementedException();
}

//-----------------
//texture functions
//-----------------
inline void quActivateTexture(GLuint id)
{ 
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,id);
}

inline void quDeactivateTexture(GLuint id=0)
{
	glDisable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,0);
}


//-----------------
//testing classes and functions
//-----------------
//TODO this should implement critically damped SHO 
class QuAccelSimulator
{
	float mX, mY, mStep;
	bool u,d,l,r;
public:
	QuAccelSimulator()
	{
		mX = 0;
		mY = 0;
		mStep = 0.03f;
		u = d = l = r = false;
	}
	float getX()
	{
		return mX;
	}
	float getY()
	{
		return mY;
	}
	void update()
	{
		if(u)
			mY += mStep;
		else if(d)
			mY -= mStep;
		else if(mY != 0)
			mY -= mStep*mY/quAbs<float>(mY);
		if(l)
			mX -= mStep;
		else if(r)
			mX += mStep;
		else if(mX != 0)
			mX -= mStep*mX/quAbs<float>(mX);
		
		if(quAbs<float>(mY) < mStep)
			mY = 0;
		if(quAbs<float>(mX) < mStep)
			mX = 0;
	}
	void keyReleased(int key)
	{
		switch(key)
		{
		case UG_KEY_UP:
			u = false;
			break;
		case UG_KEY_DOWN:
			d = false;
			break;
		case UG_KEY_LEFT:
			l = false;
			break;
		case UG_KEY_RIGHT:
			r = false;
			break;
		default:
			break;
		}
	}
	void keyPressed(int key)
	{
		switch(key)
		{
		case UG_KEY_UP:
			u = true;
			break;
		case UG_KEY_DOWN:
			d = true;
			break;
		case UG_KEY_LEFT:
			l = true;
			break;
		case UG_KEY_RIGHT:
			r = true;
			break;
		default:
			break;
		}
	}
private:
};

const unsigned QU_DOUBLE_TAP_TIME = 5;
class QuDoubleTap
{
	std::map<int,int> mKeyMap;
public:
	bool setKeyAndCheck(int key)
	{
		if(mKeyMap.count(key) && mKeyMap[key] != -1)
		{
			mKeyMap.erase(key);
			return true;
		}
		mKeyMap[key] = 0;
		return false;
	}
	void update()
	{
		for(std::map<int,int>::iterator i = mKeyMap.begin(); i != mKeyMap.end(); i++)
		{
			if(i->second == -1)
				return;
			if(i->second < QU_DOUBLE_TAP_TIME)
				i->second += 1;
			else
				//flag for deletion
				i->second = -1;
		}
	}
};

class QuAccelManager
{
	float pY,pX,pZ;
public:
	QuAccelManager()
	{
		pX = pY = 1;
		pZ = 1;
	}
	void update(float x, float y, float z)
	{
		//TODO add some fancy algorithm in here
		pX = x;
		pY = y;
		pZ = z;
	}
	bool notiff(bool A, bool B)
	{
		return (A&&!B)||(B&&!A);
	}
	//see if this one works first, saves you some trouble
	float getX()
	{
		return notiff((pZ > 0),(pY > 0)) ? pX : 2-pX;
	}
	float getY()
	{
		return notiff((pZ > 0),(pX > 0))  ? pY : 2-pY;
	}
};

struct QuTimer
{
	
	QuTimer(int aStart = 0, int aTarget = 0):mStart(aStart),mCurrent(aStart),mTarget(aTarget)
	{}
	bool isSet()
	{
		return mTarget != mStart;
	}
	void update()
	{
		mCurrent++;
	}
	QuTimer & operator++(int unused)
	{
		update();
		return *this;
	}
	QuTimer & operator--(int unused)
	{
		if(mCurrent > 0)
			mCurrent--;
		return *this;
	}
	void setTarget(int aTarget)
	{
		mTarget = aTarget;
	}
	void reset()
	{
		mCurrent = mStart;
	}
	void setTargetAndReset(int aTarget)
	{
		setTarget(aTarget);
		reset();
	}
	void expire()
	{
		mCurrent = mTarget;
	}
	//timer expires when mCurrent is >= mTarget
	bool isExpired()
	{
		return mCurrent >= mTarget;
	}
	int getTimeSinceStart()
	{
		return mCurrent-mStart;
	}
	float getLinear()
	{
		if(isExpired())
			return 1;
		return (float)(mCurrent-mStart)/(float)(mTarget-mStart);
	}
	float getLinear(float l, float r)
	{
		return getLinear()*(r-l)+l;
	}
	float getSquareRoot()
	{
		return sqrt(getLinear());
	}
	float getSquare()
	{
		float r = getLinear();
		return r*r;
	}
	float getCubed(float l = 0, float r = 1)
	{
		float v = getLinear();
		return v*v*v*(r-l)+l;
	}
	//0 and 0 and 1, max at 0.5
	float getUpsidedownParabola()
	{
		float x = getLinear();
		return (-(x-0.5)*(x-0.5) + 0.25)*4;
	}
	//1 at 0 and 1, 0 at 0.5
	float getParabola(float l = 0, float r = 1)
	{
		return (1-getUpsidedownParabola())*(r-l)+l;
	}
private:
	int mStart,mCurrent,mTarget;
};

struct QuMouseDragged
{
	int dx, dy;
	int cx, cy;
	bool isMouseDown;
	QuTimer mTimer;
	float mThreshSquared;
	void setThresh(float aThresh)
	{
		mThreshSquared = aThresh*aThresh;
	}
	QuMouseDragged():mTimer(0,15)
	{
		dx = dy = cx = cy = 0;
		setThresh(50);
		isMouseDown = false;
	}
	float getChangeNorm()
	{
		QuPoint<float> c = getChange();
		return sqrt(c.x*c.x + c.y*c.y);
	}
	QuPoint<float> getChange()
	{
		if(isMouseDown)
			return QuPoint<float>((dx-cx),(dy-cy));
		return QuPoint<float>(0,0);
	}
	QuTurnDirections mouseReleased(int x, int y)
	{
		if(mTimer.isExpired())
			return IDENTITY_DIRECTION;
		isMouseDown = false;
		int deltax = (dx-x);
		int deltay = (dy-y);
		//test if threshold is exceeded
		if(deltax*deltax + deltay*deltay < mThreshSquared)
			return IDENTITY_DIRECTION;
		if(quAbs(deltax) >= quAbs(deltay))
		{
			if(deltax > 0)
				return RIGHT;
			else return LEFT;
		}
		else
		{
			if(deltay > 0)
				return DOWN;
			else return UP;
		}
	}
	void mousePressed(int x, int y)
	{
		mTimer.reset();
		cx = dx = x;
		cy = dy = y;
		isMouseDown = true;
	}
	void mouseDragged(int x, int y)
	{
		cx = x; cy = y;
	}
	void mouseMoved(int x, int y)
	{
		if(isMouseDown)
			mouseDragged(x,y);
	}
	void update()
	{
		mTimer++;
	}
};

template<typename T, int N>
class QuArrayInterpolator
{
	T mBaseColor[N];
	T mTargetColor[N];
public:
	void setBaseValue(T aColor[N])
	{
		for(int i = 0; i < N; i++)
			mBaseColor[i] = aColor[i];
	}
	void setTargetValue(T aColor[N])
	{
		for(int i = 0; i < N; i++)
			mTargetColor[i] = aColor[i];
	}
	struct ARRAY
	{
	   T d[N];
	};
	ARRAY interpolate(float t)
	{
		if(t < 0) t = 0;
		if(t > 1) t = 1;
	
		ARRAY r;
		for(int i = 0; i < N; i++)
			r.d[i] = mTargetColor[i]*(t) + mBaseColor[i]*(1-t);
		return r;
	}
private:
};

//simple harmonic oscillator using (backwards!) euler
class QuSho
{
	float xNot,k,b,m,x,v,dt;
public:
	QuSho()
	{
		xNot = 0;
        k = 1.8;
        m = 0.6;
        b = 1;
        dt = 0.1;
        x = 0;
        v = 0;
	}
	float getXNot(){ return xNot; }
	void setXNot(float aXNot)
	{
		xNot = aXNot;	
	}
	void setX(float ax)
	{
		x = ax;
	}
	void addV(float av)
	{
		v += av;
	}
	void setV(float av)
	{
		v = av;
	}
	float update()
	{
		float nx, nv;
		nx = x + dt * v;
		nv = (xNot-nx)*k*dt/m + v*(1-b*dt/m);
		x = nx;
		v = nv;
		return x;
	}
	float updateWithFakeCriticalDamping()
	{
		float pX = x;
		update();
		//TODO Make this more precise to handle jumps in xNot
		if( (pX < xNot && x >= xNot) || (pX > xNot && x <= xNot) )
		{
			if(quAbs(v) < 1)
			{
				x = xNot;
				v = 0;
			}
		}
		return x;
	}
	float getValue()
	{
		return x;
	}
private:
	
};


//BROKEN!!! FIX ME
class QuGaussianSho
{
	float xNot,k,b,m,x,v,dt;
public:
	QuGaussianSho()
	{
		xNot = 0;
		k = 1.2;
		m = 0.8;
		b = 0.7;
		dt = 0.1;
		x = 0;
		v = 0;
	}
	void setXNot(float aXNot)
	{
		xNot = aXNot;
		
	}
	void setX(float ax)
	{
		x = ax;
	}
	void addV(float av)
	{
		v += av;
	}
	void setV(float av)
	{
		v = av;
	}
	float update()
	{
		float nx, nv;
		nx = x + dt * v;
		nv = v + (xNot-nx)*k*dt/m + -v*b*dt/m;
		x = nx;
		v = nv;
		return x;
	}
	float updateWithFakeCriticalDamping()
	{
		float pX = x;
		update();
		//TODO Make this more precise to handle jumps in xNot
		if( (pX < xNot && x >= xNot) || (pX > xNot && x <= xNot) )
		{
			if(quAbs(v) < 1)
			{
				x = xNot;
				v = 0;
			}
		}
		return x;
	}
	float getValue()
	{
		return x;
	}
private:

};

inline void QuTransparentColor(GLfloat r[4], float percent)
{
	if(percent > 1)
		percent = 1;
	if(percent < 0)
		percent = 0;
	r[0] = 1;
	r[1] = 1;
	r[2] = 1;
	r[3] = percent;
}

//---------------
//transformation functios
//---------------
inline void quScale2d(float s)
{
	glScalef(s,s,1);
}
inline void quScale(float s)
{
	glScalef(s,s,s);
}

//--------------
//projection model function
//--------------
inline void QuPerspective(double fovy, double aspect, double zNear, double zFar)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

   double xmin, xmax, ymin, ymax;

   ymax = zNear * tan(fovy * PI / 360.0);
   ymin = -ymax;
   xmin = ymin * aspect;
   xmax = ymax * aspect;


   glFrustumf(xmin, xmax, ymin, ymax, zNear, zFar);
   
   
   
	glMatrixMode(GL_MODELVIEW);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	

	glDepthMask(GL_TRUE);
}

inline void QuOrthographic()		   
{                   
	glMatrixMode(GL_PROJECTION);		   
	glLoadIdentity();					   
	glOrthof( 0, 320, 480, 0, 1, 0 );				
	glMatrixMode(GL_MODELVIEW);				
	glLoadIdentity();					
	glDepthMask(GL_FALSE);
}

inline void QuLookAt(GLfloat eyex, GLfloat eyey, GLfloat eyez,
           GLfloat centerx, GLfloat centery, GLfloat centerz,
           GLfloat upx, GLfloat upy, GLfloat upz)
 {
    GLfloat m[16];
    GLfloat x[3], y[3], z[3];
    GLfloat mag;

    /* Make rotation matrix */

    /* Z vector */
    z[0] = eyex - centerx;
    z[1] = eyey - centery;
    z[2] = eyez - centerz;

    mag = sqrt(z[0] * z[0] + z[1] * z[1] + z[2] * z[2]);

    if (mag) {                   /* mpichler, 19950515 */
       z[0] /= mag;
       z[1] /= mag;
       z[2] /= mag;
    }

    /* Y vector */
    y[0] = upx;
    y[1] = upy;
    y[2] = upz;

    /* X vector = Y cross Z */
    x[0] = y[1] * z[2] - y[2] * z[1];
    x[1] = -y[0] * z[2] + y[2] * z[0];
    x[2] = y[0] * z[1] - y[1] * z[0];

    /* Recompute Y = Z cross X */
    y[0] = z[1] * x[2] - z[2] * x[1];
    y[1] = -z[0] * x[2] + z[2] * x[0];
    y[2] = z[0] * x[1] - z[1] * x[0];

 
    /* mpichler, 19950515 */
    /* cross product gives area of parallelogram, which is < 1.0 for
     * non-perpendicular unit-length vectors; so normalize x, y here
     */

    mag = sqrt(x[0] * x[0] + x[1] * x[1] + x[2] * x[2]);

    if (mag) {
       x[0] /= mag;
       x[1] /= mag;
       x[2] /= mag;
    }

    mag = sqrt(y[0] * y[0] + y[1] * y[1] + y[2] * y[2]);

    if (mag) {
       y[0] /= mag;
       y[1] /= mag;
       y[2] /= mag;
    }

 

 #define M(row,col)  m[col*4+row]

    M(0, 0) = x[0];

    M(0, 1) = x[1];

    M(0, 2) = x[2];

    M(0, 3) = 0.0;

    M(1, 0) = y[0];

    M(1, 1) = y[1];

    M(1, 2) = y[2];

    M(1, 3) = 0.0;

    M(2, 0) = z[0];

    M(2, 1) = z[1];

    M(2, 2) = z[2];

    M(2, 3) = 0.0;

    M(3, 0) = 0.0;

    M(3, 1) = 0.0;

    M(3, 2) = 0.0;

    M(3, 3) = 1.0;

 #undef M

    glMultMatrixf(m);

 

    /* Translate Eye to Origin */

    glTranslatef(-eyex, -eyey, -eyez);

 

 }

#ifdef TARGET_OF_IPHONE
#undef glFrustum
#undef glOrtho
#endif