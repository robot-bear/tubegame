#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "ug.h"
#include "QuConstants.h"
#include "testApp.h"
#include "IwGx.h"
#include "s3ePointer.h"
#include "../QuImage.h"
#include "QuSound_STROKE.h"

std::set<QuImage*> QuImage::globalImageList = std::set<QuImage*>();
QuSoundManager * QuSoundManager::mSelf = NULL;


#if defined(GL_VERSION_1_1) || defined(GL_OES_VERSION_1_0)

testApp game;

int32 unpause(void *systemData, void *userData)
{
	QuImage::reloadAllImages();
	return 0;
}
void init(void)
{    
	//TODO register key events
	//IwGxInit();
	//IwResManagerInit();
	//IwSoundInit();
	//IwGetResManager()->AddHandler(new CIwResHandlerWAV);
	s3eAccelerometerStart();
	game.setup();
	s3eDeviceRegister(S3E_DEVICE_UNPAUSE,unpause,NULL); 
}

void destroy()
{
	s3eDeviceUnRegister(S3E_DEVICE_UNPAUSE,unpause); 
	game.destroy();
	s3eAccelerometerStop();
	//IwSoundTerminate();
	//IwResManagerTerminate();
	//IwGxTerminate();
}

void display(UGWindow uwin)
{
	game.update();
	game.draw();
	glFlush();
    ugSwapBuffers(uwin);

	//HACK for quitting 
	if(game.isDone)
	{
		destroy();
		//void * j = s3eJNIGetVM();
		//if(j != NULL)
		//	(JavaVM*)j->
		s3eDeviceExit();
		exit(0);
	}
}

void reshape(UGWindow uwin, int w, int h)
{
}

void keyboard (UGWindow uwin, int key, int x, int y)
{
	if(x)
		game.keyPressed(key);
	else
		game.keyReleased(key);
}

void pointerFunc(UGWindow uwin, int button, int state, int x, int y)
{
	if(s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION) == S3E_SURFACE_BLIT_DIR_ROT180)
	{
		x = SCREEN_WIDTH - x;
		y = SCREEN_HEIGHT - y;
	}
	if(state)
		game.mousePressed(x,y,button);
	else
		game.mouseReleased(x,y,button);
}

void motionFunc(UGWindow w, int x, int y)
{
	x = s3ePointerGetX();
	y = s3ePointerGetY();
	if(s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION) == S3E_SURFACE_BLIT_DIR_ROT180)
	{
		x = SCREEN_WIDTH - x;
		y = SCREEN_HEIGHT - y;
	}
	game.mouseMoved(x,y);
}

void idle(UGWindow uwin)
{
	//insert time loop here
	ugPostRedisplay(uwin);
}
int main(int argc, char** argv)
{
	UGCtx ug = ugInit();
	UGWindow uwin = ugCreateWindow (ug, "UG_DEPTH", argv[0], SCREEN_WIDTH, SCREEN_HEIGHT, 100, 100);
	//initialize ug
	ugDisplayFunc(uwin, display); 
	ugIdleFunc(uwin,idle);
	ugReshapeFunc(uwin, reshape);
	ugKeyboardFunc(uwin, keyboard);
	ugMotionFunc(uwin,motionFunc);
	ugPointerFunc(uwin,pointerFunc);
	init();
	ugMainLoop(ug);
	destroy();
	//cout << "leaving main loop" << endl;
	return 0; 
}
#else
int main(int argc, char** argv)
{
    fprintf (stderr, "This program demonstrates a feature which is not in OpenGL Version 1.0.\n");
    fprintf (stderr, "If your implementation of OpenGL Version 1.0 has the right extensions,\n");
    fprintf (stderr, "you may be able to modify this program to make it run.\n");
    return 0;
}
#endif
