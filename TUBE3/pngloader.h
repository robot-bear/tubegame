#pragma once
#include "png.h"
#include "QuMath.h"
#include "s3eFile.h"
#include "s3eMemory.h"
#include <iostream>

class PngLoader
{
	bool bDone;
	bool bReady;

	//s3e file stuff
	s3eFile *fileHandle;

	//png info
	int mNumberPasses;
	png_structp png_ptr;
	png_infop info_ptr;

	//additional png info
	png_uint_32 mW;
	png_uint_32 mH;
	int potW,potH;
	int mBitDepth;
	int mColorType;
	int bytesPerPixel;
	png_uint_32 bytesPerRow;

	//update cycle info
	int linesRead;

	//image data
	void * imageData;
	png_bytepp rowPointerData;

public:

	static void readDataFromFileHandle(png_structp png_ptr, png_bytep outBytes, png_size_t byteCountToRead)
	{
		if(png_ptr->io_ptr == NULL)
			return;   // add custom error handling here
		s3eFile * file = (s3eFile*)png_ptr->io_ptr;
		const size_t bytesRead = s3eFileRead((void*)outBytes, sizeof(unsigned char), byteCountToRead, file);
		if((png_size_t)bytesRead != byteCountToRead)
		{ 
			return;   // add custom error handling here
		}
	}

	PngLoader()
	{
		imageData = NULL;
		rowPointerData = NULL;
		linesRead = 0;
		bDone = false;
		bReady = false;
	}
	~PngLoader()
	{
		abort();
		deleteImageData();
	}
	void abort()
	{
		if(fileHandle != NULL)
		{
			s3eFileClose(fileHandle);
			fileHandle = NULL;
		}
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
	}
	void deleteImageData()
	{
		if(imageData != NULL)
		{
			s3eFree( imageData );
			imageData = NULL;
		}
		if(rowPointerData != NULL)
		{
			s3eFree( rowPointerData );
			rowPointerData = NULL;
		}
	}
	bool setFile(std::string filename)
	{
		//load the file into memory
		//TODO implement incremental file reading if needed
		fileHandle = s3eFileOpen(filename.c_str(), "rb");
		//TODO error in loading??
		
		const int sigl = 8;
		unsigned char header[sigl];
		s3eFileRead(header, sigl*sizeof(char), 1, fileHandle);
		//TODO error in reading??
		if(!png_check_sig((unsigned char *)header,sigl))
		{
			return false;
		}

		//setup the png for reading
		png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,NULL,NULL,NULL);
		if(png_ptr == NULL){
			abort();
			return false;
		}
		info_ptr = png_create_info_struct(png_ptr);
		if (info_ptr == NULL){
			abort();
			return false;
		}
		if (setjmp(png_jmpbuf(png_ptr)))
		{
			abort();
			assert(false);
			return false;
		}

		//now set our read function
		png_set_read_fn(png_ptr, fileHandle, readDataFromFileHandle);
		
		//already read this much from headr
		png_set_sig_bytes(png_ptr, sigl);

		//lets just try and read in the whole image for now
		//png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY , NULL);

		//okay we are doing things the hard way
		//interlacing
		/*mNumberPasses = png_set_interlace_handling(png_ptr);
		if(mNumberPasses != 1)
		{
			abort();
			s3eDebugTracePrintf("oh no interlaced png encountered");
			return false;
		}*/		

		//parse the header info
		png_read_info(png_ptr, info_ptr);
		png_uint_32 retval = png_get_IHDR(png_ptr, info_ptr,
		   &mW,
		   &mH,
		   &mBitDepth,
		   &mColorType,
		   NULL, NULL, NULL);
		bytesPerRow = png_get_rowbytes(png_ptr, info_ptr);
		bytesPerPixel = bytesPerRow/mW;

		if(retval != 1)
		{
			abort();
			return false;
		}

		//compute potw/h
		potW = QuCielPowerOfTwo(mW);
		potH = QuCielPowerOfTwo(mH);
		std::cout << potW << " " << potH << std::endl;
		
		imageData = s3eMalloc(potW*potH*bytesPerPixel*sizeof(unsigned char));
		rowPointerData = (png_bytepp)s3eMalloc(mH*sizeof(png_byte *));
		for(int i = 0; i < mH; i++)
			rowPointerData[i] = (png_byte *)imageData+bytesPerPixel*potW*i;

		//READY TO GO
		bReady = true;
		return true;
	}
	bool update(int rowsToRead)
	{
		if(bDone)
			return true;
		rowsToRead = MIN(mH-linesRead,rowsToRead);
		//TODO fix this
		png_read_rows(png_ptr,rowPointerData + linesRead,NULL,rowsToRead);
		linesRead += rowsToRead;
		if(linesRead == mH)
		{
			bDone = true;
			abort();
			return true;
		}
		return false;
	}
	int getWidth() { return mW; }
	int getHeight() { return mH; }
	int getPotWidth() { return potW; }
	int getPotHeight() { return potH; }
	float getWidthRatio() const { return mW/(float)potW; }
	float getHeightRatio() const { return mH/(float)potH; }
	int getImageTexelSize()
	{
		return potW*potH*bytesPerPixel;
	}
	unsigned char * getImageData()
	{
		return (unsigned char *)imageData;
	}
	bool isPngDoneLoading()
	{
		return bDone;
	}
	GLenum getFormat()
	{
		switch(mColorType)
		{
		case PNG_COLOR_TYPE_RGB:
			return GL_RGB;
		case PNG_COLOR_TYPE_RGB_ALPHA:
			return GL_RGBA;
		default:
			return GL_RGB;
		}
	}
	bool isPngReadyToLoad()
	{
		return bReady;
	}
private:

};