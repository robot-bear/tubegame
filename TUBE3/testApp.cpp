

#include "testApp.h"
#include "QuDrawer.h"
#include "QuInterface.h"
#include "QuSound_STROKE.h"
#include "QuConstants.h"
#include "s3eKeyboard.h"
//--------------------------------------------------------------

void testApp::initialize()
{
	mTouchEffect = new QuTouchEffect;
	//QuSoundManager::getRef().loadSound(SOUND_SELECT,SOUND_SELECT_RATE,false,false,true);
}
void testApp::setup(){
}

void testApp::drawMenu()
{
}
void testApp::drawGame()
{
}
void testApp::updateMenu()
{
}

void testApp::restartGame()
{
	//QuSoundManager::getRef().stopSound(SOUND_BG);
	isMenu = true;
}

void testApp::updateGame()
{
}

void testApp::pressMenu(int x, int y)
{
}

void testApp::releaseMenu(int x, int y)
{
}

void testApp::releaseGame(int x, int y)
{
}
void testApp::pressGame(int x, int y)
{
}

void testApp::drawDummyLoadingImage()
{
	QuTintableGuiImage img;
	img.loadImage("images/image.png");
	img.draw();
}

void testApp::update()
{
	QuSoundManager::getRef().update();
	if(hasLoaded == 1)
	{
		initialize(); hasLoaded++;
	}
	else if(hasLoaded == 2)
	{
		mTouchEffect->update();
		if(isMenu)
			updateMenu();
		else
			updateGame();
	}
}

void testApp::draw(){
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	if(hasLoaded == 0)
	{
		drawDummyLoadingImage(); 
		hasLoaded++;
		return;
	}
	if(isMenu)
		drawMenu();
	else
		drawGame();
	mTouchEffect->draw();
}

void testApp::keyPressed(int key){
}

void testApp::keyReleased(int key){
	if(key == s3eKeyAbsBSK)
	{
		restartGame();
	}
}
 
void testApp::mousePressed(int x, int y, int button){
	if(!isMenu)
		pressGame(x,y);
	if(s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION) != S3E_SURFACE_BLIT_DIR_ROT180)
	{
		x = SCREEN_WIDTH - x;
		y = SCREEN_HEIGHT - y;
	}
	if(isMenu)
		pressMenu(x,y);
	mTouchEffect->touch(x,y);
}

void testApp::mouseReleased(int x, int y, int button){
	if(isMenu)
	{
		if(s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION) != S3E_SURFACE_BLIT_DIR_ROT180)
		{
			x = SCREEN_WIDTH - x;
			y = SCREEN_HEIGHT - y;
		}
		releaseMenu(x,y);
	}
	else
		releaseGame(x,y);
}

void testApp::mouseMoved(int x, int y)
{
}


//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
	//reset screen center
}

void testApp::tiltUpdate()
{
	float y = s3eAccelerometerGetY()/1000.0;
	float x = s3eAccelerometerGetX()/1000.0;
	if(s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION) == S3E_SURFACE_BLIT_DIR_ROT180)
	{
		x*=-1; y*=-1;
	}
	//mCam->setTiltTarget(mAccel.getY(),mAccel.getX());
	//mCam->adjustTiltTarget(-mMouse.getChange().x/100.,mMouse.getChange().y/200.);
}
