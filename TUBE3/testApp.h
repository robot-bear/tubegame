
#pragma once
#include "QuUtilities.h"
#include "QuCamera.h"
#include "QuDrawer.h"
#include "QuInterface.h"
class testApp
{
private:
	bool isMenu;
	int hasLoaded;
	QuTouchEffect * mTouchEffect;
public:
	bool isDone;
	testApp():hasLoaded(0),isMenu(true)
	{
	}
	~testApp()
	{
		delete mTouchEffect;
	}
	void destroy()
	{
	}
	

	void drawDummyLoadingImage();
	void restartGame();

	void drawMenu();
	void drawGame();
	void updateMenu();
	void updateGame();
	void releaseMenu(int x, int y);
	void releaseGame(int x, int y);
	void pressGame(int x, int y);
	void pressMenu(int x, int y);

	void initialize();
	void setup();
	void update();
	void draw();

	void keyPressed  (int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button){}
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);

	void tiltUpdate();
};