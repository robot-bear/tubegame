/*
 * GLESonGL implementation
 * Version:  1.2
 *
 * Copyright (C) 2003  David Blythe   All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * DAVID BLYTHE BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <GLES/egl.h>

#include "s3e.h"
#include "ug.h"

#ifndef DEBUG
#define DEBUG(x)
#endif

typedef struct
{
    void (*idle)(UGWindow w);
    struct ugwindow* win, * winlist;
    EGLDisplay egldpy;
	int redraw;
} UGCtx_t;

typedef struct ugwindow {
    int width, height;
    int redraw;
    void (*draw)(UGWindow w);
    void (*reshape)(UGWindow w, int width,int height);
    void (*kbd)(UGWindow w, int key, int x, int y);
    void (*pointer)(UGWindow w, int button, int state, int x, int y);
    void (*motion)(UGWindow w, int x, int y);
    UGCtx_t* ug;
    struct ugwindow* next, * prev;
    EGLSurface surface;
    EGLConfig eglconfig;
    EGLContext eglctx;
} UGWindow_t;

static UGCtx_t * context;

int32 kbcb(void* systemData, void* userData)
{
	s3eKeyboardEvent* event = (s3eKeyboardEvent*)systemData;
	UGCtx_t* ug = (UGCtx_t*)userData;
	if (ug->win->kbd)
		ug->win->kbd((UGWindow)ug->win, event->m_Key, event->m_Pressed, 0 );
	return 0;
}

int32 mbcb(void* systemData, void* userData)
{
	s3ePointerEvent* event = (s3ePointerEvent*)systemData;
	UGCtx_t* ug = (UGCtx_t*)userData;
	if(ug->win->pointer)
		ug->win->pointer((UGWindow)ug->win,event->m_Button,event->m_Pressed,event->m_x,event->m_y);
	return 0;
}

int32 mmcb(void* systemData, void* userData)
{
	s3ePointerEvent* event = (s3ePointerEvent*)systemData;
	UGCtx_t* ug = (UGCtx_t*)userData;
	if(ug->win->motion)
		ug->win->motion((UGWindow)ug->win,event->m_x,event->m_y);
	return 0;
}


UGCtx ugInit(void)
{
    UGCtx_t* ug = malloc(sizeof *ug);
	memset(ug, 0, sizeof *ug);
	ug->egldpy = eglGetDisplay(NULL);
	eglInitialize(ug->egldpy, NULL, NULL);
	s3eKeyboardRegister(S3E_KEYBOARD_KEY_EVENT, kbcb, ug);
	s3ePointerRegister(S3E_POINTER_BUTTON_EVENT,mbcb,ug);
	s3ePointerRegister(S3E_POINTER_MOTION_EVENT,mmcb,ug);
	context = ug;
    return (UGCtx)ug;
}

void ugFini(UGCtx ug)
{
    /*XXXblythe open windows?*/
    UGCtx_t* _ug = (UGCtx_t*)ug;
	if (_ug->win)
		ugDestroyWindow((UGWindow)_ug->win, _ug->egldpy);
	s3eKeyboardUnRegister(S3E_KEYBOARD_KEY_EVENT, kbcb);
	s3ePointerUnRegister(S3E_POINTER_BUTTON_EVENT,mbcb);
	s3ePointerUnRegister(S3E_POINTER_MOTION_EVENT,mmcb);
	eglTerminate(_ug->egldpy);
    free(_ug);
}

UGCtx APIENTRY ugCtxFromWin(UGWindow uwin)
{
    return (UGCtx)((UGWindow_t*)uwin)->ug;
}

UGWindow ugCreateWindow(UGCtx ug,  const char* config, const char* title, int width, int height, int x, int y)
{
    UGWindow_t *w = malloc(sizeof *w);
    UGCtx_t* _ug = (UGCtx_t*)ug;

	EGLint n, vid;
	EGLConfig configs[1];

	/*XXXblythe horrible hack, need to parse config string*/
	static int attribs[] = { EGL_RED_SIZE, 1, EGL_NONE }; /*XXXblythe*/
	static int attribs2[] = {EGL_RED_SIZE, 1, EGL_DEPTH_SIZE, 1, EGL_NONE};
	int depth = strstr(config, "UG_DEPTH") != 0;

	memset(w, 0, sizeof *w);
	w->ug = _ug;
	if (!eglChooseConfig(_ug->egldpy, depth ? attribs2 : attribs, configs, 1, &n))
	{
		s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglChooseConfig failed - failed to find a usable GLES configuration. Application cannot run.");
		free(w);
		return 0;
	}
	w->eglconfig = configs[0];
	eglGetConfigAttrib(_ug->egldpy, configs[0], EGL_NATIVE_VISUAL_ID, &vid);

	w->width = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
	w->height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);

	w->surface = eglCreateWindowSurface(_ug->egldpy, w->eglconfig, (NativeWindowType)s3eGLGetNativeWindow(), 0);
	w->eglctx = eglCreateContext(_ug->egldpy, w->eglconfig, NULL, NULL);
	if (!eglMakeCurrent(_ug->egldpy, w->surface, w->surface, w->eglctx))
	{
		s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglMakeCurrent failed. Application cannot run.");
		free(w);
		return 0;
	}

	_ug->win = w;
	return (UGWindow)w;
}

void ugDestroyWindow(UGWindow uwin, EGLDisplay egldpy)
{
    UGWindow_t *w = (UGWindow_t*)uwin;
	
	eglMakeCurrent(EGL_NO_DISPLAY, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);

	if(w->eglctx && egldpy)
	{
		eglDestroyContext(egldpy, w->eglctx);
		w->eglctx = NULL;
	}
	if(w->surface && egldpy)
	{
		eglDestroySurface(egldpy, w->surface);
		w->surface = NULL;
	}

	free(w);
}

void ugReshapeFunc(UGWindow uwin, void (*f)(UGWindow uwin, int width, int height))
{
    UGWindow_t *w = (UGWindow_t*)uwin;
    w->reshape = f;
}

void ugDisplayFunc(UGWindow uwin, void (*f)(UGWindow uwin))
{
    UGWindow_t *w = (UGWindow_t*)uwin;
    w->draw = f;
}

void ugKeyboardFunc(UGWindow uwin, void (*f)(UGWindow uwin, int key, int x, int y))
{
    UGWindow_t *w = (UGWindow_t*)uwin;
    w->kbd = f;
}

void ugPointerFunc(UGWindow uwin, void (*f)(UGWindow uwin, int button, int state, int x, int y))
{
    UGWindow_t *w = (UGWindow_t*)uwin;
    w->pointer = f;
}

void ugMotionFunc(UGWindow uwin, void (*f)(UGWindow uwin, int x, int y))
{
    UGWindow_t *w = (UGWindow_t*)uwin;
    w->motion = f;
}

void ugIdleFunc(UGCtx ug, void (*f)(UGWindow w))
{
    UGCtx_t *_ug = (UGCtx_t*)ug;
    _ug->idle = f;
}


#define MSPF 30
void yieldCycle()
{
	static int time = 0;
	time = s3eTimerGetMs()-time;
	if(time > MSPF)
		s3eDeviceYield(0);
	else
		s3eDeviceYield(MSPF-time);
	time = s3eTimerGetMs();
}

void ugMainLoop(UGCtx ug) {
	UGWindow_t* win;
	int sz = 1;

	context = (UGCtx_t *) ug;
	win = (UGWindow_t*) context->win;

	s3eConfigGetInt("S3E", "ScreenZoom", &sz);

	if (win->reshape)
	{
		int w = s3eSurfaceGetInt(S3E_SURFACE_WIDTH) * sz;
		int h = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT) * sz;
		win->reshape((UGWindow)win, w, h);
	}

	// Main message loop:
	while (!s3eDeviceCheckQuitRequest())
	{
		//IwTrace(UG, ("loop %p", win));
		if (context->idle)
			context->idle((UGWindow)win);
		if (win)
		{
			if (win->draw)
				win->draw((UGWindow)win);
		}
		s3ePointerUpdate();
		yieldCycle();
	}
	ugFini(ug);
}

void ugPostRedisplay(UGWindow uwin)
{
}

void ugSwapBuffers(UGWindow uwin)
{
    UGWindow_t* w = (UGWindow_t*)uwin;
    eglSwapBuffers(w->ug->egldpy, w->surface);
}

extern int main(int, char**);

S3E_MAIN_DECL void IwMain()
{
	char* args[] = { "s3e" };
    main(1, args);
}
