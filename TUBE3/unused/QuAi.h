#pragma once

#include <iostream>
#include <set>
#include <map>
#include <vector>
#include <algorithm>
#include <time.h>


using namespace std;


namespace QUAI
{
	typedef char byte;

	static int nCnt = 0;
	
	//TODO should get rid of this and use the one in QuConstants
	static int face[9*6] =
					{18, 9, 0, 21, 12, 3, 24, 15, 6,
					 2, 11, 20, 5, 14, 23, 8, 17, 26,
					 18, 19, 20, 9, 10, 11, 0, 1, 2,
					 6, 7, 8, 15, 16, 17, 24, 25, 26,
					 0, 1, 2, 3, 4, 5, 6, 7, 8,
					 24, 25, 26, 21, 22, 23, 18, 19, 20};

	inline byte& GetFaceAt(byte* arrCube, int nFace, int nPos)
	{
		return arrCube[face[nFace * 9 + nPos]];
	}

	//FACE, MOVE, PLAYER
	inline bool WinCheck(byte* arrCube, int nFace, int nMove, int n)
	{
		int nCombos[ (6*2 + 1) * 27] = {
			6,  1, 2, 3, 6, 4, 8, 9, 18, 10, 20, 12, 24,
			3,  0, 2, 4, 7, 10, 19, 0, 0, 0, 0, 0, 0,
			6,  0, 1, 4, 6, 5, 8, 10, 18, 11, 20, 14, 26,
			3,  0, 6, 4, 5, 12, 21, 0, 0, 0, 0, 0, 0,
			4,  0, 8, 1, 7, 2, 6, 3, 5, 0, 0, 0, 0,
			3,  2, 8, 3, 4, 14, 23, 0, 0, 0, 0, 0, 0,
			6,  0, 3, 2, 4, 7, 8, 12, 18, 15, 24, 16, 26,
			3,  1, 4, 6, 8, 16, 25, 0, 0, 0, 0, 0, 0,
			6,  0, 4, 2, 5, 6, 7, 14, 20, 16, 24, 17, 26,
			3,  0, 18, 10, 11, 12, 15, 0, 0, 0, 0, 0, 0,
			4,  0, 20, 1, 19, 2, 18, 9, 11, 0, 0, 0, 0,
			3,  2, 20, 9, 10, 14, 17, 0, 0, 0, 0, 0, 0,
			4,  0, 24, 3, 21, 6, 18, 9, 15, 0, 0, 0, 0,
			0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			4,  2, 26, 5, 23, 8, 20, 11, 17, 0, 0, 0, 0,
			3,  6, 24, 9, 12, 16, 17, 0, 0, 0, 0, 0, 0,
			4,  6, 26, 7, 25, 8, 24, 15, 17, 0, 0, 0, 0,
			3,  8, 26, 11, 14, 15, 16, 0, 0, 0, 0, 0, 0,
			6,  0, 9, 2, 10, 6, 12, 19, 20, 21, 24, 22, 26,
			3,  1, 10, 18, 20, 22, 25, 0, 0, 0, 0, 0, 0,
			6,  0, 10, 2, 11, 8, 14, 18, 19, 22, 24, 23, 26,
			3,  3, 12, 18, 24, 22, 23, 0, 0, 0, 0, 0, 0,
			4,  18, 26, 19, 25, 20, 24, 21, 23, 0, 0, 0, 0,
			3,  5, 14, 20, 26, 21, 22, 0, 0, 0, 0, 0, 0,
			6,  0, 12, 6, 15, 8, 16, 18, 21, 20, 22, 25, 26,
			3,  7, 16, 19, 22, 24, 26, 0, 0, 0, 0, 0, 0,
			6,  2, 14, 6, 16, 8, 17, 18, 22, 20, 23, 24, 25
		};

		int nOff = face[nFace * 9 + nMove] * (6*2 + 1) + 1;
		int sz = nCombos[nOff - 1];
		for(int i = 0; i < sz; ++i)
			if( (arrCube[nCombos[nOff + i*2]] == n) && (arrCube[nCombos[nOff + i*2 + 1]] == n))
				return true;
		return false;
	}

	inline int FaceControl(byte* arrCube, int nFace, int nPlayer)
	{
		int nRet = 0;
		for(int i = 0; i < 9; ++i)
		{
			//if(GetFaceAt(arrCube, nFace, i) == nPlayer && (i == 0 || i == 2 || i == 6 || i == 8))
			//    nRet += 1;
        
			if(GetFaceAt(arrCube, nFace, i) != 0)
				continue;
			if(WinCheck(arrCube, nFace, i, nPlayer))
			{
				if(nRet == 0)
					nRet += 10;
				else
					nRet += 5;
				if(i == 4)
					nRet += 5;
			}
		}
		return nRet;
	}

	inline int GetHeuristic(byte* arrCube, int nPlayer)
	{
		int nRet = 0;
		for(int i = 0; i < 6; ++i)
			nRet += FaceControl(arrCube, i, nPlayer);
		if(nPlayer == 2)
			nRet *= -1;
		return nRet;
	}

	inline bool Triple(byte* arrCube, int nFace, int nPlayer, int a, int b, int c)
	{
		return 
			(GetFaceAt(arrCube, nFace, a) == nPlayer) &&
			(GetFaceAt(arrCube, nFace, b) == nPlayer) &&
			(GetFaceAt(arrCube, nFace, c) == nPlayer);
	}

	inline int WinCheck(byte* arrCube)
	{
		for(int n = 1; n <= 2; ++n)
		for(int f = 0; f < 6; ++f)
		{
			if(
				Triple(arrCube, f, n, 0, 1, 2) ||
				Triple(arrCube, f, n, 3, 4, 5) ||
				Triple(arrCube, f, n, 6, 7, 8) ||
				Triple(arrCube, f, n, 0, 3, 6) ||
				Triple(arrCube, f, n, 1, 4, 7) ||
				Triple(arrCube, f, n, 2, 5, 8) ||
				Triple(arrCube, f, n, 0, 4, 8) ||
				Triple(arrCube, f, n, 2, 4, 6)
			  )
				return n;
		}
		return 0;
	}

	inline bool FaceFull(byte* arrCube, int nFace)
	{
		for(int i = 0; i < 9; ++i)
			if(GetFaceAt(arrCube, nFace, i) == 0)
				return false;
		return true;
	}


	inline int GetQuMove(int nFace, int n)
	{
		int QuMove[6*4] = 
						{2, 3, 4, 5, 2, 3, 4, 5,
						 0, 1, 4, 5, 0, 1, 4, 5,
						 0, 1, 2, 3, 0, 1, 2, 3};
		return QuMove[nFace*4 + n];
	}

	inline int OppFace(int nFace)
	{
		int opp[6] = {1, 0, 3, 2, 5, 4};
		return opp[nFace];
	}

	inline void PrintCube(byte* arrCube)
	{
		int x,y;
		for(y = 0; y < 3; ++y)
		{   
			cout << "       ";
			for(x = 0; x < 3; ++x)
				cout << int(GetFaceAt(arrCube, 2, y*3 + x)) << " ";
			cout << "\n";
		}
    
		cout << "\n";

		for(y = 0; y < 3; ++y)
		{   
			for(x = 0; x < 3; ++x)
				cout << int(GetFaceAt(arrCube, 0, y*3 + x)) << " ";
			cout << " ";
			for(x = 0; x < 3; ++x)
				cout << int(GetFaceAt(arrCube, 4, y*3 + x)) << " ";
			cout << " ";
			for(x = 0; x < 3; ++x)
				cout << int(GetFaceAt(arrCube, 1, y*3 + x)) << " ";
			cout << " ";
			cout << "\n";
		}

		cout << "\n";
    
		for(y = 0; y < 3; ++y)
		{   
			cout << "       ";
			for(x = 0; x < 3; ++x)
				cout << int(GetFaceAt(arrCube, 3, y*3 + x)) << " ";
			cout << "\n";
		}

		cout << "\n";

		for(y = 0; y < 3; ++y)
		{   
			cout << "       ";
			for(x = 0; x < 3; ++x)
				cout << int(GetFaceAt(arrCube, 5, y*3 + x)) << " ";
			cout << "\n";
		}
	}

	inline byte Opp(byte b)
	{
		if(b == 1)
			return 2;
		else
			return 1;
	}


	/*
	byte StrategyGame(byte* arrCube, byte* arrData)
	{
		++nCnt;
		int i, j;
		for(i = 0; i < 9; ++i)
		{
			byte& b = GetFaceAt(arrCube, arrData[0], i);
			if(b != 0)
				continue;
			if(arrData[1] == 1)
				b = 1;
			else
				b = 2;
			if(WinCheck(arrCube, arrData[0], i, arrData[1]))
			{
				cout << "Automatic " << int(arrData[1]) << "\n";
				return arrData[1];
			}
			b = 0;
		}

		bool bInconclusive = false;

		int nQuMoveOrder[9] = {0, 2, 6, 8, 4, 1, 3, 5, 7};

		for(int i_ = 0; i_ < 9; ++i_)
		{
			int i = nQuMoveOrder[i_];

			byte& b = GetFaceAt(arrCube, arrData[0], i);

			if(b != 0)
				continue;
			for(j = 0; j < 4; ++j)
			{
				byte arrNewCube[27];
				memcpy(arrNewCube, arrCube, 27);
				byte arrNewData[4] = {GetQuMove(arrData[0], j), 0, arrData[2] + 1, arrData[3]};
            
				if(arrData[1] == 1)
				{
					arrNewData[1] = 2;
					GetFaceAt(arrNewCube, arrData[0], i) = 1;
				}
				else
				{
					arrNewData[1] = 1;
					GetFaceAt(arrNewCube, arrData[0], i) = 2;
				}

				byte bRet;

				cout << int(arrData[2]) << ": Player " << int(arrData[1]) << " marks " << i << " at face "
					 << int(arrData[0]) << " and turns to " << int(arrNewData[0]) << "\n";

				if(arrData[2] >= arrData[3])
				{
					arrNewData[2] = 0;
					arrNewData[3] = 4;

					bRet = Game(arrNewCube, arrNewData);

					//PrintCube(arrNewCube);

					if(bRet == 0)
					{
						arrNewData[2] = 0;
						arrNewData[3] = 17;

						bRet = Game(arrNewCube, arrNewData);
					}

					cout << "res: " << int(bRet) << "\n";
				}
				else
				{
					bRet = StrategyGame(arrNewCube, arrNewData);
				}

				if(bRet == arrData[1])
				{
					if(arrData[2] == 0)
					{
					//    cout << i << " " <<  GetQuMove(arrData[0], j) << "\n";
					}

					return arrData[1];
				}
				else if(bRet == 0)
					bInconclusive = true;
			}

			//if(arrData[1] == 1)
			//    break;
		}

		if(bInconclusive)
			return 0;
		return Opp(arrData[1]);
	}
	*/

	static int nNodes = 0;

	struct QuMove
	{
		int nQuMove;
		int nFace;

		QuMove(){}
		QuMove(int nQuMove_, int nFace_):nQuMove(nQuMove_), nFace(nFace_){}

		bool operator == (const QuMove& mv) const {return nFace == mv.nFace && nQuMove == mv.nQuMove;}
		bool operator < (const QuMove& mv) const {return nQuMove == mv.nQuMove ? nFace < mv.nFace : nQuMove < mv.nQuMove;}
	};

	struct StrategyNode
	{
		QuMove FirQuMove;
		map<QuMove, StrategyNode*> mpSecResponse;

		StrategyNode()
		{
			++nNodes;
		}

		void CleanMap()
		{
			while(mpSecResponse.size())
			{
				delete mpSecResponse.begin()->second;
				mpSecResponse.erase(mpSecResponse.begin());
			}
		}

		~StrategyNode()
		{
			--nNodes;
			CleanMap();
		}
	};

	inline void PrintStrategyNode(StrategyNode* pNode, int nLevel = 0)
	{
		for(int i = 0; i < nLevel; ++i)
			cout << " ";

		if(pNode->FirQuMove.nQuMove == -1)
			cout << "1: filled\n";
		else if(pNode->FirQuMove.nFace == -1)
			cout << "1: " << pNode->FirQuMove.nQuMove << " wins\n";
		else
			cout << "1: " << pNode->FirQuMove.nQuMove << " " << pNode->FirQuMove.nFace << "\n";

		for(map<QuMove, StrategyNode*>::iterator itr = pNode->mpSecResponse.begin(), etr = pNode->mpSecResponse.end(); itr != etr; ++itr)
		{
			for(int i = 0; i < nLevel; ++i)
				cout << " ";
			cout << "2: " << itr->first.nQuMove << " " << itr->first.nFace << "\n";
			PrintStrategyNode(itr->second, nLevel + 1);
		}
	}

	inline int NodeCount(StrategyNode* pNode)
	{
		int nRet = 1;
		for(map<QuMove, StrategyNode*>::iterator itr = pNode->mpSecResponse.begin(), etr = pNode->mpSecResponse.end(); itr != etr; ++itr)
			nRet += NodeCount(itr->second);
		return nRet;
	}

	inline int NodeDepth(StrategyNode* pNode)
	{
		if(pNode->mpSecResponse.empty())
			return 0;
		int nMax = 0;
		for(map<QuMove, StrategyNode*>::iterator itr = pNode->mpSecResponse.begin(), etr = pNode->mpSecResponse.end(); itr != etr; ++itr)
		{
			int n = NodeDepth(itr->second);
			if(nMax < n)
				nMax = n;
		}
		return nMax + 1;
	}

	inline bool DeepNode(StrategyNode* pNode, int nDepth, int nMaxDepth)
	{
		if (nDepth > nMaxDepth)
			return true;
		for(map<QuMove, StrategyNode*>::iterator itr = pNode->mpSecResponse.begin(), etr = pNode->mpSecResponse.end(); itr != etr; ++itr)
		{
			if(DeepNode(itr->second, nDepth + 1, nMaxDepth))
				return true;
		}
		return false;
	}

	inline void CheckSanity(byte* arrCube, int nFace, StrategyNode* pNode)
	{
		if(pNode->mpSecResponse.empty())
		{
			if(FaceFull(arrCube, nFace))
				return;

			if(WinCheck(arrCube) != 0)
				throw string("someone already won!");

			if(pNode->FirQuMove.nQuMove < 0 || pNode->FirQuMove.nQuMove >= 9)
				throw string("invalid first QuMove");

			if(GetFaceAt(arrCube, nFace, pNode->FirQuMove.nQuMove) != 0)
				throw string("invalid first QuMove!");

			GetFaceAt(arrCube, nFace, pNode->FirQuMove.nQuMove) = 1;

			if(WinCheck(arrCube) != 1)
				throw string("false winning condition!");

			return;
		}
    
		if(GetFaceAt(arrCube, nFace, pNode->FirQuMove.nQuMove) != 0)
			throw string("invalid first QuMove!");

		GetFaceAt(arrCube, nFace, pNode->FirQuMove.nQuMove) = 1;

		if(pNode->FirQuMove.nFace < 0 || pNode->FirQuMove.nFace >= 6)
			throw string("bad turn for first QuMove");

		if(pNode->FirQuMove.nFace == nFace || pNode->FirQuMove.nFace == OppFace(nFace))
			throw string("invalid turn for first QuMove");

		nFace = pNode->FirQuMove.nFace;

		QuMove mv;
		int nC = 0;
		for(mv.nQuMove = 0; mv.nQuMove < 9; ++mv.nQuMove)
		{
			if(GetFaceAt(arrCube, nFace, mv.nQuMove) != 0)
				continue;

			for(int f = 0; f < 4; ++f, ++nC)
			{
				mv.nFace = GetQuMove(nFace, f);
				map<QuMove, StrategyNode*>::iterator itr = pNode->mpSecResponse.find(mv);
				if(itr == pNode->mpSecResponse.end())
					throw string("incomplete second player QuMoves!");

				byte arrNewCube[27];
				memcpy(arrNewCube, arrCube, 27);

				GetFaceAt(arrNewCube, nFace, mv.nQuMove) = 2;

				CheckSanity(arrNewCube, mv.nFace, itr->second);
			}
		}

		if(nC != pNode->mpSecResponse.size())
			throw std::string("too many second player QuMoves!");
	}

	inline void OptimizeNode(byte* arrCube, int nFace, StrategyNode* pNode);


	inline int Game(byte* arrCube, StrategyNode* pNode, int nFace, int nPlayer, int nDepth, int nDepthMax, bool bBegin = false)
	{
		int i_, i, j;
		bool bFilled = true;
		for(i = 0; i < 9; ++i)
		{
			if(GetFaceAt(arrCube, nFace, i) != 0)
				continue;
			bFilled = false;
			if(WinCheck(arrCube, nFace, i, nPlayer))
			{
				if(bBegin)
				{
					for(int k = 0; k < nDepth; ++k)
						cout << " ";
					cout << "Automatic\n";
				}
				if(nPlayer == 1)
					pNode->FirQuMove = QuMove(i, -1);
				return nPlayer;
			}
		}

		if(bFilled)
		{
			if(nPlayer == 1)
				pNode->FirQuMove = QuMove(-1, -1);
			return nPlayer;
		}

		if(!bBegin && (nDepth >= nDepthMax))
			return 0;

		bool bInconclusive = false;

		int nQuMoveOrder[9] = {0, 2, 6, 8, 4, 1, 3, 5, 7};

		for(i_ = 0; i_ < 9; ++i_)
		{
			i = nQuMoveOrder[i_];

			if(GetFaceAt(arrCube, nFace, i) != 0)
				continue;

			for(j = 0; j < 4; ++j)
			{
				if(nPlayer == 1)
					pNode->CleanMap();
            
				byte arrNewCube[27];
				memcpy(arrNewCube, arrCube, 27);

				GetFaceAt(arrNewCube, nFace, i) = nPlayer;

				StrategyNode* pNextNode = 0;

				QuMove mv(i, GetQuMove(nFace, j));

				if(bBegin)
				{
					for(int k = 0; k < nDepth; ++k)
						cout << " ";
					cout << "Player " << nPlayer << " QuMoves " << mv.nQuMove << " at face " << mv.nFace << "\n";
				}

				if(nPlayer == 1)
				{
					pNode->FirQuMove = mv;
					pNextNode = pNode;
				}
				else
				{
					pNextNode = new StrategyNode();
					pNode->mpSecResponse[mv] = pNextNode;
				}

				int bRet;
				if(bBegin)
				{
					if(nDepth < nDepthMax)
						bRet = Game(arrNewCube, pNextNode, GetQuMove(nFace, j), Opp(nPlayer), nDepth + 1, nDepthMax, true);
					else
					{
						bRet = Game(arrNewCube, pNextNode, GetQuMove(nFace, j), Opp(nPlayer), 0, 4);
						if(bRet == 0)
						{
							pNextNode->CleanMap();
							bRet = Game(arrNewCube, pNextNode, GetQuMove(nFace, j), Opp(nPlayer), 0, 17);
						}

						if(bRet == 0)
							throw std::string("oh, no!");
					}

					for(int k = 0; k < nDepth; ++k)
						cout << " ";
					cout << "Res: " << bRet << "\n";
				}
				else
					bRet = Game(arrNewCube, pNextNode, GetQuMove(nFace, j), Opp(nPlayer), nDepth + 1, nDepthMax);

				if(bBegin && (nDepth == nDepthMax || (nDepth - 1) == nDepthMax) && nPlayer == 1)
				{
					if(DeepNode(pNode, 0, 2))
					{
						for(int k = 0; k < nDepth; ++k)
							cout << " ";
						cout << "Optimizing... ";
						byte CpCube[27];
						memcpy(CpCube, arrCube, 27);
						int n1 = NodeCount(pNode); 
						OptimizeNode(CpCube, nFace, pNode);
						int n2 = NodeCount(pNode);
						cout << int(double(n2)/n1 * 100) << "%\n";
					}
				}

				if(bRet == nPlayer)
					return nPlayer;
				else if(bRet == 0)
					bInconclusive = true;
			}
		}

		if(bInconclusive)
			return 0;
		return Opp(nPlayer);
	}

	struct Tracker
	{
		bool bInit;
		int nMax;
		QuMove vMaxVal;
		int nMin;
		QuMove vMinVal;

		Tracker():bInit(false){}

		void Boom(int val, QuMove obj)
		{
			if(!bInit)
			{
				bInit = true;
				nMax = nMin = val;
				vMaxVal = vMinVal = obj;
			}
			else
			{
				if(val < nMin)
				{
					nMin = val;
					vMinVal = obj;
				}
				if(val > nMax)
				{
					nMax = val;
					vMaxVal = obj;
				}
			}
		}
	};

	inline int HeuristicGame(byte* arrCube, int nFace, int nPlayer, int nDepth, int nDepthMax, QuMove& mvNext)
	{
		int i, j;
		bool bFilled = true;
		for(i = 0; i < 9; ++i)
		{
			if(GetFaceAt(arrCube, nFace, i) != 0)
				continue;
			bFilled = false;
			if(WinCheck(arrCube, nFace, i, nPlayer))
			{
				mvNext.nQuMove = i;
	            mvNext.nFace = -1;
				return 10000 * (nPlayer == 1 ? 1 : -1);
			}
		}

		if(bFilled)
		{
			mvNext.nQuMove = i;
            mvNext.nFace = -1;
			return 10000 * (nPlayer == 1 ? 1 : -1);
		}

		if(nDepth >= nDepthMax)
		{
			return GetHeuristic(arrCube, 1) + GetHeuristic(arrCube, 2);
		}

		Tracker t;

		for(i = 0; i < 9; ++i)
		{
			if(GetFaceAt(arrCube, nFace, i) != 0)
				continue;
			for(j = 0; j < 4; ++j)
			{
				byte arrNewCube[27];
				memcpy(arrNewCube, arrCube, 27);

				GetFaceAt(arrNewCube, nFace, i) = nPlayer;

				QuMove mv(i, GetQuMove(nFace, j));

				QuMove mv_dummy;
				int nVal = HeuristicGame(arrNewCube, mv.nFace, Opp(nPlayer), nDepth + 1, nDepthMax, mv_dummy);

				t.Boom(nVal, mv);
			}
		}

		if(!t.bInit)
			throw std::string("fool");

		if(nPlayer == 1)
		{
			mvNext = t.vMaxVal;
			return t.nMax;
		}
		else
		{
			mvNext = t.vMinVal;
			return t.nMin;
		}
	}

	inline void OptimizeNode(byte* arrCube, int nFace, StrategyNode* pNode)
	{
		if(!DeepNode(pNode, 0, 2))
			return;
    
		StrategyNode* pAltNode = new StrategyNode();
		int nRes = Game(arrCube, pAltNode, nFace, 1, 0, 4);
		if(nRes == 1)
		{
			int n1 = NodeCount(pNode);
			int n2 = NodeCount(pAltNode);
			if(n2 < n1)
			{
				pNode->CleanMap();
				pNode->FirQuMove = pAltNode->FirQuMove;
				pNode->mpSecResponse = pAltNode->mpSecResponse;

				while(pAltNode->mpSecResponse.size())
					pAltNode->mpSecResponse.erase(pAltNode->mpSecResponse.begin());
			}

			delete pAltNode;

			return;
		}
		else
		{
			delete pAltNode;

			if(nRes == 2)
				throw std::string("what?");
		}

		GetFaceAt(arrCube, nFace, pNode->FirQuMove.nQuMove) = 1;
		nFace = pNode->FirQuMove.nFace;

		for(map<QuMove, StrategyNode*>::iterator itr = pNode->mpSecResponse.begin(),
			etr = pNode->mpSecResponse.end(); itr != etr; ++itr)
		{
			byte arrNewCube[27];
			memcpy(arrNewCube, arrCube, 27);

			GetFaceAt(arrNewCube, nFace, itr->first.nQuMove) = 2;

			OptimizeNode(arrNewCube, itr->first.nFace, itr->second);
		}
	}

	inline bool Random_Game(byte* arrCube, int& nFace, int& nPlayer, int nDepth, int nDepthMax)
	{
		if(nDepth > nDepthMax)
			return true;

		int i, j;
		bool bFilled = true;
		for(i = 0; i < 9; ++i)
		{
			if(GetFaceAt(arrCube, nFace, i) != 0)
				continue;
			bFilled = false;
			if(WinCheck(arrCube, nFace, i, nPlayer))
				return false;
		}

		if(bFilled)
			return false;

		while(true)
		{
			i = rand()%9;

			if(GetFaceAt(arrCube, nFace, i) != 0)
				continue;

			j = rand()%4;

			//cout << i << " " << GetQuMove(nFace, j) << "\n";

			GetFaceAt(arrCube, nFace, i) = nPlayer;
			nFace = GetQuMove(nFace, j);
			nPlayer = Opp(nPlayer);

			return Random_Game(arrCube, nFace, nPlayer, nDepth + 1, nDepthMax);
		}
	}

}

/*
int main()
{
    srand( unsigned (time(0)) );

    byte cube[27] = {};
    int nFace;
    int nPlayer;

    nFace = 4;
    nPlayer = 1;

    for(;;)
    {
        QuMove mv;
        HeuristicGame(cube, nFace, nPlayer, 0, 3, mv);

        GetFaceAt(cube, nFace, mv.nQuMove) = nPlayer;

        PrintCube(cube);

        cout << "\nPlayer " << nPlayer << " QuMoves " << mv.nQuMove << " and turns to face " << mv.nFace << "\n\n";

        nFace = mv.nFace;
        nPlayer = Opp(nPlayer);

        cout << "1: " << GetHeuristic(cube, 1) << "\n";
        cout << "2: " << GetHeuristic(cube, 2) << "\n\n";

        cin.get();
    }     
    return 0;
}*/