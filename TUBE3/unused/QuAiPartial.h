#pragma once

#include <iostream>
#include <iomanip>
#include <string>
#include <list>
#include <set>
#include <map>
#include <vector>
#include <algorithm>
#include <time.h>

using namespace std;



namespace QUAIPARTIAL
{

typedef char byte;

static int face[9*6] =
                {18, 9, 0, 21, 12, 3, 24, 15, 6,
                 2, 11, 20, 5, 14, 23, 8, 17, 26,
                 18, 19, 20, 9, 10, 11, 0, 1, 2,
                 6, 7, 8, 15, 16, 17, 24, 25, 26,
                 0, 1, 2, 3, 4, 5, 6, 7, 8,
                 24, 25, 26, 21, 22, 23, 18, 19, 20};

inline byte& GetFaceAt(byte* arrCube, int nFace, int nPos)
{
    return arrCube[face[nFace * 9 + nPos]];
}

inline bool WinCheck(byte* arrCube, int nFace, int nMove, int n)
{
    int nCombos[ (6*2 + 1) * 27] = {
        6,  1, 2, 3, 6, 4, 8, 9, 18, 10, 20, 12, 24,
        3,  0, 2, 4, 7, 10, 19, 0, 0, 0, 0, 0, 0,
        6,  0, 1, 4, 6, 5, 8, 10, 18, 11, 20, 14, 26,
        3,  0, 6, 4, 5, 12, 21, 0, 0, 0, 0, 0, 0,
        4,  0, 8, 1, 7, 2, 6, 3, 5, 0, 0, 0, 0,
        3,  2, 8, 3, 4, 14, 23, 0, 0, 0, 0, 0, 0,
        6,  0, 3, 2, 4, 7, 8, 12, 18, 15, 24, 16, 26,
        3,  1, 4, 6, 8, 16, 25, 0, 0, 0, 0, 0, 0,
        6,  0, 4, 2, 5, 6, 7, 14, 20, 16, 24, 17, 26,
        3,  0, 18, 10, 11, 12, 15, 0, 0, 0, 0, 0, 0,
        4,  0, 20, 1, 19, 2, 18, 9, 11, 0, 0, 0, 0,
        3,  2, 20, 9, 10, 14, 17, 0, 0, 0, 0, 0, 0,
        4,  0, 24, 3, 21, 6, 18, 9, 15, 0, 0, 0, 0,
        0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        4,  2, 26, 5, 23, 8, 20, 11, 17, 0, 0, 0, 0,
        3,  6, 24, 9, 12, 16, 17, 0, 0, 0, 0, 0, 0,
        4,  6, 26, 7, 25, 8, 24, 15, 17, 0, 0, 0, 0,
        3,  8, 26, 11, 14, 15, 16, 0, 0, 0, 0, 0, 0,
        6,  0, 9, 2, 10, 6, 12, 19, 20, 21, 24, 22, 26,
        3,  1, 10, 18, 20, 22, 25, 0, 0, 0, 0, 0, 0,
        6,  0, 10, 2, 11, 8, 14, 18, 19, 22, 24, 23, 26,
        3,  3, 12, 18, 24, 22, 23, 0, 0, 0, 0, 0, 0,
        4,  18, 26, 19, 25, 20, 24, 21, 23, 0, 0, 0, 0,
        3,  5, 14, 20, 26, 21, 22, 0, 0, 0, 0, 0, 0,
        6,  0, 12, 6, 15, 8, 16, 18, 21, 20, 22, 25, 26,
        3,  7, 16, 19, 22, 24, 26, 0, 0, 0, 0, 0, 0,
        6,  2, 14, 6, 16, 8, 17, 18, 22, 20, 23, 24, 25
    };

    int nOff = face[nFace * 9 + nMove] * (6*2 + 1) + 1;
    int sz = nCombos[nOff - 1];
    for(int i = 0; i < sz; ++i)
        if( (arrCube[nCombos[nOff + i*2]] == n) && (arrCube[nCombos[nOff + i*2 + 1]] == n))
            return true;
    return false;
}

inline int FaceControl(byte* arrCube, int nFace, int nPlayer)
{
    int nRet = 0;
    for(int i = 0; i < 9; ++i)
    {
       if(GetFaceAt(arrCube, nFace, i) != 0)
            continue;
        if(WinCheck(arrCube, nFace, i, nPlayer))
        {
            if(nRet == 0)
                nRet += 10;
            else
                nRet += 5;
            if(i == 4)
                nRet += 5;
        }
    }
    return nRet;
}

inline int GetHeuristic(byte* arrCube, int nPlayer)
{
    int nRet = 0;
    
    int nCorners[8] = {0, 2, 6, 8, 18, 20, 24, 26};
    int i;
    
    for(i = 0; i < 8; ++i)
        if(arrCube[nCorners[i]] == nPlayer)
            ++nRet;
    for(i = 0; i < 6; ++i)
        nRet += FaceControl(arrCube, i, nPlayer);
    if(nPlayer == 2)
        nRet *= -1;
    return nRet;
}

inline bool Triple(byte* arrCube, int nFace, int nPlayer, int a, int b, int c)
{
    return 
        (GetFaceAt(arrCube, nFace, a) == nPlayer) &&
        (GetFaceAt(arrCube, nFace, b) == nPlayer) &&
        (GetFaceAt(arrCube, nFace, c) == nPlayer);
}

inline int WinCheck(byte* arrCube)
{
    for(int n = 1; n <= 2; ++n)
    for(int f = 0; f < 6; ++f)
    {
        if(
            Triple(arrCube, f, n, 0, 1, 2) ||
            Triple(arrCube, f, n, 3, 4, 5) ||
            Triple(arrCube, f, n, 6, 7, 8) ||
            Triple(arrCube, f, n, 0, 3, 6) ||
            Triple(arrCube, f, n, 1, 4, 7) ||
            Triple(arrCube, f, n, 2, 5, 8) ||
            Triple(arrCube, f, n, 0, 4, 8) ||
            Triple(arrCube, f, n, 2, 4, 6)
          )
            return n;
    }
    return 0;
}

inline bool FaceFull(byte* arrCube, int nFace)
{
    for(int i = 0; i < 9; ++i)
        if(GetFaceAt(arrCube, nFace, i) == 0)
            return false;
    return true;
}


inline int GetMove(int nFace, int n)
{
    int move[6*4] = 
                    {2, 3, 4, 5, 2, 3, 4, 5,
                     0, 1, 4, 5, 0, 1, 4, 5,
                     0, 1, 2, 3, 0, 1, 2, 3};
    return move[nFace*4 + n];
}

inline int OppFace(int nFace)
{
    int opp[6] = {1, 0, 3, 2, 5, 4};
    return opp[nFace];
}

inline void PrintCube(byte* arrCube)
{
    int x,y;
    for(y = 0; y < 3; ++y)
    {   
        cout << "       ";
        for(x = 0; x < 3; ++x)
            cout << int(GetFaceAt(arrCube, 2, y*3 + x)) << " ";
        cout << "\n";
    }
    
    cout << "\n";

    for(y = 0; y < 3; ++y)
    {   
        for(x = 0; x < 3; ++x)
            cout << int(GetFaceAt(arrCube, 0, y*3 + x)) << " ";
        cout << " ";
        for(x = 0; x < 3; ++x)
            cout << int(GetFaceAt(arrCube, 4, y*3 + x)) << " ";
        cout << " ";
        for(x = 0; x < 3; ++x)
            cout << int(GetFaceAt(arrCube, 1, y*3 + x)) << " ";
        cout << " ";
        cout << "\n";
    }

    cout << "\n";
    
    for(y = 0; y < 3; ++y)
    {   
        cout << "       ";
        for(x = 0; x < 3; ++x)
            cout << int(GetFaceAt(arrCube, 3, y*3 + x)) << " ";
        cout << "\n";
    }

    cout << "\n";

    for(y = 0; y < 3; ++y)
    {   
        cout << "       ";
        for(x = 0; x < 3; ++x)
            cout << int(GetFaceAt(arrCube, 5, y*3 + x)) << " ";
        cout << "\n";
    }
}

inline byte Opp(byte b)
{
    if(b == 1)
        return 2;
    else
        return 1;
}

struct QuMove
{
    int nMove;
    int nFace;

    QuMove(){}
    QuMove(int nMove_, int nFace_):nMove(nMove_), nFace(nFace_){}

    bool operator == (const QuMove& mv) const {return nFace == mv.nFace && nMove == mv.nMove;}
    bool operator < (const QuMove& mv) const {return nMove == mv.nMove ? nFace < mv.nFace : nMove < mv.nMove;}
};

template<class T>
struct TTracker
{
    bool bInit;
    int nMax;
    vector<T> vMaxVal;
    int nMin;
    vector<T> vMinVal;

    TTracker():bInit(false){}

    T GetMax(){return vMaxVal[rand()%vMaxVal.size()];}
    T GetMin(){return vMinVal[rand()%vMinVal.size()];}

    void Boom(int val, T obj)
    {
        if(!bInit)
        {
            bInit = true;
            nMax = nMin = val;
            vMaxVal.push_back(obj);
            vMinVal.push_back(obj);
        }
        else
        {
            if(val < nMin)  
            {
                nMin = val;
                vMinVal.clear();
                vMinVal.push_back(obj);
                return;
            }
            
            if(val > nMax)
            {
                nMax = val;
                vMaxVal.clear();
                vMaxVal.push_back(obj);
                return;
            }

            if(val == nMin)
                vMinVal.push_back(obj);

            if(val == nMax)
                vMaxVal.push_back(obj);
        }
    }
};

typedef TTracker<QuMove> Tracker;

inline int HeuristicGame(byte* arrCube, int nFace, int nPlayer, int nDepth, int nDepthMax, QuMove& mvNext)
{
    int i, j;
    bool bFilled = true;
    for(i = 0; i < 9; ++i)
    {
        if(GetFaceAt(arrCube, nFace, i) != 0)
            continue;
        bFilled = false;
        if(WinCheck(arrCube, nFace, i, nPlayer))
        {
            mvNext.nMove = i;
            mvNext.nFace = -1;
            return 10000 * (nPlayer == 1 ? 1 : -1);// + GetHeuristic(arrCube, 1) + GetHeuristic(arrCube, 2);
        }
    }

    if(bFilled)
    {
        mvNext.nMove = -1;
        mvNext.nFace = -1;
        return 10000 * (nPlayer == 1 ? 1 : -1);// + GetHeuristic(arrCube, 1) + GetHeuristic(arrCube, 2);
    }

    if(nDepth >= nDepthMax)
    {
        return GetHeuristic(arrCube, 1) + GetHeuristic(arrCube, 2);
    }

    Tracker t;

    for(i = 0; i < 9; ++i)
    {
        if(GetFaceAt(arrCube, nFace, i) != 0)
            continue;
        for(j = 0; j < 4; ++j)
        {
            byte arrNewCube[27];
            memcpy(arrNewCube, arrCube, 27);

            GetFaceAt(arrNewCube, nFace, i) = nPlayer;

            QuMove mv(i, GetMove(nFace, j));

            QuMove mv_dummy;
            int nVal = HeuristicGame(arrNewCube, mv.nFace, Opp(nPlayer), nDepth + 1, nDepthMax, mv_dummy);

            t.Boom(nVal, mv);
        }
    }

    if(!t.bInit)
        throw std::string("fool");

    if(nPlayer == 1)
    {
        mvNext = t.GetMax();
        return t.nMax;
    }
    else
    {
        mvNext = t.GetMin();
        return t.nMin;
    }
}

struct MoveTuple
{
    QuMove mvPrev;
    QuMove mvNext;

    MoveTuple(){}
    MoveTuple(QuMove mvPrev_, QuMove mvNext_):mvPrev(mvPrev_), mvNext(mvNext_){}
};

struct TurnInfo
{
    QuMove PrMove;

    byte* arrCube;
    int nFace;
    int nPlayer;
    TTracker<MoveTuple> t;
    int i, j;

    TurnInfo()
        :i(0), j(0), arrCube(0), nFace(-1), nPlayer(-1){}
    TurnInfo(QuMove PrMove_, byte* arrCube_, int nFace_, int nPlayer_)
        :i(0), j(0), PrMove(PrMove_), arrCube(arrCube_), nFace(nFace_), nPlayer(nPlayer_){}
    TurnInfo(byte* arrCube_, int nFace_, int nPlayer_)
        :i(0), j(0), arrCube(arrCube_), nFace(nFace_), nPlayer(nPlayer_){}
};

struct RecursiveStat
{
    QuMove mvNext;
    int nRes;
};


struct ProgressionInfo
{
    byte* StaticArrayStorage;
    std::list<TurnInfo> lsDepth;
    int nMaxDepth;
    TurnInfo infStart;

    RecursiveStat rs;

    ProgressionInfo(TurnInfo infStart_, int nMaxDepth_)
        :StaticArrayStorage(new byte[nMaxDepth_ * 27]), nMaxDepth(nMaxDepth_), infStart(infStart_)
    {
        TurnInfo infDummy;
        lsDepth.push_back(infDummy);
        lsDepth.push_back(infStart);
    }

    ~ProgressionInfo()
    {
        delete [] StaticArrayStorage;
    }
private:
    ProgressionInfo(const ProgressionInfo&);

};

inline bool HeuristicGameStep(ProgressionInfo& pi)
{
    if(pi.lsDepth.size() <= 1)
    {
        if(pi.infStart.nPlayer == 1)
        {
            pi.rs.mvNext = pi.lsDepth.front().t.GetMax().mvNext;
            pi.rs.nRes = pi.lsDepth.front().t.nMax;
        }
        else
        {
            pi.rs.mvNext = pi.lsDepth.front().t.GetMin().mvNext;
            pi.rs.nRes = pi.lsDepth.front().t.nMin;
        }

        return true;
    }

    TurnInfo& inf = *--pi.lsDepth.end();
    TurnInfo& infLast = *--(--pi.lsDepth.end());

    if(inf.j >= 4)
    {
        inf.j = 0;
        ++inf.i;
        if(inf.i >= 9)
        {
            if(inf.nPlayer == 1)
                infLast.t.Boom(inf.t.nMax, MoveTuple(inf.PrMove, inf.t.GetMax().mvPrev));
            else
                infLast.t.Boom(inf.t.nMin, MoveTuple(inf.PrMove, inf.t.GetMin().mvPrev));

            pi.lsDepth.pop_back();
            return false;
        }
    }

    if(inf.i == 0 && inf.j == 0)
    {
        int i;
        bool bFilled = true;
        for(i = 0; i < 9; ++i)
        {
            if(GetFaceAt(inf.arrCube, inf.nFace, i) != 0)
                continue;

            bFilled = false;
            if(WinCheck(inf.arrCube, inf.nFace, i, inf.nPlayer))
            {
                infLast.t.Boom(10000 * (inf.nPlayer == 1 ? 1 : -1), MoveTuple(inf.PrMove, QuMove(i, -1)));
                pi.lsDepth.pop_back();
                return false;
            }
        }

        if(bFilled)
        {
            infLast.t.Boom(10000 * (inf.nPlayer == 1 ? 1 : -1), MoveTuple(inf.PrMove, QuMove(-1, -1)));
            pi.lsDepth.pop_back();
            return false;
        }

        if(int(pi.lsDepth.size() - 2) >= pi.nMaxDepth)
        {
            infLast.t.Boom(GetHeuristic(inf.arrCube, 1) + GetHeuristic(inf.arrCube, 2), MoveTuple(inf.PrMove, QuMove(-1, -1)));
            pi.lsDepth.pop_back();
            return false;
        }
    }

    if(GetFaceAt(inf.arrCube, inf.nFace, inf.i) == 0)
    {
        byte* arrNewCube = &(pi.StaticArrayStorage[27*(pi.lsDepth.size() - 2)]);
        memcpy(arrNewCube, inf.arrCube, 27);

        GetFaceAt(arrNewCube, inf.nFace, inf.i) = inf.nPlayer;

        QuMove mv(inf.i, GetMove(inf.nFace, inf.j));

        pi.lsDepth.push_back(TurnInfo(mv, arrNewCube, mv.nFace, Opp(inf.nPlayer)));
    } 
    else
        inf.j = 4;

    ++inf.j;

    return false;
}

inline int RecursiveHeuristicGame(TurnInfo infStart, unsigned nMaxDepth, RecursiveStat& rs)
{
    ProgressionInfo inf(infStart, nMaxDepth);

    while(!HeuristicGameStep(inf));

    rs = inf.rs;
    return rs.nRes;
}

class AiData
{
    TurnInfo infStart;
    ProgressionInfo* pInf;

    int nCurrDepth;
    int nMaxDepth;

    QuMove mv;

    AiData(const AiData&);
public:

    AiData(byte* arrCube_, int nFace_, int nPlayer_, int nMaxDepth_ = -1)
        :infStart(arrCube_, nFace_, nPlayer_), mv(-1, -1),
        nCurrDepth(1), nMaxDepth(nMaxDepth_)
    {
        pInf = new ProgressionInfo(infStart, nCurrDepth);
    }

    ~AiData()
    {
        delete pInf;
    }

    QuMove GetMove(){return mv;}
    int GetDepth(){return nCurrDepth - 1;}
    
    bool Calculate(int nSteps)
    {
        for(int i = 0; i < nSteps; ++i)
        {
            if(nMaxDepth != -1 && nCurrDepth > nMaxDepth)
                return true;
            if(HeuristicGameStep(*pInf))
            {
                QuMove mv_tmp = pInf->rs.mvNext;
                int nRes = pInf->rs.nRes;

                //cout << "Depth " << nCurrDepth << ": " << nRes << " in " << i << " steps \n";

                if(nCurrDepth == 1)
                    mv = mv_tmp;
                
                if (infStart.nPlayer == 2 && nRes < 1000)
                    mv = mv_tmp;
                if (infStart.nPlayer == 1 && nRes > -1000)
                    mv = mv_tmp;
                

                ++nCurrDepth;

                delete pInf;
                pInf = new ProgressionInfo(infStart, nCurrDepth);

                if(nRes > 1000 || nRes < -1000 || (nMaxDepth != -1 && nCurrDepth > nMaxDepth))
                    return true;
            }
        }

        return false;
    }
};


inline int HowDeep(int nTurn)
{
    if (nTurn <= 4)
        return 3;
    if (nTurn <= 6)
        return 4;
    if (nTurn <= 7)
        return 5;
    if (nTurn <= 9)
        return 6;
    return 7;
}

inline int main()
{
    srand( unsigned (time(0)) );

    byte cube[27] = {};
    int nFace;
    int nPlayer;

    nFace = 4;
    nPlayer = 1;

    for(unsigned nTurn = 0 ; ; ++nTurn)
    {
        QuMove mv;

        for(int i = 1; i <= HowDeep(nTurn); ++i)
        {
            QuMove mv_tmp;
            RecursiveStat rs;
            int nTimer = clock();
            int nRes = RecursiveHeuristicGame(TurnInfo(cube, nFace, nPlayer), i, rs);
            mv_tmp = rs.mvNext;
            nTimer = clock() - nTimer;
            cout << "Depth " << i << ": " << nRes << " in " << nTimer << " ms\n";

            if (i == 1)
                mv = mv_tmp;
            if (nPlayer == 2 && nRes < 1000)
                mv = mv_tmp;
            if (nPlayer == 1 && nRes > -1000)
                mv = mv_tmp;

            if(nRes > 1000 || nRes < -1000)
                break;
        }
        cout << "\n";

        cout << "Steps:\n";

        AiData ad(cube, nFace, nPlayer);
        ad.Calculate(10000000);

        GetFaceAt(cube, nFace, mv.nMove) = nPlayer;

        PrintCube(cube);

        cout << "\nPlayer " << nPlayer << " moves " << mv.nMove << " and turns to face " << mv.nFace << "\n\n";

        if(mv.nFace == -1)
            break;

        nFace = mv.nFace;
        nPlayer = Opp(nPlayer);

        cout << "1: " << GetHeuristic(cube, 1) << "\n";
        cout << "2: " << GetHeuristic(cube, 2) << "\n\n";

        cin.get();
    }
        
    return 0;
}


}