#include "QuUtilities.h"
#include "QuAiPartial.h"
#include "QuConstants.h"
#include "QuCube.h"
#include <list>
#define MIN(a,b)    ( (a) < (b) ? (a) : (b) )

using namespace std;
using namespace QUAIPARTIAL;


class QuAiPlayer
{
	unsigned mC;
	unsigned mPlayer;
	unsigned mDiff;
	QuTimer mHighTimer;
	QuTimer mTurnTimer;
	char mCube[27];
	AiData * mData;
	QuMove mv;
public:
	QuAiPlayer(unsigned aPlayer, unsigned aDiff)
	{
		mData = NULL;
		mDiff = aDiff;
		mPlayer = aPlayer;
		mC = 0;
		mHighTimer.setTargetAndReset(3);
		mTurnTimer.setTargetAndReset(25);
	}
	~QuAiPlayer()
	{
		delete mData;
	}
	void update(QuCube & cube)
	{
		//if someone has won, we need do nothing
		if(cube.getCurrentPlayer() != mPlayer || cube.getCurMoveType() == 2)
			return;
		
		if(mData == NULL)
		{
			cube.makeAiCube(mCube);
			//cout << "player is " << mPlayer << " diff is " << mDiff << endl;
			mData = new AiData(mCube,cube.getCurrentFace(),cube.getCurrentAiPlayer(),mDiff);
			if(mDiff > 3)
				mHighTimer.setTargetAndReset(8);
			else if(mDiff < 10)
				mHighTimer.setTargetAndReset(5);
			else
				mHighTimer.setTargetAndReset(3);
		}

		if(mC == 0)
		{
			mData->Calculate(MAX_AI_DEPTH);

			if(mTurnTimer.isExpired())
			{
				mC = 1;
				mTurnTimer.reset();
			}
			else
				mTurnTimer.update();
		}
		else if(mC < 10)
		{
			mData->Calculate(MAX_AI_DEPTH);
			if(mHighTimer.isExpired())
			{
				while(mC < 10)
				{
					if(cube.highlightMove(FACE_TO_SUBCUBES[cube.getCurrentFace()][mC-1]))
					{
						mHighTimer.reset();
						mC++;
						break;
					}
					mC++;
				}
			}
			mHighTimer.update();
		}
		else if (mC == 10)
		{
			mData->Calculate(MAX_AI_DEPTH);
			if(mTurnTimer.isExpired())
			{
				mTurnTimer.reset();
				mC = 11;
			}
			else
			{
				cube.unHighlightCube();
			}
			mTurnTimer.update();
		}
		else if (mC == 11)
		{
			mData->Calculate(MAX_AI_DEPTH);
			if(mHighTimer.isExpired())
			{
				mv = mData->GetMove();
				cube.attempMakeMove(FACE_TO_SUBCUBES[cube.getCurrentFace()][mv.nMove]);
				mHighTimer.reset();
				mC = 12;
			}
			mHighTimer.update();
		}
		else
		{
			if(mTurnTimer.isExpired())
			{
				if(mv.nFace == -1 || !cube.attemptTurnToFace(mv.nFace))
				{
					cube.makeARandomTurn();
				}
				mTurnTimer.reset();
				mC = 0;

				delete mData;
				mData = NULL;
			}
			mTurnTimer.update();
		}
		
	}
private:
};

class QuPlayerProfile
{
	bool isPlayerAi;
	int mPlayer;
	QuAiPlayer * mAi;
public:
	QuPlayerProfile(int aPlayer)
	{
		mPlayer = aPlayer;
		isPlayerAi = false;
	}
	~QuPlayerProfile()
	{
		if(isPlayerAi)
			delete mAi;
	}
	void reset()
	{
		if(isPlayerAi)
			delete mAi;
		isPlayerAi = false;
	}
	bool isAi()
	{
		return isPlayerAi;
	}
	void createAi(int diff)
	{
		isPlayerAi = true;
		assert(diff >= 0);
		mAi = new QuAiPlayer(mPlayer,diff);
	}
	QuAiPlayer & getAi()
	{
		assert(isPlayerAi);
		return *mAi;
	}
	
};
