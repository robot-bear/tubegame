#include "QuConstants.h"
#include "QuExceptions.h"
#include "QuCube.h"
#include "../QuSound_STROKE.h"

//LAME
#include "QuMaterials.h"
list<int> QuLights::globalAvail = list<int>();
QuSoundManager * QuSoundManager::mSelf = NULL;

bool QuCube::highlightMove(unsigned aSubCube)
{
	//TODO check if game state is correct
	if(getCurMoveType() != 0)
		return false;
	unHighlightCube();
	QuMoveImplications imp = getCubeImplication(aSubCube, getCurrentPlayer());
	if(isOkayToHighlight(imp,getCurrentPlayer()))
	{
		setSubCubeState(aSubCube,playerToHighlightState(getCurrentPlayer()));
		if(aSubCube != 13)
			QuSoundManager::getRef().playSound(SOUND_HIGHLIGHT);
		return true;
	}
	return false;
}

bool QuCube::isMoveHighlighted(unsigned aSubCube)
{
	return (getSubCubeState(aSubCube) == P1_HIGHLIGHT || getSubCubeState(aSubCube) == P2_HIGHLIGHT);
}

bool QuCube::attempMakeMove(unsigned aSubCube)
{
	if(getCurMoveType() != 0)
		return false;
	QuMoveImplications imp = getCubeImplication(aSubCube, getCurrentPlayer());
	if(isOkayToMove(imp,getCurrentPlayer()))
	{
		setSubCubeState(aSubCube,playerToOwnerState(getCurrentPlayer()));
		mLastMoveMade = aSubCube;
		if(computeWinningFaces(aSubCube))
		{
			if(getCurrentPlayer() == 0)
				mCurState = P1_WINNER;
			else
				mCurState = P2_WINNER;

			makeFaceTurnByFace(computeWinningFace(getCurrentFace()));

			QuSoundManager::getRef().playSound(SOUND_WINNER);
			cout << "player " << getCurrentPlayer()+1 << " has won" << endl;
			return true;
		}
		QuSoundManager::getRef().playSound(SOUND_SELECT);
		advanceState();
		return true;
	}
	return false;
}

bool QuCube::attemptTurnToFace(unsigned aFace)
{
	if(getCurMoveType() != 1)
		return false;
	QuMoveImplications imp = getFaceImplication(aFace, getCurrentPlayer());
	if(isOkayToTurn(imp,getCurrentPlayer()))
	{
		makeFaceTurnByFace(aFace);
		advanceState();
		QuSoundManager::getRef().playSound(SOUND_NEXT_TURN);
		//printCurrentFace();
		return true;
	}
	return false;
}

bool QuCube::attemptTurnToFace(QuTurnDirections aDir)
{
	return attemptTurnToFace(getFaceInDirection(aDir));
}

void QuCube::undoLastMove()
{
	setSubCubeState(mLastMoveMade,NEUTRAL);
	mLastMoveMade = 13;
	revertState();
}