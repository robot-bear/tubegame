#pragma once
#include "QuConstants.h"
#include "QuAi.h"
#include <string>

//mCube values
//0	neutral
//1	p1 sel
//2 p2	sel
//3 p1 ownership
//4 p2 ownership

//Player values
//0 p1
//1 p2
//2 neutral

//terminology
//pos	face + position on face (0-8)
//face	a face of the cube (0-5)
//subcube	one of the 27 sub cubes (0-26)
class QuCube
{
	char mAiCube[27];
	QUAI::QuMove mAiMove;
	bool mIsAiMoveComputed;


	QuSubCubeStates mCube[27];
	unsigned mOrient[6]; 
	char mCurPlayer; 
	QuGameState mCurState;

	int mLastMoveMade;
	QuTurnDirections mLastTurn;	
public:
	QuCube()
	{
		initialize();
	}
	~QuCube(){}

	void reset()
	{
		initialize();
	}

	//---------------
	//game interface
	//---------------
	bool highlightMove(unsigned aSubCube);
	bool isMoveHighlighted(unsigned aSubCube);						//if I return true call highlightmove, otherwise call makemove
	bool attempMakeMove(unsigned aSubCube);
	bool attemptTurnToFace(unsigned aFace);							//these functions will automatically advance the turn if returns true
	bool attemptTurnToFace(QuTurnDirections aDir);
	void undoLastMove();
	int getLastMove() { return mLastMoveMade; }

	//0 - 1
	//1 - 2
	char getCurrentPlayer()
	{
		return stateToPlayer(mCurState);
	}

	int getCurrentAiPlayer()
	{
		return getCurrentPlayer()+1;
	}

	//0 - move
	//1 - turn
	//2 - win
	char getCurMoveType()
	{
		return stateToMoveType(mCurState);
	}

	QuGameState getCurrentState()
	{
		return mCurState;
	}

	void unHighlightCube()
	{
		for(int i = 0; i < 27; i++)
			if(mCube[i] == P1_HIGHLIGHT || mCube[i] == P2_HIGHLIGHT)
				mCube[i] = NEUTRAL;
	}

	//---------------
	//interface with camera
	//---------------
	QuTurnDirections getAndResetLastTurnDirection()
	{
		QuTurnDirections t = mLastTurn;
		mLastTurn = IDENTITY_DIRECTION;
		return t;
	}

	//------------
	//accessors
	//------------
	QuSubCubeStates getSubCubeState(unsigned aSubCube)
	{
		return (QuSubCubeStates)mCube[aSubCube];
	}
	
	unsigned getCurrentFace()
	{
		return mOrient[0];
	}

	unsigned getOppositeFace()
	{
		return mOrient[1];
	}

	//-------------
	//ai functions
	//-------------
	//plays current player as ai
	//TODO needs to pause a little bit before rotating so player can see what move the ai made
		//maybe interface this in testapp.cpp
	int getNumberMovesMade()
	{
		int counter = 0;
		for(int i = 0; i < 27; i++)
			if(getSubCubeState(i) != NEUTRAL)
				counter++;
		return counter;
	}
	void makeAiCube()
	{
		for(int i = 0; i < 27; i++)
		{
			switch(getSubCubeState(i))
			{
			case P1_OWN:
				mAiCube[i] = 1;
				break;
			case P2_OWN:
				mAiCube[i] = 2;
				break;
			default:
				mAiCube[i] = 0;
			}
		}
	}
	void makeAiCube(char * aCube)
	{
		for(int i = 0; i < 27; i++)
		{
			switch(getSubCubeState(i))
			{
			case P1_OWN:
				aCube[i] = 1;
				break;
			case P2_OWN:
				aCube[i] = 2;
				break;
			default:
				aCube[i] = 0;
			}
		}
	}
	int getSubCubeIndexFromFacePos(int aFace, int aPos)
	{
		//return QUAI::face[aFace * 9 + aPos];
		return FACE_TO_SUBCUBES[aFace][aPos];
	}
	//TODO what happens when ai can not make a move?
		//answer, mAiMove is not updated and the program crashes haha.
	void calculateAiMove(unsigned steps = 3)
	{
		if(mIsAiMoveComputed == true)
			return;
		makeAiCube();
		QUAI::HeuristicGame((char*)mAiCube,getCurrentFace(),getCurrentAiPlayer(),0,steps,mAiMove);
		mIsAiMoveComputed = true;
	}
	bool makeAiHighlight()
	{
		if(!mIsAiMoveComputed)
			return false;
		return highlightMove(getSubCubeIndexFromFacePos(getCurrentFace(),mAiMove.nQuMove));
	}
	bool makeAiMove()
	{
		if(!mIsAiMoveComputed)
			return false;
		//cout << "trying to make a move" << endl;
		if(attempMakeMove(getSubCubeIndexFromFacePos(getCurrentFace(),mAiMove.nQuMove)))
		{
			//cout << "is player " << getCurrentAiPlayer() << endl;
			makeAiCube();
			//QUAI::PrintCube((char*)mAiCube);
			return true;
		}
		return false;
	}
	bool makeAiTurn()
	{
		if(!mIsAiMoveComputed)
			return false;
		if( attemptTurnToFace(mAiMove.nFace) )
		{
			mIsAiMoveComputed = false;
			return true;
		}
		return false;
	}
	bool makeAllAiMoves()
	{
		calculateAiMove();
		if(!makeAiHighlight())
			return false;
		if(!makeAiMove())
			return false;
		if(!makeAiTurn())
			return false;
		return true;
	}
	bool makeARandomTurn()
	{
		for(int i = 0; i < 4; i++)
		{
			if(attemptTurnToFace(FACE_TO_POSSIBLE_TURNS[getCurrentFace()][i]))
				return true;
		}
		return false;
	}
private:
	void initialize()
	{
		//set the starting orientation
		for(unsigned i = 0; i < 6; i++)
			//mOrient[i] = i;
			mOrient[i] = ROTATION_TO_ORIENTATION[3][i];

		//set entire cube to neutral
		for(int i = 0; i < 27; i++)
			mCube[i] = NEUTRAL;

		mCurState = P1_MOVE;
		//last turn was identity
		mLastTurn = IDENTITY_DIRECTION;
		mLastMoveMade = 13;

		mIsAiMoveComputed = false;
	}

	bool checkFace(unsigned face)
	{
		int i = face;
		for(int j = 0; j < 8; j++)
		{
			if( mCube[FACE_TO_SUBCUBES[i][TRIPLE_INDICES[j][0]]] != NEUTRAL &&
				mCube[FACE_TO_SUBCUBES[i][TRIPLE_INDICES[j][0]]] == mCube[FACE_TO_SUBCUBES[i][TRIPLE_INDICES[j][1]]] &&
				mCube[FACE_TO_SUBCUBES[i][TRIPLE_INDICES[j][0]]] == mCube[FACE_TO_SUBCUBES[i][TRIPLE_INDICES[j][2]]]
			)
				return true;
		}
		return false;
	}
	int computeWinningFace(unsigned checkFirst = 0)
	{
		if(checkFace(checkFirst))
			return checkFirst;
		for(int i = 0; i < 6; i++)
		{
			if( i == checkFirst)
				continue;
			if(checkFace(i))
				return i;
		}
		return -1;
	}

	bool computeWinningFaces(unsigned moveToCheck)
	{
		static int nCombos[ (6*2 + 1) * 27] = {
			6,  1, 2, 3, 6, 4, 8, 9, 18, 10, 20, 12, 24,
			3,  0, 2, 4, 7, 10, 19, 0, 0, 0, 0, 0, 0,
			6,  0, 1, 4, 6, 5, 8, 10, 18, 11, 20, 14, 26,
			3,  0, 6, 4, 5, 12, 21, 0, 0, 0, 0, 0, 0,
			4,  0, 8, 1, 7, 2, 6, 3, 5, 0, 0, 0, 0,
			3,  2, 8, 3, 4, 14, 23, 0, 0, 0, 0, 0, 0,
			6,  0, 3, 2, 4, 7, 8, 12, 18, 15, 24, 16, 26,
			3,  1, 4, 6, 8, 16, 25, 0, 0, 0, 0, 0, 0,
			6,  0, 4, 2, 5, 6, 7, 14, 20, 16, 24, 17, 26,
			3,  0, 18, 10, 11, 12, 15, 0, 0, 0, 0, 0, 0,
			4,  0, 20, 1, 19, 2, 18, 9, 11, 0, 0, 0, 0,
			3,  2, 20, 9, 10, 14, 17, 0, 0, 0, 0, 0, 0,
			4,  0, 24, 3, 21, 6, 18, 9, 15, 0, 0, 0, 0,
			0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			4,  2, 26, 5, 23, 8, 20, 11, 17, 0, 0, 0, 0,
			3,  6, 24, 9, 12, 16, 17, 0, 0, 0, 0, 0, 0,
			4,  6, 26, 7, 25, 8, 24, 15, 17, 0, 0, 0, 0,
			3,  8, 26, 11, 14, 15, 16, 0, 0, 0, 0, 0, 0,
			6,  0, 9, 2, 10, 6, 12, 19, 20, 21, 24, 22, 26,
			3,  1, 10, 18, 20, 22, 25, 0, 0, 0, 0, 0, 0,
			6,  0, 10, 2, 11, 8, 14, 18, 19, 22, 24, 23, 26,
			3,  3, 12, 18, 24, 22, 23, 0, 0, 0, 0, 0, 0,
			4,  18, 26, 19, 25, 20, 24, 21, 23, 0, 0, 0, 0,
			3,  5, 14, 20, 26, 21, 22, 0, 0, 0, 0, 0, 0,
			6,  0, 12, 6, 15, 8, 16, 18, 21, 20, 22, 25, 26,
			3,  7, 16, 19, 22, 24, 26, 0, 0, 0, 0, 0, 0,
			6,  2, 14, 6, 16, 8, 17, 18, 22, 20, 23, 24, 25
		};

		int nOff = moveToCheck * (6*2 + 1) + 1;
		int sz = nCombos[nOff - 1];
		bool flag = false;
		//highlight all the winning faces
		for(int i = 0; i < sz; ++i)
		{
			if( (mCube[nCombos[nOff + i*2]] == playerToOwnSubCubeState(getCurrentPlayer())) && (mCube[nCombos[nOff + i*2 + 1]] == playerToOwnSubCubeState(getCurrentPlayer())) )
			{
				QuSubCubeStates set = playerToWinSubCubeState(getCurrentPlayer());
				mCube[moveToCheck] = set;
				mCube[nCombos[nOff + i*2]] = set;
				mCube[nCombos[nOff + i*2 + 1]] = set;
				flag = true;
			}
		}
		return flag;
	}

	//----------------
	//conversion and mode dependent functions
	//----------------
	inline QuSubCubeStates playerToHighlightState(char aPlayer){return (QuSubCubeStates)(aPlayer+1);}
	inline QuSubCubeStates playerToOwnerState(char aPlayer){return (QuSubCubeStates)(aPlayer+3);}

	//eventually these functions will do different things based on what mode we are in.
	bool isOkayToHighlight(QuMoveImplications aImp, char aPlayer)
	{
		if(aImp != SUBCUBE_FILLED && aImp != SUBCUBE_UNREACHABLE)
			return true;
		return false;
	}
	bool isOkayToMove(QuMoveImplications aImp, char aPlayer)
	{
		return isOkayToHighlight(aImp,aPlayer);
	}
	bool isOkayToTurn(QuMoveImplications aImp, char aPlayer)
	{
		if(aImp == FACE_FULL || aImp == FACE_UNREACHABLE)
			return false;
		return true;
	}

	
	//----------------
	//game state functions
	//----------------
	QuGameState advanceState()
	{
		return mCurState = (QuGameState)((unsigned)(mCurState+1)%4);
	}
	QuGameState revertState()
	{
		return mCurState = (QuGameState)((unsigned)(mCurState-1)%4);
	}

	//----------------
	//cube state functions
	//----------------
	bool currentPlayerHasWon()
	{
		makeAiCube();
		if(getCurrentAiPlayer() == QUAI::WinCheck((char*)mAiCube))
			return true;
		return false;
	}
	void setSubCubeState(unsigned aSubCube, QuSubCubeStates aState)
	{
		mCube[aSubCube] = aState;
	}
	QuMoveImplications getCubeImplication(unsigned aSubCube, char aPlayer)
	{
		//OKAY
		//SUBCUBE FILLED
		//SUBCUBE UNREACHABLE
		//CURRENT PLAYER WINS NOT IMPLEMENTED
		//CURRENT PLAYER WILL LOSE (i.e. for easy mode where you can't make stupid moves) NOT IMPLEMENTED
		if(getSubCubeState(aSubCube) != NEUTRAL && getSubCubeState(aSubCube) != P1_HIGHLIGHT && getSubCubeState(aSubCube) != P2_HIGHLIGHT)	//TODO do we check for move highlighted???
			return SUBCUBE_FILLED;
		return OKAY;
		//TODO implement the rest
	}
	void highlightMove(unsigned aSubCube, char aPlayer)
	{
		setSubCubeState(aSubCube,playerToHighlightState(aPlayer));
	}

	void makeMove(unsigned aSubCube, char aPlayer)
	{
		setSubCubeState(aSubCube,playerToOwnerState(aPlayer));
	}

	//----------------
	//C style orientation operations
	//----------------
	bool applyRotation(QuTurnDirections aDir, unsigned (&aOrient)[6])
	{
		if(aDir == INVALID_DIRECTION)
			return false;
		unsigned oldOrient [6];
		memcpy(&oldOrient,&aOrient,sizeof(unsigned)*6);
		for(unsigned i = 0; i < 6; i++)
			aOrient[i] = oldOrient[ROTATION_TO_ORIENTATION[(unsigned)aDir][i]];
		return true;
	}

	//----------------
	//face state functions
	//----------------
	void printCurrentFace()
	{
		cout << "printing face " << getCurrentFace() << endl;
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				cout << mCube[FACE_TO_SUBCUBES[getCurrentFace()][i*3 + j]] << " ";
			}
			cout << endl;
		}
	}
	QuTurnDirections getDirectionOfFace(unsigned aFace)
	{
		for(int i = 0; i < 6; i++)
		{
			if(i == 1)
				continue;
			if(aFace == mOrient[i])
				return (QuTurnDirections)i;
		}
		return INVALID_DIRECTION;
	}
	unsigned getFaceInDirection(QuTurnDirections aDir) 
	{
		return mOrient[aDir];
	}

	//TODO deprecated
	bool canFaceBeTurnedTo(unsigned aFace)
	{
		return !(aFace == getCurrentFace() || aFace == getOppositeFace());
		/*
		bool isCanBeTurnedTo = false;
		for(int i = 0; i < 4; i++)
		{
			if(aFace == FACE_TO_POSSIBLE_TURNS[getCurrentFace()][i])
				isCanBeTurnedTo = true;
		}
		return isCanBeTurnedTo;*/
	}
	bool isFaceFull(unsigned aFace)
	{
		for(int i = 0; i < 9; i++)
		{
			if(getSubCubeState(FACE_TO_SUBCUBES[aFace][i]) == NEUTRAL)
				return false;
		}
		cout << endl;
		return true;
	}
	QuMoveImplications getFaceImplication(unsigned aFace, char aPlayer)
	{
		if(!canFaceBeTurnedTo(aFace))
			return FACE_UNREACHABLE;
		if(isFaceFull(aFace))
			return FACE_FULL;

		//TODO NOT OKAY
		return OKAY;

		//OKAY
		//FACE_FULL
		//FACE UNREACHABLE
		//FACE FULL AND UNREACHABLE
		//CURRENT PLAYER WILL LOSE

		
		//TODO implement
		//use posToCube function
		//of course you can't since you're trying to inline everything
		throw QuNotImplementedException();
	}


	void makeFaceTurnByDirection(QuTurnDirections aDir)
	{
		//this function assumes that that the face is valid to turn to
		//well, we still do one check 
		if(aDir == INVALID_DIRECTION)
			throw QuUndefinedBehaviourException("made turn to INVALID_DIRECTION");

		//TODO make sure this is all good and working
		applyRotation(aDir,mOrient);
		mLastTurn = aDir;
		return;


		throw QuNotImplementedException();
	}
	void makeFaceTurnByFace(unsigned aFace)
	{
		makeFaceTurnByDirection(getDirectionOfFace(aFace));
	}
};