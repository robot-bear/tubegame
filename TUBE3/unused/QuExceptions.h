#pragma once
#include <string>

class QuNotImplementedException
{

};

struct QuBasicException
{
	std::string message;
	QuBasicException(std::string m)
	{
		message = m;
	}
};

struct QuUndefinedBehaviourException : QuBasicException
{
	std::string message;

	QuUndefinedBehaviourException(std::string m):QuBasicException(m)
	{}
};

struct QuNotInitializedException : QuBasicException
{
	std::string message;

	QuNotInitializedException(std::string m):QuBasicException(m)
	{}
};